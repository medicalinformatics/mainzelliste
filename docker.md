# Mainzelliste Docker

## Introduction
Mainzelliste is a widely used component for pseudonimization and record linkage. In order to set up a Mainzelliste yourself, you will need to configure a tomcat server and supply a database. To make this steps easier, we offer a docker image which devilers a preconfigured tomcat with mainzelliste installed.

## Quick start
To deploy Mainzelliste, a database is needed. You can either provide this on your docker host machine or as a docker container. For the approach with a docker container we provide a [docker-compose file](./docker-compose.yml) which can be used to either deploy with [_docker-compose_](https://docs.docker.com/compose/overview/) or with an ochestrator like [_docker stack_](https://docs.docker.com/engine/swarm/stack-deploy/).

### Deploy with docker compose

To deploy with docker-compose just navigate to the directory there _docker-compose file_ file lies and execute following line:
```shell
docker-compose up
```
To stop the deployed containers use:
```shell
docker-compose down
```

### Deploy with docker stack

To initialize a stack, your docker engine must be set to swarm mode. This can be done with following command:
```shell
docker swarm init
```
After this you should receive a notification that your machine is now a node of a docker swarm. Now you can start the mainzelliste stack with the following command:
```shell
docker stack deploy --compose-file /path/to/docker-compose.yml mainzelliste
```

## Configuration

### Using user preconfiguration

```shell
docker-compose -f docker-compose.yml -f docker-compose.user.yml up
```

### Environment Variables

Here is a list of all currently supported environment variables:

|Variable Name|Default Value|Description|
|-------------|-------------|-----------|
|`ML_DB_DRIVER`|`org.postgresql.Driver`|The driver used for db connection. Can be changed if you want to use mysql|
|`ML_DB_TYPE`|`postgresql`|Can be changed to mysql|
|`ML_DB_HOST`|`db`|Host address where the database is deployed, e.g. localhost|
|`ML_DB_PORT`|`5432`|Port of the database|
|`ML_DB_NAME`|`mainzelliste`|Name of the database|
|`ML_DB_USER`|`mainzelliste`|Username for a user with permissions on the database|
|`ML_DB_PASS`|(none, please define)|Password for a user who has permissions on the database. Can also be defined as Docker Secret `ML_DB_PASS_FILE`|
|`ML_API_KEY`|(none, please define)|The API Key for Mainzelliste API (MDAT server 0). Also also be defined as Docker Secret `ML_API_KEY_FILE`|
|`ML_ALLOWEDREMOTEADDRESSES`|`0.0.0.0/0`|Accepted origin addresses (IPv4 and/or IPv6) or address ranges in CIDR notation (IPv4 only) for MDAT server 0|
|`ML_LOG_LEVEL`|`warn`|Sets log level of the mainzelliste, allowed values are: error, warn, info, debug |
|`ML_REVERSEPROXY_FQDN`|(none, define if you want to use a reverseproxy)|Fully-qualified domain name to be used for access to this Mainzelliste, e.g. `patientlist.example.org`|
|`ML_REVERSEPROXY_PORT`|80 or 443 according to `ML_REVERSEPROXY_SSL`|The corresponding port number|
|`ML_REVERSEPROXY_SSL`|`false`|Set to `true` if Mainzelliste is accessed via SSL/TLS; `false` otherwise|
|`DEBUG`|`false`|Set to `true` if you want to open a port for remote debugging. You will need to forward the port 1099 with dockers port configuration.|
|`DEPLOYMENT_CONTEXT`|(none, define if you want to change |Defines the subpath were the mainzelliste will be deployed, e.g. "/someString" will deploy the mainzelliste to "http://mainzelliste:8080/someString"|

### Supported Secrets

You can define [docker secrets](https://docs.docker.com/engine/swarm/secrets/) for all environment variables starting with ML\_. The secret name is always the environment variable name followed by "\_SECRET". Values passed by a secret will override any value passed with the corresponding environment variable.
For example: The secret for "ML\_API\_KEY" is "ML\_API\_KEY\_SECRET"

```shell
echo "<your_secret>" | docker secret create <secret_name> -
```

> Please note: When using docker-compose you will need to pass the secrets from a file on the filesystem using the syntax from [*docker-compose.user.yml*](./docker-compose.user.yml).  

### Passing your own mainzelliste configuration file

By default, the mainzelliste docker image will be delivered with an included baseline configuration, which will then be adapted with the environment variables. This baseline configuration is defined in [mainzelliste.docker.conf](./resources/mainzelliste.docker.conf).  
Because the mainzelliste is used in different scenarios, the image also supports passing your own mainzelliste configuration file to the container. The file will be processed the same way as the default one. 
This means, that the startup script will try to replace phrases starting with "ML\_" with the value of an environment variable that matches the phrase. It will also recognize corresponding secrets for those environment variables "ML\_\*\*\_SECRET".
The configuration must be passed with a docker secret named ***mainzelliste.docker.conf***. An example for this is available in [*docker-compose.user.yml*](./docker-compose.user.yml).  
You can create this secret with following command:
```shell
cat "</path/to/your/mainzelliste.conf>" | docker secret create "mainzelliste.docker.conf" -; 
rm "</path/to/your/mainzelliste.conf>";
```
After creating the secret, you can remove the file from filesystem.

## For Developers
### Building the Mainzelliste Image
All Necessary files to build the mainzelliste container are included in mainzelliste repository. You can build your own container from the repository.
First you will need to build the application with maven. This can be done with a local installation of maven.
```shell script
mvn clean install
```
Alternatively you can run the maven build with the official maven docker container:
```shell script
docker run --rm -v $(pwd):/usr/src/build/ -w /usr/src/build/ \
       maven:3-openjdk-8 mvn clean install
```
> Please note: To speed up the development process, you can mount your maven home directory to "/root/.m2/" in the container. Maven will now use your maven settings in the container and
> won't download the libraries with every build.
> Further information on how to use the maven image is available at [docker hub](https://hub.docker.com/_/maven)

After finishing the maven build, you can build the docker container using docker-compose:
```shell script
docker-compose -f docker-compose.yml -f docker-compose.dev.yml build;
```
If you don't have access to docker-compose, you can use docker build command. e.g.:
```shell script
docker build -t medicalinformatics/mainzelliste:latest \
    -f Dockerfile ./target/mainzelliste-1.9.0-SNAPSHOT
```
> Please note: For running the docker build behind a proxy, you will need to specify build argument ***http_proxy***.
> An example for this configuration is available in docker-compose.dev.yml.

### Use Docker to deploy a database for testing
It is possible to deploy a database for testing mainzelliste with following command:
```shell
docker-compose up db
```
To access the database from your host system you will need to expose the database port to your host. e.g. with docker-compose.override.yml:
```yml
version: "3"
services:
  db:
    ports:
      - 5432:5432
```

With the default configuration the database should now be available on port 5432 on your docker host system and it should be possible to  
connect using the Values from the *ML_DB_NAME*, *ML_DB_USER* and *ML_DB_PASSWORD* environment variables.

