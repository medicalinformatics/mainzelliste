# Benchmarking 
## Pseudonymisation of demographic data and external source system IDs
Mainzelliste was deployed using docker container on a linux server. 
The server has 4 CPU cores and 32 GB RAM. The experiment is about to pseudonymize a data set
of demographic data (name, birthday, city ...) and external source system IDs using add patient 
functionality.

### Full load with one million records
We are going now to initialize an empty Mainzelliste with one million records using the
asynchronous batch api `/jobs`. The test sends ten requests sequentially, each containing 100,000 records.

| Mode                         | Response Time (minute) | Transaction per second |
|------------------------------|------------------------|------------------------|
| disabled IDAT record linkage | 15,5 min               | 1091 TPS               |
| enable IDAT record linkage   | 51 min                 | 326 TPS                |

### Incremental Update with 100 thousand records
Our Mainzelliste already contains one million records, external and generated IDs. 
Let's now test how an incremental update works. We are going to run the test using two different APIs.
The first test sends an asynchronous request to the REST endpoint `/jobs` containing all records and 
the second one sends 1000 requests sequentially to the REST endpoint `/patients`.  

#### asynchronous batch api
| Mode                         | Response Time (minute) | Transaction per second |
|------------------------------|------------------------|------------------------|
| disabled IDAT record linkage | 1,5 min                | 1091 TPS               |
| enable IDAT record linkage   | 9 min                  | 186 TPS                |

#### synchronous REST api
| Mode                         | Response Time (minute) | Transaction per second  |
|------------------------------|------------------------|-------------------------|
| disabled IDAT record linkage | 7 min                  | 235 TPS                 |
| enable IDAT record linkage   | 78 min                 | 21 TPS                  |
