openapi: "3.0.3"
info:
  title: "mainzelliste API"
  description: "mainzelliste API"
  contact:
    name: "Mainzelliste"
    url: "http://www.mainzelliste.de"
    email: "info@mainzelliste.de"
  version: "3.3.0 DRAFT"
servers:
  - url: "https://mainzelliste"
paths:
  /:
    get:
      summary: "Output status information as JSON."
      responses:
        "200":
          description: "A status message containing the distribution name and software version of this Mainzelliste instance."
          content:
            application/json:
              schema:
                type: "object"
                properties:
                  distname:
                    description: "Distribution name according to configuration parameter \"dist\"."
                    type: "string"
                  version:
                    description: "Software version of this Mainzelliste instance."
                    type: "string"
                required:
                  - "distname"
                  - "version"
                example:
                  distname: "Mainzelliste"
                  version: "1.9.0"
            text/html:
              schema:
                type: "string"
                example: "This is Mainzelliste running version 1.9.0 for Mainzelliste."
  /configuration/fieldKeys:
    get:
      summary: "GET configuration/fieldKeys"
      description: "Resource for querying configuration parameters via the REST interface. This resource is for internal use and subject to change."
      operationId: "getFieldKeys"
      responses:
        "200":
          description: "Get field keys as an array of strings."
          content:
            application/json:
              schema:
                description: "Field keys as an array of strings"
                type: "array"
                items:
                  type: "string"
                minItems: 1
                uniqueItems: true



  /html/addPatient:
    get:
      summary: "GET html/addPatient"
      parameters:
        - name: "tokenId"
          in: "query"
          schema:
            type: string
      responses:
        "200":
          description: "OK"
  /html/admin/editPatient:
    get:
      summary: "GET html/admin/editPatient"
      parameters:
        - name: "idType"
          in: "query"
          schema:
            type: string
        - name: "idString"
          in: "query"
          schema:
            type: string
      responses:
        "200":
          description: "OK"
    post:
      summary: "POST html/admin/editPatient"
      parameters:
        - name: "idType"
          in: "query"
          schema:
            type: string
        - name: "idString"
          in: "query"
          schema:
            type: string
      responses:
        "200":
          description: "OK"
  /html/admin/viewAuditTrail:
    get:
      summary: "GET html/admin/viewAuditTrail"
      parameters:
        - name: "idType"
          in: "query"
          schema:
            type: string
        - name: "idString"
          in: "query"
          schema:
            type: string
      responses:
        "200":
          description: "OK"
  /html/createPatient:
    get:
      summary: "GET html/createPatient"
      parameters:
        - name: "tokenId"
          in: "query"
          schema:
            type: string
      responses:
        "200":
          description: "OK"
  /html/editPatient:
    get:
      summary: "GET html/editPatient"
      parameters:
        - name: "tokenId"
          in: "query"
          schema:
            type: string
      responses:
        "200":
          description: "OK"
  /html/logo:
    get:
      summary: "GET html/logo"
      responses:
        "200":
          description: "OK"
  /jobs:
    post:
      summary: "POST jobs"
      parameters:
        - name: "tokenId"
          in: "query"
          schema:
            type: string
      responses:
        "200":
          description: "OK"
  /jobs/{jobId}/:
    get:
      summary: "GET jobs/{jobId}/"
      parameters:
        - name: "jobId"
          in: "path"
          required: true
          schema:
            type: string
        - name: "tokenId"
          in: "query"
          schema:
            type: string
      responses:
        "200":
          description: "OK"
  /monitoring/metrics/IDRequestCount:
    get:
      summary: "GET monitoring/metrics/IDRequestCount"
      parameters:
        - name: "start"
          in: "query"
          schema:
            type: string
        - name: "end"
          in: "query"
          schema:
            type: string
      responses:
        "200":
          description: "OK"
  /monitoring/metrics/patientCount:
    get:
      summary: "GET monitoring/metrics/patientCount"
      responses:
        "200":
          description: "OK"
  /monitoring/metrics/tentativePatientCount:
    get:
      summary: "GET monitoring/metrics/tentativePatientCount"
      responses:
        "200":
          description: "OK"
  /monitoring/status/cpuInfo:
    get:
      summary: "GET monitoring/status/cpuInfo"
      responses:
        "200":
          description: "OK"
  /monitoring/status/memoryInfo:
    get:
      summary: "GET monitoring/status/memoryInfo"
      responses:
        "200":
          description: "OK"

  /patients/checkMatch/{tokenId}:
    post:
      summary: "POST patients/checkMatch/{tokenId}"
      parameters:
        - name: "tokenId"
          in: "path"
          required: true
          schema:
            type: string
      responses:
        "200":
          description: "OK"
          
  /patients/tokenId/{tokenId}:
    summary: Get or Create Patients.
    get:
      summary: Get a list of patients.
      operationId: getPatients
      description:  |
        Retrieve a list of all patients to whom the provided token grants access. The token specifies which patients and which data (IDAT, IDs) of these patients are retrieved.
      parameters:
        - name: tokenId
          in: path
          description: Id of a valid "readPatients" token.
          required: true
          schema:
            type: string
      responses:
        "200":
          description: Get all patients.
          content:
            application/json:
              schema:
                type: array
                title: array of Patient
                items:
                  type: object
                  title: Patient Object
                  default: { }
                  required:
                    - fields
                    - ids
                  properties:
                    fields:
                      type: object
                      title: patient fields
                      additionalProperties:
                        type: string
                    ids:
                      type: array
                      title: patient ids
                      items:
                        type: object
                        required:
                          - idType
                          - idString
                        properties:
                          idType:
                            type: string
                          idString:
                            type: string
                        additionalProperties: false
                  example: 
                    - fields:
                        geburtstag: '01'
                        vorname: Lars
                        geburtsjahr: '1983'
                        nachname: Rarigausson
                        geburtsmonat: '11'
                      ids:
                        - idType: pid
                          idString: 0003Y0WZ
                          tentative: false
    post:
      summary: Create a patient and assign IDs to them.
      operationId: addPatient
      description: |
        It is checked whether a patient with the given identifying data already exists in the database. If not, a new patient is created, and the configured IDs are generated and returned or forwarded for this patient. If yes, the existing IDs are used. For the caller, it is not discernible whether the patient already existed or was newly created.
        Check if a patient with the given identifying data exists in the database. If not sure (true) whether the data corresponds to an existing patient or a new entry, perform the following:
        - `sureness: true`: Create a new patient. Generate the IDs as tentative (field "tentative," see ID section). This signals that a subsequent merge with an existing record is possible.
        - `sureness: false`: Check if the patient already exists and use the existing IDs.
      parameters:
        - name: tokenId
          in: path
          description: Id of a valid "addPatient" token.
          required: true
          schema:
            type: string
        - name: mainzellisteApiVersion
          in: header
          description: Version of Mainzelliste API.
          schema:
            type: string
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              title: Data
              description: IDAT fields and external Ids.
              type: object
              properties:
                sureness:
                  type: boolean
                  default: false
        required: true
      responses:
        '201':
          description: Patient created successfully.
          content:
            application/json:
              schema:
                type: array
                title: array of Patient
                items:
                  type: object
                  title: Patient Object
                  default: { }
                  required:
                    - idType
                    - idString
                    - tentative
                    - uri
                  properties:
                    idType:
                      description: Typ of the pseudonym
                      type: string
                    idString:
                      description: Value of the pseudonym
                      type: string
                    tentative:
                      description: If true, the patient is considered to be a duplicate
                      type: boolean
                    uri:
                      description: URI of the ID
                      type: string
            text/html:
              schema:
                type: string
        '400':
          description: Invalid input data. Details provided in the response body.
          content:
            application/json:
              schema:
                type: object
                properties:
                  error:
                    type: string
                    description: Description of the error.
        '401':
          description: Unauthorized - Invalid or missing token.
        '409':
          description: Conflict - Insecure match found, external ID already assigned to another patient, or external ID defined with a different value for the found patient.
    put:
      summary: Edit existing Patient
      description: |
        The dataset of the patient provided during token creation is modified as follows:
          - All fields and IDs that are included in the call with a non-empty value are set to the provided value.
          - Fields and IDs that are included in the call with an empty value (null or an empty string) are deleted. The field content is set to an empty string even if null is passed as input.
          - Fields and IDs that are not included in the call (i.e., the field name is not present as a key) are not changed.
          - Validation functions that affect the fields to be changed are applied. If date validation is configured, it may also consider non-changing fields (example: attempting to set the birth month to "02" for a patient with a birthdate of 30.01.2000 will fail). If any single validation fails, the entire call has no effect.
      parameters:
        - name: tokenId
          in: path
          description: Id of a valid "editPatient" token.
          required: true
          schema:
            type: string
        - name: mainzellisteApiVersion
          in: header
          description: version of mainzelliste api.
          schema:
            type: string
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              title: Data
              description: IDAT fields and external Ids.
              type: object
              properties:
                sureness:
                  type: boolean
                  default: false
        required: true
      responses:
        '200':
          description: Successfully updated patient data.
          content:
            text/html:
              schema:
                type: string
        '204':
          description: Successfully updated patient data.
          headers:
            Location:
              description: URL to the updated patient resource.
              schema:
                type: string
                format: uri
        '303':
          description: Successfully updated patient data with a redirect.
          headers:
            Location:
              description: URL to the updated patient resource (specified in the token).
              schema:
                type: string
                format: uri
        '400':
          description: Bad Request - Invalid input data. Details provided in the response body.
        '401':
          description: Unauthorized - Invalid or missing token.
        '404':
          description: Not Found - The specified patient does not exist.
    
  /patients/{tokenId}/{idType}/{idString}:
    delete:
      summary: "DELETE patients/{tokenId}/{idType}/{idString}"
      parameters:
        - name: "tokenId"
          in: "path"
          required: true
          schema:
            type: string
        - name: "idType"
          in: "path"
          required: true
          schema:
            type: string
        - name: "idString"
          in: path
          required: true
          schema:
            type: string
        - name: "withDuplicates"
          in: query
          schema:
            type: string
      responses:
        "200":
          description: "OK"
  
  /sessions:
    post:
      summary: Create new session.
      parameters:
        - in: header
          name: mainzellisteApiKey
          description: Configurated API key for authentification.
          schema:
            type: string
          required: true
      responses:
        '201':
          description: A status message containing the sessionId and URI of this session.
          content:
            application/json:
              schema:
                type: object
                properties:
                  sessionId:
                    description: ID of the session
                    type: string
                  uri:
                    description: URI of the session
                    type: string
                required:
                  - sessionId
                  - uri
                example:
                  sessionId: 57c4a68d-a113-4b52-a391-3f3fc2157344
                  uri: http://mainzelliste.de/sessions/57c4a68d-a113-4b52-a391-3f3fc2157344/
        '401':
          description: |
            Unauthorized - Authentication failed. Possible reasons include:
            - Incorrect or missing authentication key.
            - The IP address of the calling system is not whitelisted.
            - Insufficient permissions for this action.

  /sessions/{sessionId}:
    get:
      summary: Get existing session
      parameters:
        - name: sessionId
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: A status message containing the sessionId and URI of this session.
          content:
            application/json:
              schema:
                type: object
                properties:
                  sessionId:
                    description: ID of the session
                    type: string
                    example: 57c4a68d-a113-4b52-a391-3f3fc2157344
                  uri:
                    description: URI of the session
                    type: string
                    example: http://mainzelliste.de/sessions/57c4a68d-a113-4b52-a391-3f3fc2157344/
                required:
                  - sessionId
                  - uri
        '404':
          description: Not Found - The requested session doesn't exist.
    delete:
      summary: Delete existing session
      parameters:
        - name: sessionId
          in: path
          required: true
          schema:
            type: string
      responses:
        '204':
          description: Session successfully deleted / doesn't exist.
  
  #/sessions/{sessionId}/patients:
  #  get:
  #    summary: "GET sessions/{session}/patients/"
  #    parameters:
  #      - name: sessionId
  #        in: path
  #        required: true
  #        schema:
  #          type: string
  #    responses:
  #      "200":
  #        description: "OK"
  #  put:
  #    summary: "PUT sessions/{session}/patients/"
  #    parameters:
  #      - name: sessionId
  #        in: path
  #        required: true
  #        schema:
  #          type: string
  #    responses:
  #      "200":
  #        description: "OK"
  #  post:
  #    summary: "POST sessions/{session}/patients/"
  #    parameters:
  #      - name: sessionId
  #        in: path
  #        required: true
  #        schema:
  #          type: string
  #    responses:
  #      "200":
  #        description: "OK"
  #  delete:
  #    summary: "DELETE sessions/{session}/patients/"
  #    parameters:
  #      - name: sessionId
  #        in: path
  #        required: true
  #        schema:
  #          type: string
  #    responses:
  #      "200":
  #        description: "OK"
  
  #/sessions/{sessionId}/patients/{idType}/{idString}:
  #  delete:
  #    summary: "DELETE sessions/{session}/patients/{idType}/{idString}"
  #    parameters:
  #      - name: sessionId
  #        in: path
  #        required: true
  #        schema:
  #          type: string
  #       - name: "idType"
  #        in: "path"
  #        required: true
  #        schema:
  #          type: string
  #      - name: idString
  #        in: path
  #        required: true
  #        schema:
  #          type: string
  #    responses:
  #      "200":
  #        description: "OK"
  
  /sessions/{sessionId}/tokens:
    post:
      summary: Create a new token. 
      operationId: createAndRegisterToken
      parameters:
        - name: sessionId
          in: path
          description: Id of a valid session.
          required: true
          schema:
            type: string
        - name: mainzellisteApiKey
          in: header
          description: Key of mainzelliste API.
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              title: Token data
              description: Token type, allowed uses and data object
              type: object
              properties:
                type:
                  description: Type of the token.
                  type: string
                allowedUses:
                  description: Number of allowed uses of the token.  
                  type: integer       
                data:
                  description: Further data about the token.
                  type: object
              example: 
                type: integer
                allowedUses: 10
                data:
                  - idtypes: 
                    - pid
                  - redirect: "https://httpbin.org/get?pid={pid}&tid={tokenId}"
        required: true      
      responses:
        '201':
          description: Token created.
          content:
            application/json:
              schema:
                type: object
                title: Token Object
                default: { }
                properties:
                  id:
                    description: ID of the token.
                    type: string
                    example: 804adcbf-85cf-475d-afe5-ded0284ead07
                  type:
                    description: Type of the token.
                    type: string
                    example: addPatient
                  allowedUses:
                    description: Number of allowed uses of the token.  
                    type: integer 
                    example: 10      
                  remainingUses:
                    description: Remaining number of allowed uses of the token.
                    type: integer
                    example: 10
                  data: 
                    description: Further data about the token.
                    type: object
                    example: 
                      - idTypes: pid
                      - redirect: "https://httpbin.org/get?pid={pid}&tid={tokenId}"
                  uri: 
                    description: URI of the token.
                    type: string
                    example: "http://localhost:8080/sessions/74b6e7db-3ec6-4430-a6b0-06b695045ef9/tokens/804adcbf-85cf-475d-afe5-ded0284ead07"
                required:
                  - id
                  - type
                  - allowedUses
                  - remainingUses
                  - data
                  - uri
        '400':
          description: Invalid input data. Details provided in the response body.
        '401':
          description: |
            Authentification failed. Possible reasons include:
            - Incorrect or missing authentification key (mainzellisteApiKey).
            - The IP address of the calling system is not whitelisted.
            - Insufficient permissions for this action.
        '404':
          description: The requested session doesn't exist.
  /sessions/{sessionId}/tokens/{tokenId}:
    get:
      summary: Get token related to session.
      parameters:
        - name: sessionId
          in: path
          required: true
          schema:
            type: string
        - name: tokenId
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Successfully received requested token.
        '404':
          description: The requested token and/or session doesn't exist.
    delete:
      summary: Delete specified token.
      parameters:
        - name: sessionId
          in: path
          required: true
          schema:
            type: string
        - name: tokenId 
          in: path
          required: true
          schema:
            type: string
      responses:
        '204':
          description: Successfully deleted token.
        '404':
          description: The requested token and/or session doesn't exist.
          
# /validate/token:
#    get:
#      summary: "GET validate/token"
#      parameters:
#        - name: "tokenId"
#          in: "query"
#          schema:
#            type: string
#      responses:
#        "200":
#          description: "OK"

externalDocs:
  description: Find more info here
  url: http://www.mainzelliste.de