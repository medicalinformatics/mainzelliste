### 1.12.2 - 2024-10-22
#### Security
- Updated hapi-fhir to 7.4.4 due to **CVE-2024-25710**, **CVE-2024-45294**, **CVE-2024-26308** and **CVE-2024-47554**
- Updated crypto-tink to 1.13.0 due to **CVE-2024-7254**
### 1.12.1 - 2024-04-23
#### Fixed
- fetch patients with wildcard and using pagination return incorrect total count of the search result
### 1.12.0 - 2024-03-07
#### Feature
- New configuration attributes **transformers.replacements** and **transformers.delimiters** allows
overriding the default values of delimiters and special characters in text field transformers.
- Callback can be now configured using **idgenerator.{identifier}.callback** configuration attribute. 
A notification will be fired to the specified callback url when new id is generated. The content of 
the request payload and the headers can be controlled using some attribute of the callback configuration 
- **callback.resultIds**, **callback.resultFields** and **callback.headers.{attribute}**.
#### Changed
- Performance optimization of filtering the result of reading multiple patients
#### Fixed
- The read patient token now support searching multiple id with wildcard as value (@see **searchIds** attribute)
#### Experimental
- CreateIdsToken obtain new attribute **idType** in order to restrict the use of **/ids/{idType}** REST endpoint

### 1.11.1 - 2023-12-11
#### Fixed
- Persist id generator memory after generating an id with the new REST end point `\ids` 
### 1.11.0 - 2023-10-30
#### Feature
- A new REST endpoint **/jobs** allows you to perform a high-performance asynchronous batch request to efficiently 
add multiple patients in a single HTTP request. The response of the request tells you in the Location header (e.g _/jobs/{job-id}_) where to find the results. 
These will be available once the asynchronous operation completes. [PR243](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/243)
- **/patients** REST endpoint now supports pagination using _limit_ and _page_ query parameters and filtering via patient fields [PR261](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/261) [PR262](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/262)
- When adding a new patient, the IDs to be returned to the caller and in the callback payload can be now controlled through the „_resultIds_“ and „_callbackResultIds_“ parameters in the token. [PR250](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/250)
#### Changed
- Update Tomcat base docker image to v10.1 [PR252](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/252)
- Update to JDK 17
- Several performance optimization to make adding new patients faster [PR241](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/241) [PR244](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/244)
- Performance optimization of reading multiple patients [PR253](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/253)
#### Fixed
- Validate patient id in delete patient token [PR266](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/266)
- Fixed possible memory Leaks in Persistor. Thanks to Daniel Volk (Mainz University) for this contribution. [PR258](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/258)
- Editing patients failed with an internal system error, when the changed data matched with an existing patient. [PR273](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/273)
- Editing a patient A  whose old IDAT fields already caused an unsure match with an existing patient B falsely returned a conflict even if the new IDAT caused an unsure match with the same existing patient B. [PR273](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/273)
- Generate multiple ID of an existing patient with a token containing only the requested multiple ID type cause an internal error
- RuntimeExceptionMapper map some WebApplicationException to internal error response
- Check invalid checkMatch token
#### Experimental
- Added new REST end point to generate new IDs.
- Support filtering with patient fields in read patients REST endpoint.

### 1.10.9 - 2023-09-19
#### Fixed
- upgrading docker common to v0.7.6 fixes running server.xml patches related issues
### 1.10.8 - 2023-09-02
#### Security
- thymeleaf to 3.1.2.RELEASE due to **CVE-2023-38286**
- okio-jvm to 3.0.0 due to **CVE-2023-3635**
- guava to 31.0.1-jre due to **CVE-2023-2976**
#### Fixed
- OpenAPI Specification [PR175](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/175)
- Bitbucket ci pipeline
- Editing patient fails with internal system error, when the changed data matches with an existing patient.
- Editing patient, that its old fields already cause an unsure match with an existing patient, return conflict even if the new changes also cause an unsure match with the same existing patient

### 1.10.7 - 2023-05-11
#### Security
- Upgraded jettison to 1.5.4 due to **CVE-2023-1436**
- Updated mysql-connector-java to 8.0.33 due to **CVE-2023-21971**

### 1.10.6 - 2023-03-14
#### Fixed
- Close entity manager after checking if patient exist
- Editing patient with a given searchId failed, if already patients exist, that doesn't have an id with the same searchId type  

### 1.10.5 - 2023-03-01
#### Fixed
- Change base docker image of tomcat-common to "main"  

### 1.10.4 - 2023-02-20
#### Security
- Updated ca.uhn.hapi.fhir libs to 6.4.0 due to  **CVE-2023-24057**

### 1.10.3 - 2023-01-11
#### Security
- Upgraded jettison to 1.5.1 due to **CVE-2022-45685** and **CVE-2022-45693**
- Upgraded postgresql to 42.5.3 due to **CVE-2022-41946**
- Upgraded common-net to 3.9.0 due to **CVE-2021-37533**

### 1.10.2 - 2022-11-17
#### Security
- Updated ca.uhn.hapi.fhir libs to 6.2.0 due to  **CVE-2022-24329**, **CVE-2020-29582**, **CVE-2022-42889** and **CVE-2021-36090**
- Updated jackson-databind to 2.14.0 due to **CVE-2022-42003** and **CVE-2022-42004**
- Updated protobuf-java to 3.21.9 due to **CVE-2022-3171**
- Updated jettison to 1.5.1 due to **CVE-2022-40150**

### 1.10.1 - 2022-09-15
#### Security
- Updated gson to 2.9.1 due to [CVE-2022-25647](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-25647)
- Updated hapi-fhir to 5.7.9 due to [CVE-2021-32053](https://www.cve.org/CVERecord?id=CVE-2021-32053)
- Updated postgresql to 42.4.1 due to [CVE-2022-31197](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-31197)
- Updated tink to due to [CVE-2020-8929](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-8929)
- Updated log4j, jackson.databind, mysql, maven-artifact, java-jwt, testng, protobuf-java and jetbrains-annotations

### 1.10.0 - 2022-07-05
#### Feature
- Generated ID can be configured to be always encrypted before exposing it to the outside via the REST API [PR140](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/140)
- Read Patient can process encrypted search id [PR170](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/170)
- Added new ID type (cryptoID) which doesn't persist, but is generated by symmetric encryption from other persistent id type [PR158](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/158)
- Support encryption of IDs with JCE API ([PR140](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/140)) and Tink ([PR151](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/151)) libraries
- IDs can be now configured to be generated eagerly once other specific IDs are created [PR145](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/145)
- Configured ID types can now be requested from the REST API [PR193](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/193)
- Added environment variable MAINZELLISTE_CONFIG_DIRS to set a list of directories the mainzelliste should search for its config file [PR149](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/149)
#### Changed
- Performance Optimisation [PR194](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/194)
  - Use parallel stream (Fork/Join framework) to compare 1 to n patient in several threads depending on the number of cpu
  - improve the runtime of adding new patient by 30% [PR205](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/205)
  - Improve the runtime of adding new patient by 10% [PR207](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/207)
- External IDs can be now validated against preconfigured Regex pattern[PR182](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/182)
- REST Conform call of delete patient [PR202](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/202)
- ADD patient endpoint support JSON content type [PR180](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/180)
- Extended String Normalization for é and É. They will now be converted to e and E. [PR214](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/214)
- You can now configure field name which contains dot [PR125](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/125)
- Allow Environment Variables in user defined configs [PR198](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/198)
#### Fixed 
- Fixes the database creation, when the actual database user has access to more than one database on the db-server [PR206](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/206)
- Added proxy configuration to the download of subconfigurations [PR199](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/199)
- Editing external id cause data inconsistencies [PR235](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/235)
#### Experimental
- FHIR Patient API [PR179](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/179)
- OAuth [PR175](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/175)
- Big integer ID Generator [PR181](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/181)
- Federated ID Generator [PR204](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/204)
- High-performance asynchronous batch request to efficiently add multiple patients in a single HTTP request. [PR173](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/173)
- Support multiple IDs of the same id type. Can be used as identifiers of a multi-occurrence encounter (e.g. visit number, therapy number or laboratory number) Thanks to Marcel Parciak (Göttingen University) for his contribution. [PR164](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/164) [PR191](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/191)

### 1.9.11 - 2023-05-11
#### Security
- Upgraded jettison to 1.5.4 due to **CVE-2023-1436**
- Updated mysql-connector-java to 8.0.33 due to **CVE-2023-21971**

### 1.9.10 - 2023-01-11
#### Security
- Upgraded jettison to 1.5.1 due to **CVE-2022-45685** and **CVE-2022-45693**
- Upgraded postgresql to 42.5.3 due to **CVE-2022-41946**
- Upgraded common-net to 3.9.0 due to **CVE-2021-37533**

### 1.9.9 - 2022-11-17
#### Security
- Upgraded postgresql to 42.4.2 due to **CVE-2022-31197**  and **CVE-2022-26520**
- Upgraded jackson-databind to 2.14.0 due to **CVE-2022-42004**
- Upgraded protobuf-java to 3.21.9 due to **CVE-2022-3171**
- Upgraded jettison to 1.5.1 due to **CVE-2022-40149** and **CVE-2022-40150**

### 1.9.8 - 2022-02-04
#### Security
- Upgraded mysql-connector-java to 8.0.28 due to [CVE-2022-21363](https://nvd.nist.gov/vuln/detail/CVE-2022-21363)
- Upgraded postgresql lib to 42.3.2 due to [CVE-2022-21724](https://nvd.nist.gov/vuln/detail/CVE-2022-21724)
- Upgraded tomcat base image to 9-jdk8-temurin due to [CVE-2022-23218](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-23218) and [CVE-2022-23219](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-23219)
- Updated protobuf to 3.19.4 due to [CVE-2021-22569](https://nvd.nist.gov/vuln/detail/CVE-2021-22569)
- Updated log4j2 to 2.17.1 due to [CVE-2021-44832](https://nvd.nist.gov/vuln/detail/CVE-2021-44832)

### 1.9.7 - 2021-12-23
#### Security
- Updated jackson-mapper-asl to Jackson Databind v2.13.1 due to [CVE-2019-10172](https://nvd.nist.gov/vuln/detail/CVE-2019-10172)
- Updated mysql-connector-java to 8.0.27 due to [CVE-2021-2471](https://nvd.nist.gov/vuln/detail/CVE-2021-2471)
- Updated httpclient to 4.5.13 due to [CVE-2020-13956](https://nvd.nist.gov/vuln/detail/CVE-2020-13956)
- Updated commons-dbcp2 to 4.5.13 due to [CWE-200](https://cwe.mitre.org/data/definitions/200.html)
- Updated postgresql driver, commons-net, commons-codec, javax.servlet.jsp-api, maven-artifact and testng to latest version

### 1.9.6 - 2021-12-22
#### Fixed
- Fixed migration failure of MySql DB Schema of an old Mainzelliste version to v1.9.0 or any later version
#### Security
- Updated log4j2 to 2.17.0 due to [CVE-2021-45105](https://nvd.nist.gov/vuln/detail/CVE-2021-45105)

### 1.9.5 - 2021-12-15
#### Security
- Updated log4j2 to 2.16.0 due to [CVE-2021-45046](https://nvd.nist.gov/vuln/detail/CVE-2021-45046)

### 1.9.4 - 2021-12-13
#### Security
- Updated log4j2 to 2.15.0 due to [CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228)

### 1.9.3 - 2021-11-05
#### Fixed
- Fixed a NullPointerException, that was thrown, when an add patient token with redirect and an omitted idType element was resolved.

### 1.9.2 - 2021-09-20
#### Fixed
- Initialization of HLsh blocking keys prevents the launch of Mainzelliste, if the DB contains more than 32767 patient records 
#### Changed
- Docker Images on [medicalinformatics/mainzelliste](https://hub.docker.com/r/medicalinformatics/mainzelliste) now are build inside of the bitbucket pipelines, instead from docker hub.

### 1.9.1 - 2021-03-25
#### Fixed
- Adding new patient with external id in a database which contains a patient with the same external id 
  and empty fields causes a conflict, if the NullMatcher has been activated
- Set tomcat connector's scheme depending on ML_REVERSEPROXY_SSL environment variable
- Map runtime exceptions to a response with 500 status code and error message in the body
- Adding an existing patient in a database which contains a patient with the same fields remove 
  optional fields, if these fields haven't been provided.

### 1.9.0 - 2020-12-21
#### Added
- A new endpoint (/monitoring) was implemented that enables clients to request information about the operating state of Mainzelliste [PR85](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/85)
- It is now possible to request all ids of a specific idtype with the ReadToken. You will need to specify idstring as “*” [PR76](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/76)
- Tokens can now be used multiple times, reducing the number of required tokens e.g. for batch processes. Specifiy the „allowedUses“ parameter at Token creation to use this feature [PR83](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/83)
- The implementation of an GCP-compliant Audit Trail ([PR55](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/55)) is now available as Beta in this version
- All endpoints of the patient resource now support sending callbacks and redirecting users [PR77](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/77)
- A new idgenerator which validates the German ”Krankenversichertennummer” was implemented [PR68](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/68)
- It is now possible to configure subconfigs that should be merged into the current config at startup [PR70](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/70)
- An implementation for Database-side blocking with Bloom filters was added [PR67](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/67)
- The performance of the hashed fields was improved [PR66](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/66)
#### Changed
- Idgenerator memories will now persist with the idrequest [PR63](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/63)
- The versions of various dependencies were updated [PR92](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/92)
- Upgrade to Java Persistence 2.1
- use logger method with arguments and supplier to add logging message instead of string concatenation to improve the performance
#### Fixed
- A NullPointerException was raised when a client omitted certain fields [PR65](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/65)
- A NullPointerException was raised in certain configurations using different idTypes in the same Mainzelliste instance [PR86](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/86)
- ReadTokens could return ids that had not been yet persisted PR86 [PR76](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/76)
- An issue was fixed, in which normalized fields were being saved instead of the input fields [PR87](https://bitbucket.org/medicalinformatics/mainzelliste/pull-requests/87)

### 1.8.13 - 2023-05-11
#### Security
- Upgraded jettison to 1.5.4 due to **CVE-2023-1436**
- Updated mysql-connector-java to 8.0.33 due to **CVE-2023-21971**

### 1.8.12 - 2023-01-11
#### Security
- Upgraded jettison to 1.5.1 due to **CVE-2022-45685** and **CVE-2022-45693**
- Upgraded postgresql to 42.5.3 due to **CVE-2022-41946**
- Upgraded common-net to 3.9.0 due to **CVE-2021-37533**

### 1.8.11 - 2022-11-17
#### Security
- Upgraded postgresql to 42.4.2 due to **CVE-2022-31197**  and **CVE-2022-26520**
- Upgraded jackson-databind to 2.14.0 due to **CVE-2022-42004**
- Upgraded protobuf-java to 3.21.9 due to **CVE-2022-3171**
- Upgraded jettison to 1.5.1 due to **CVE-2022-40149** and **CVE-2022-40150**

### 1.8.10 - 2022-02-04
#### Security
- Updated mysql-connector-java to 8.0.28 due to [CVE-2022-21363](https://nvd.nist.gov/vuln/detail/CVE-2022-21363)
- Updated postgresql driver to 42.3.2 due to [CVE-2022-21724](https://nvd.nist.gov/vuln/detail/CVE-2022-21724)
- Updated protobuf to 3.19.4 due to [CVE-2021-22569](https://nvd.nist.gov/vuln/detail/CVE-2021-22569)

### 1.8.9 - 2022-01-25
#### Security
- Updated log4j2 to 2.17.1 due to [CVE-2021-44832](https://nvd.nist.gov/vuln/detail/CVE-2021-44832)

### 1.8.8 - 2021-12-23
#### Security
- Updated log4j2 to 2.17.0 due to [CVE-2021-45105](https://nvd.nist.gov/view/vuln/detail?vulnId=CVE-2021-45105)
- Updated jackson-mapper-asl to Jackson Databind v2.13.1 due to [CVE-2019-10172](https://nvd.nist.gov/vuln/detail/CVE-2019-10172)
- Updated mysql-connector-java to 8.0.27 due to [CVE-2021-2471](https://nvd.nist.gov/vuln/detail/CVE-2021-2471), [CVE-2019-2692](https://nvd.nist.gov/vuln/detail/CVE-2019-2692) and [CVE-2018-3258](https://nvd.nist.gov/vuln/detail/CVE-2018-3258)
- Updated httpclient to 4.5.13 due to [CVE-2020-13956](https://nvd.nist.gov/vuln/detail/CVE-2020-13956)
- Updated commons-dbcp2 to 4.5.13 due to [CWE-200](https://cwe.mitre.org/data/definitions/200.html)
- Updated postgresql driver to 42.3.1 dure to [CVE-2018-10936](https://nvd.nist.gov/view/vuln/detail?vulnId=CVE-2018-10936) and [CVE-2020-13692](https://nvd.nist.gov/vuln/detail/CVE-2020-13692)
- Update openjpa, commons-net, commons-codec and javax.servlet.jsp-api to the latest version

### 1.8.7 - 2021-12-15
#### Security
- Updated log4j2 to 2.16.0 due to [CVE-2021-45046](https://nvd.nist.gov/vuln/detail/CVE-2021-45046)

### 1.8.6 - 2021-12-13
#### Security
- Updated log4j2 to 2.15.0 due to [CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228)

### 1.8.5 - 2020-10-13
#### Fixed
- Upgrade to log4j2

### 1.8.4 - 2020-07-21
#### Fixed
- Fixed endless loop on startup if database is not available

### 1.8.3 - 2020-07-08
#### Fixed
- Fixed issues with edit patient, when Cross Origin Resource Sharing is active

### 1.8.2 - 2020-07-03
#### Fixed
- JSON representation of an empty control number field must be a string instead of a JSON object

### 1.8.1 - 2020-07-02
#### Fixed
- Editing any fields of a Patient with an empty control number leads to error message
- Stop retrying database connection if the login to DB failed

### 1.8.0 - 2020-06-03
#### Added
- Example configuration files for docker: docker-compose.user.yml and docker-compose.dev.yml
- SSL Configuration via reverse proxy in docker
- Privacy-preserving record linkage using Bloom filter instead of plain text
- Deployment via Docker (BETA)
- Development via Docker (BETA)
- Integrationtests with bitbucket-pipelines
- Matching with external IDs and/or IDATs (identifying data)
- Optional fields can now added or edited later
- Editing of external generated IDs
- Request all patient Id types (readAllPatientIdTypes)
- Request all patient ids (readAllPatientIds)
- ElasticIDGenerator - Create IDs with given ID length and vocabulary (formerly MasterIDGenerator)
- Delete Patient via REST-API
- Soundex-like-blocking - Many thanks to [Ziad Sehili](https://bitbucket.org/%7B087d1ddc-2490-4c54-a353-a5aaf2d0cfed%7D/) from the Leipzig University
#### Changed
- Port for remote debugging in docker container now always defaults to 1099
- Updated references and contributor list
#### Fixed
- Correction in token handling
- Use correct NULL representation of JSONObject
- Issues with multiple idtypes in same mainzelliste instance

### 1.7.0

This release introduces support for externally generated IDs. This feature introduces an incompatible API change; therefore the API version is bumped to 3.0. However, old API versions are still supported by means of the `mainzellisteApiVersion` header, i.e. this software release is fully backwards compatible. 

New features:

- Support for externally generated IDs. See the [Wiki page on this topic](https://bitbucket.org/medicalinformatics/mainzelliste/wiki/External-IDs) for further information on how to use this feature.
- The admin form for editing patient data now shows duplicates and potential duplicates of the patient.

Bug fixes:

- Trying to edit a field that is not listed as editable in the `editPatient` token lead to HTTP status code 400 (Bad Request) instead of the more suitable 401 (Unauthorized).
- Edit requests sometimes lead to an error message saying that date fields are missing even when the date was not be edited at all.
- Some API methods returned inappropriate status codes for certain error conditions. 
- All writing API methods are now synchronized to prevent race conditions.

### 1.6.2

Adds `<attachClasses>` configuration parameter to Maven WAR Plugin in order to make classes accessible as a library. 

###1.6.1

This is a bugfix release for restoring compatibility with Java 7. Version 1.6.0 does not compile with Java versions <8 due to an unimplemented interface method, which has a default implementation in Java 8.

Fix submitted by Stephan Rusch (Universitätsklinikum Freiburg).

###1.6.0

This release further enhances the mechanism to choose the UI language and includes some important fixes, notably the contribution of changes to prevent memory leaks contributed by Daniel Volk. Again, upgrading is possible from all previous releases by replacing the binary.   

#### New features:

- The language of the user forms can be set in the configuration file (e.g. `language = en`). This setting overrides all other means of setting the language.
- The default language is now always English (it used to be the server's system language).
- If the language is induced from the `Accept-Language` header, all languages listed therein are tried, respecting the preference order. Previously, if for example Albanian (or any other language for which no localization file is included) and German were listed in the header in this order, Mainzelliste did not consider the second choice (German) but used the server language.
- When a cross origin (CORS) request issues an origin domain not listed as acceptable source, Mainzelliste now cancels the request. Previously, it processed the request nevertheless and relied on the web browser blocking the response. 
- Trailing whitespace is now trimmed from configuration parameters, as this was a common source of errors.
- When printing the result of an ID request, the page order is omitted for a better print layout.
- The logo can now also be read from a relative path within the .war file or from the directory `META-INF/resources` within a .jar file on the class path (contributed by Daniel Volk, see pull request #32).
  

#### Bug fixes:

- When a text field was set to `null` via a PUT request in JSON format, the string `"null"` was saved in the database and returned upon reading the field. To fix this, `null` values are now converted to empty strings.
- Setting an empty value for field of type `IntegerField` failed with an exception.
- Fixed memory leaks caused by the Java preferences system and improper shutdown of resources (contributed by Daniel Volk, see pull request #36).
- Fixed logging during token creation and dependency errors in some IDEs (contributed by Daniel Volk, see pull request
  #34).  

#### Other changes:

- Added debug logging about weight generation during record linkage. 

###1.5.0

This release introduces a couple of new features and bug fixes, the addition of language selection via URL parameter being the only (backward compatible) change to the public API (now version 2.2). We recommend an upgrade to all users, which is possible from any earlier release without any steps necessary other than replacing the binary. The update is fully backwards compatible to all Mainzelliste versions down to 1.0, except for use cases with the requirement that future dates can be entered (these are now rejected by date validation).

####New features:

- A new field transformation, `StringTrimmer`, can be used to delete leading and trailing whitespace from a `PlainTextField`.
- The language of user forms can be set by providing the language code as URL parameter `language` (currently `de` and `en` are supported). If provided, this parameter overrides the choice of preferred languages in the `Accept-Language` header.
- Date validation rejects dates in the future.
- Application name and version are provided in responses and callback requests as HTTP header `Server` and `User-Agent`, respectively, in the format `Mainzelliste/x.y.z`.
- The implementation of the callback request ensures the use of state-of-the-art transport layer security (TLS) (contributed by Matthias Lemmer, see pull request #26).
- When configuration parameter `callback.allowSelfsigned` is set to `true`, self-signed certificates on the target host are accepted when making callback requests (contributed by Matthias Lemmer, see pull request #26).

####Bug fixes:

- The host name provided by the `Origin` header was checked against the configured list of hosts (configuration parameter `servers.allowedOrigins`) even if equal to the host of the Mainzelliste instance itself, i.e. treating a same-origin request like a cross-origin request (reported by Benjamin Gathmann).
- Requests with an invalid token (i.e. non-existent or of wrong type) lead to status code 400 (Bad Request), now 401 (Unauthorized) is returned.
- Fixed internationalization of title in result page shown after a patient has been created (patientCreated.jsp).

####Other changes:

- Changed data type annotation for `Patient#fieldsString` and `Patient#inputFieldsString` to `@Lob` for portable mapping of unbounded character strings to appropriate database types.
- Internal improvements in class `Persistor`.

###1.4.3

This is a backward compatible maintenance release and we recommend an upgrade to all users. Also, we strongly recommend to incorporate the changes in the configuration template into actual configuration files (see comments below).

####Changes in matching configuration:
- Fixes a typo in the proposed matching configuration. In existing configuration files based on the template (`mainzelliste.conf.default`), the value for `matcher.epilink.geburtsmonat.frequency` should be changed from `0.833` to `0.0833`. This change has to be made manually because the actually used configuration file resides outside of the distributed package. The location of the configuration file is defined in the context descriptor by parameter `de.pseudonymisierung.mainzelliste.ConfigurationFile` (see installation manual for details).
- Also, the proposed weight threshold for matches (`matcher.epilink.threshold_match`) has been raised from `0.9` to `0.95`. We also recommend to adopt this change in existing configuration files.

These changes prevent that a definitive match occurs for two records that differ in one component (day, month, year) of the date of birth, all other fields being equal (reported by Matthias Lemmer).

####Bug fixes:
- When creating an `addPatient` token, ID types were not checked when using data item `idTypes` (API version 2.x syntax) with declared API version missing or < 2.0.
- The configuration parameter for cross origin resource sharing and its explanation were missing in the configuration template.

###1.4.2

Fixes an encoding error in German language properties file. This version can be skipped by users who do not use the HTML interface or use their own JSP files.

###1.4.1

This is a bug fix release and we recommend an upgrade to all users. Upgrading is possible from every earlier release, and there are no steps necessary apart from replacing the binary.

####Bug fixes:

- When a patient had been edited, the updated data was not considered for record linkage until after restarting the application (reported by Benjamin Gathmann).
- POST /sessions/{sid}/tokens returned an invalid token object (reported by Matthias Lemmer).
- Confirming an unsure case failed due to missing api version in request URL (fixed by Benjamin Gathmann, see pull request #22).
- Date validation accepted some illegal dates when data was not entered through the select lists in the HTML form (fixed by Benjamin Gathmann, see pull request #24).
- Some Instances of EntityManager were not closed on errors, leading to a memory leak.


####Other changes:

- Removed references to deleted Javascript files (contributed by Benjamin Gathmann, see pull request #20).
- The version number is now read from pom.xml, i.e. this is the only place in the source code where the version number is set.
- All HTML forms have been converted to HTML 5 and validated using the W3C markup validation service (https://validator.w3.org/).
- Logging of Jersey class WebComponent has been limited to SEVERE to avoid excessive warning messages in use cases where POST /patients is used with an empty request body (see http://stackoverflow.com/questions/2011895/how-to-fix-jersey-post-request-parameters-warning).
- Field#build can now be called with null as initialization object, returning an empty field.
- Created tokens are logged entirely only in log level DEBUG.


### 1.4.0

This release implements API version 2.1, including the new features presented therein. For detailed information, we refer to the comprehensive API document (currently only available in German), which can be downloaded from the [project web site](http://www.mainzelliste.de). All preceding API versions are still supported.

As this release contains various bug fixes, we recommend an upgrade to all users, regardless of whether they plan to use the new features or not. Upgrading is possible from every earlier release, and there are no steps necessary apart from replacing the binary. As noted above, the update is fully backwards compatible to all Mainzelliste versions including 1.0.

#### New features:

- The user forms have been internationalized and are now available in English and German. Contributions for other languages are welcome: just send us the `MessageBundle_xx.properties` file for your language. The language is selected according to the `Accept-Language` header sent by the browser, with English being the default language if the requested language is not supported. The English translation was kindly provided by the project “European Management Platform for Childhood Interstitial Lung Diseases” (chILD-EU).
- Contact information and logo do not show “Universitätsmedizin Mainz” anymore but can be configured to identify the operator of the Mainzelliste instance in the user forms (see configuration parameters `operator.contact` and `operator.logo`).
- Patient data can be edited by means of an `editPatient` token and a PUT request.
- Sessions and tokens can be explicitly deleted by means of a DELETE request.
- POST requests can be used to perform other HTTP methods (notably PUT and DELETE). This technique, frequently called “method override”, allows for the use of additional methods other than GET and POST from HTML forms.
- Cross origin resource sharing (CORS) for simple GET requests. This makes it possible, for instance, to retrieve patient data via an AJAX request from a server in a different domain than the server that serves the web page. This requires configuration of the permissible hosts (see configuration parameter `servers.allowedOrigins`).
- For use cases including the redirect functionality, the result page that appears after creating a patient can be disabled (see configuration parameter `result.show`).
- In addition to specific IP addresses, IPv4 address ranges can be included in the list of addresses from which another server can get access (see configuration parameter `servers.{n}.allowedRemoteAdresses`)
- For all successful requests to add a patient (POST /patients), the timestamp is logged in the database (for future use cases).
- When making requests via the HTML interface, error messages are formatted as web pages.
- The result page that is shown after a patient has been added now lists all requested pseudonyms according to parameter `idTypes` in the token (contributed by Matthias Lemmer).

#### Bug fixes:

- The error message issued upon entering an ID of an unknown type stated the ID value instead of the ID type.
- The JSON output of requests to sessions and tokens did not match the API documentation.
- Token objects were not garbage collected, leading to memory problems in use cases where sessions exist for a long time and contain high numbers of tokens.
- Submitting the form to add a patient from Internet Explorer returned JSON data in some cases, causing an error message due to unrecognized content.
- The reserved word `value` appeared as a name in an SQL statement, which led to a database error when using Firebird.
- Various methods turned out to be not thread-safe, causing errors in use cases where concurrent requests occur.

#### Other changes:

- All Java classes now have complete Javadoc comments.
- The source code has been cleaned up; in particular unused code, obsolete comments and test methods have been removed.
- The filter name in the deployment descriptor (web.xml) has been changed from “Mainzer ID-Framework” to “Mainzelliste”.
- The error message upon a failed callback request (while adding a patient) has been made more concrete so that the specific reason, such as a mismatch between the declared and actually used API version, can be detected more easily (contributed by Matthias Lemmer).

### 1.3.2

#### Bugfix:

- Creation of database schema failed with ReportingSQLException (reported by Matthias Lemmer).

### 1.3.1

#### Bug fixes and improvements:

- GET request to /patients with invalid `readPatients` token caused a NullPointerException.
- Creation of `readPatients` tokens has been accelerated by optimizing the database query and using an index.
- Corrections in the template configuration file:
    - A trailing space in `callback.allowedFormat` has been removed.
    - Token type `readPatient` has been corrected to `readPatients`.

### 1.3.0

As of release 1.3.0, a comprehensive API document exists. It is available as a PDF document from the project web page (see above). Software version 1.3 supports the former (1.0) as well as the current (2.0) API version.

#### New features and API changes:

- IDs and IDAT of existing patients can be read by means of a `readPatients` token and a GET request.
- The id of the `createPatient` token can be injected into the redirect URL by using a designated template parameter.
- When creating an `addPatient` token, specific ID types that should be created can be specified as an array in the token parameter `idTypes`. This replaces the former syntax (providing a single ID type).
- The callback request transmits all IDs according to the types defined in the `addPatient` token.
- The same holds for the response to POST /patients when requesting a JSON result (`Accept: application/json`).

#### Bug fixes:

- A bug in the record linkage algorithm led to the situation that two patient records were considered a definite match when the last name agreed, the first name disagreed and the birth name was empty in both cases, provided that all other fields agreed (reported by Benjamin Gathmann).
- The session timeout set in the configuration file was interpreted as seconds instead of minutes.
- The format of the callback request did not match the documentation (reported by Michael Storck).
- The template configuration file did not define “ß” as a valid character (reported by Benjamin Gathmann).
- Deletion of invalidated sessions caused a ConcurrentModificationException (reported by Stephan Rusch).

### 1.2

#### New features:
- Sessions expire after not having been accessed for a configured time (see configuration parameter `sessionTimeout`).
- Fields can be defined without being used in the record linkage algorithm. This allows saving additional fields in the database.

#### Bug fixes:

- On the result page, the month of birth was rendered incorrectly (fixed by Daniel Volk).
- Deleting a non-existent session caused a NullPointerException.
- A bug in the weight calculation of EpilinkMatcher lead to unreasonable high matching weights in some cases (reported by Dirk Langner).

### 1.1

- Conversion from Eclipse project to Maven (contributed by Jens Schwanke).
- Added information to `context.xml.default concerning running the application in Netbeans.

#### Bug fixes:
- Wrong characters in template configuration file (reported by Maximilian Ataian).
- Wrong default path for configuration file.
- A bug in IDGeneratorMemory caused a database error due to a duplicate primary key.

### 1.0
- Initial release
