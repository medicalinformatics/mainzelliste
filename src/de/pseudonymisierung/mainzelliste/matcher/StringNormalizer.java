/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.matcher;

import de.pseudonymisierung.mainzelliste.PlainTextField;
import de.pseudonymisierung.mainzelliste.RecordTransformer;
import java.util.Map;
import java.util.Set;

/**
 * Performs the following normalization on strings:
 * <ul>
 *   <li> Trailing delimiters such as blanks, colons, dashes etc. are removed.</li>
 *   <li> German umlauts are replaced: ä -> ae, ö -> oe etc., ß -> ss </li>
 *   <li> Conversion to uppercase. </li>
 * </ul>
 *
 */
public class StringNormalizer extends FieldTransformer<PlainTextField, PlainTextField> {

	/** Delimiters to recognize when decomposing Names as Set.
	 * Used internally for efficient access to delimiters.
	 */
	private final Set<Character> delimiters;

	/**
	 * Mapping between umlauts and their replacement
	 * {ä, Ä, ö, Ö, ü, Ü, ß, á, Á, é, É, è, È}
	 * */
	public static final Map<Character, String> defaultChartReplacementMap = Map.ofEntries(
			Map.entry('\u00e4', "ae"),
			Map.entry('\u00c4', "AE"),
			Map.entry('\u00f6', "oe"),
			Map.entry('\u00d6', "OE"),
			Map.entry('\u00fc', "ue"),
			Map.entry('\u00dc', "UE"),
			Map.entry('\u00df', "ss"),
			Map.entry('\u00e1', "a"),
			Map.entry('\u00c1', "A"),
			Map.entry('\u00e9', "e"),
			Map.entry('\u00c9', "E"),
			Map.entry('\u00e8', "e"),
			Map.entry('\u00c8', "E")
	);

	public final Map<Character, String> chartReplacementMap;

	public StringNormalizer(RecordTransformer recordTransformer) {
		this(recordTransformer.getCharReplacements() , recordTransformer.getCharDelimiters());
	}

	public StringNormalizer(Map<Character, String> charReplacementMap, Set<Character> delimiters) {
		this.delimiters = delimiters == null || delimiters.isEmpty() ?
				RecordTransformer.defaultDelimiterChars : delimiters;
		this.chartReplacementMap = charReplacementMap == null || charReplacementMap.isEmpty() ?
				defaultChartReplacementMap : charReplacementMap;
	}

	/**
	 * Normalize a PlainTextField. Normalization includes:
	 * <ul>
	 * 	<li> removal of leading and trailing delimiters,
	 *  <li> conversion of Umlauts.
	 * 	<li> conversion to upper case,
	 * <ul>
	 */
	@Override
	public PlainTextField transform(PlainTextField input)
	{
		if (input == null) return null;
		if (input.getValue() == null) return new PlainTextField(null);
		if (input.getValue().isEmpty()) return new PlainTextField("");

		char[] inputChars = input.toString().toCharArray();

		// ignore leading and trailing delimiters
		int start, end;
		for (start = 0; start < inputChars.length && delimiters.contains(inputChars[start]); start++);
		for (end = inputChars.length - 1; end >= start && delimiters.contains(inputChars[end]); end--);

		StringBuilder result = new StringBuilder();
		// convert umlauts
		for (int i = start; i <= end; i++) {
			String replacement = chartReplacementMap.get(inputChars[i]);
			if(replacement != null) {
				result.append(replacement);
			} else {
				result.append(inputChars[i]);
			}
		}
		return new PlainTextField(result.toString().toUpperCase());
	}

	@Override
	public Class<PlainTextField> getInputClass()
	{
		return PlainTextField.class;
	}

	@Override
	public Class<PlainTextField> getOutputClass()
	{
		return PlainTextField.class;
	}
}
