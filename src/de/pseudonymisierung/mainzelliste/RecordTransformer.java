/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidConfigurationException;
import de.pseudonymisierung.mainzelliste.matcher.FieldTransformer;
import de.pseudonymisierung.mainzelliste.matcher.FieldTransformerChain;
import de.pseudonymisierung.mainzelliste.util.ConfigUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A RecordTransformer applies all configured FieldTranformers to the fields of
 * an input record.
 */
public class RecordTransformer {
	private static final Logger logger = LogManager.getLogger(RecordTransformer.class);

	public static final Set<Character> defaultDelimiterChars = Set.of(' ', '.', ':', ',', ';', '-', '\'');
	/**
	 * Map of field transformers. Keys are the field names, values the
	 * corresponding FieldTransformer objects.
	 */
	private Map<String, FieldTransformerChain> fieldTransformers;

	private Map<Character, String> charReplacements;

	private Set<Character> charDelimiters;

	/**
	 * Create an instance from the configuration.
	 *
	 * @param props
	 *            The configuration of the Mainzelliste instance as provides by
	 *            {@link Config}.
	 *
	 * @throws InternalErrorException
	 *             If an error occurs during initalization. A typical cause is
	 *             when a configured FieldTransformer class cannot be found on
	 *             the class path.
	 */
	@SuppressWarnings("unchecked")
	public RecordTransformer(Properties props) throws InternalErrorException {
		// deserialize char replacements:
		charReplacements = new Gson().fromJson(
				props.getProperty("transformers.replacement", "{}"),
				new TypeToken<HashMap<Character, String>>() {}.getType());

		charDelimiters = ConfigUtils.readValueAsSet(props, "transformers.delimiters");

		fieldTransformers = new HashMap<>();

		// Get names of fields from config vars.*
		Pattern p = Pattern.compile("^field\\.(\\w+)\\.type");
		java.util.regex.Matcher m;

		// Build map of comparators and map of frequencies from Properties
		for (Object key : props.keySet()) {
			m = p.matcher((String) key);
			if (m.find()) {
				String fieldName = m.group(1);
				String transformerProp = props.getProperty("field." + fieldName + ".transformers");
				if (transformerProp != null)
				{
					String[] transformers = transformerProp.split(",");
					FieldTransformerChain thisChain = new FieldTransformerChain();
					for (String thisTrans : transformers) {
						thisTrans = thisTrans.trim();
						try {
							FieldTransformer<Field<?>, Field<?>> tr = (FieldTransformer<Field<?>, Field<?>>)
									Class.forName("de.pseudonymisierung.mainzelliste.matcher." + thisTrans)
									.getConstructor(RecordTransformer.class).newInstance(this);
							thisChain.add(tr);
						} catch (Exception e) {
							InvalidConfigurationException configurationException = new InvalidConfigurationException("field." + fieldName + ".transformers", "failed init. field transformer. Reason:" + e.getMessage());
							logger.error(configurationException.getMessage());
							throw configurationException;
						}
					}
					this.fieldTransformers.put(fieldName, thisChain);
				}
			}
		}
	}

	/**
	 * Transforms a patient by transforming all of its fields. Fields for which
	 * no transformer is found (i.e. the field name is not in .keySet()) are
	 * passed unchanged, as well as IDs.
	 * @param input The record to transform.
	 * @return The transformed record.
	 */
	public Patient transform(Patient input) {
		Map<String, Field<?>> inFields = input.getFields();
		Patient output = new Patient(new HashSet<>(input.getIds()), new ArrayList<>(input.getAssociatedIdsList()));
		HashMap<String, Field<?>> outFields = new HashMap<>();
		/* iterate over input fields and transform each */
		for (Entry<String, Field<?>> field : inFields.entrySet()) {
			FieldTransformerChain transformer = this.fieldTransformers.get(field.getKey());
			outFields.put(field.getKey(), transformer != null ?
					transformer.transform(field.getValue()) : field.getValue().clone());
		}
		output.setFields(outFields);
		return output;
	}

	public Map<Character, String> getCharReplacements() {
		return charReplacements;
	}

	public Set<Character> getCharDelimiters() {
		return charDelimiters;
	}
}
