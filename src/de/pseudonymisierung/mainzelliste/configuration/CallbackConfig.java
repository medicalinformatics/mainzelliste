package de.pseudonymisierung.mainzelliste.configuration;

import java.util.List;
import java.util.Map;

public record CallbackConfig(
    String url,
    List<String> idTypes,
    List<String> fields,
    Map<String, String> headers
) {

}
