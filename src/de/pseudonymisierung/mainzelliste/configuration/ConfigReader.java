package de.pseudonymisierung.mainzelliste.configuration;

import de.pseudonymisierung.mainzelliste.exceptions.InvalidConfigurationException;
import de.pseudonymisierung.mainzelliste.util.ConfigUtils;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.net.ssl.SSLContext;
import org.apache.http.HttpHost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConfigReader {

  private static final Logger logger = LogManager.getLogger(ConfigReader.class);
  private final Properties properties;

  public ConfigReader(Properties properties) {
    this.properties = properties;
  }

  public Map<String, List<CallbackConfig>> readIDGeneratorsCallbacks(Collection<String> fieldTypes) {
    Map<String, List<CallbackConfig>> result = new HashMap<>();
    ConfigUtils.getVariableSubProperties(properties, "idgenerator")
        .forEach((idType, idGeneratorP) -> {
          if (!isIdTypeConfigured(idType))
            return;

          List<CallbackConfig> callbackConfigList = new ArrayList<>();
          result.put(idType, callbackConfigList);

          ConfigUtils.getVariableSubProperties(idGeneratorP, "callback")
              .forEach((callbackId, callbackP) -> {
                String url = callbackP.getProperty("url", "").trim();
                if(url.isEmpty())
                  throwAndLog(new InvalidConfigurationException("idgenerator." + idType
                      + ".callback." + callbackId + ".url", "callback url required"));
                if (result.values().stream().anyMatch( l ->
                    l.stream().anyMatch(a -> a.url().equalsIgnoreCase(url))
                )) {
                  throwAndLog(
                      new InvalidConfigurationException("idgenerator." + idType + ".callback."
                          + callbackId + ".url", "callback url '" + url + "' already exist"));
                }

                List<String> resultIds = ConfigUtils.readValue(callbackP, "resultIds");
                if (resultIds != null && resultIds.stream().anyMatch(i -> !isIdTypeConfigured(i))) {
                  throwAndLog(
                      new InvalidConfigurationException("idgenerator." + idType + ".callback."
                          + callbackId + ".resultIds", "idType '" + resultIds + "' not configured"));
                }

                List<String> resultFields = ConfigUtils.readValue(callbackP, "resultFields");
                if (resultFields != null && resultFields.stream().anyMatch(f -> !fieldTypes.contains(f))) {
                  throwAndLog(
                      new InvalidConfigurationException("idgenerator." + idType + ".callback."
                          + callbackId + ".resultFields", "field '" + resultFields + "' not configured"));
                }

                // ex. "headers.0.apiKey = changePass"
                Map<String, String> headers = ConfigUtils.getVariableSubPropertiesAsMap(callbackP,
                    "headers");
                callbackConfigList.add(new CallbackConfig(url, resultIds, resultFields, headers));
              });
        });
    return result;
  }

  public HttpClientBuilder initHttpClientBuilder() {
    try {
      SSLContextBuilder sslContextBuilder = new SSLContextBuilder();
      boolean allowSelfsigned = ConfigUtils.readValue(properties, "callback.allowSelfsigned", false);
      SSLContext sslContext = !allowSelfsigned ? SSLContexts.createSystemDefault() :
          sslContextBuilder.loadTrustMaterial(null, new TrustSelfSignedStrategy()).build();

      SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext,
          new String[]{"TLSv1", "TLSv1.1", "TLSv1.2"}, null,
          SSLConnectionSocketFactory.getDefaultHostnameVerifier());
      HttpClientBuilder builder = HttpClients.custom().setSSLSocketFactory(sslSocketFactory);

      //set proxy
      String proxyUrl = properties.getProperty("proxy.callback.url", "").trim();
      int proxyPort = ConfigUtils.readValue(properties,"proxy.callback.port", -1);
      if (!proxyUrl.isEmpty() && proxyPort > -1) {
        builder.setRoutePlanner(new DefaultProxyRoutePlanner(new HttpHost(proxyUrl, proxyPort)));
      } else if (proxyUrl.isEmpty() && proxyPort > -1) {
        throwAndLog(new InvalidConfigurationException("proxy.callback.url", " url required"));
      } else if (!proxyUrl.isEmpty()) {
        throwAndLog(new InvalidConfigurationException("proxy.callback.url", " port required"));
      }

      return builder;
    } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException ex) {
      throwAndLog(new InvalidConfigurationException("callback.allowSelfsigned", "failed to "
          + "initialise client transport layer security of apache HttpClientBuilder . reason:", ex));
      return null;
    }
  }

  protected boolean isIdTypeConfigured(String idType) {
    return Arrays.stream(this.properties.getProperty("idgenerators", "").split("[;,]"))
        .anyMatch(t -> t.trim().equals(idType));
  }

  private void throwAndLog(RuntimeException e) {
    logger.error(e.getMessage());
    throw e;
  }
}
