package de.pseudonymisierung.mainzelliste.permissions;

public enum PermissionEnum {
  READ_ALL_PATIENTS("readAllPatients"),
  READ_ALL_PATIENT_IDS("readAllPatientIds"),
  READ_ALL_PATIENT_ID_TYPES("readAllPatientIdTypes"),
  CALLBACK("callback");

  final String value;

  PermissionEnum(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
