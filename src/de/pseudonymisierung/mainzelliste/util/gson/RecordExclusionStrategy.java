package de.pseudonymisierung.mainzelliste.util.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import java.util.Objects;

public class RecordExclusionStrategy implements ExclusionStrategy {

  @Override
  public boolean shouldSkipField(FieldAttributes fieldAttributes) {
    return !Objects.isNull(fieldAttributes.getAnnotation(Exclude.class));
  }

  @Override
  public boolean shouldSkipClass(Class<?> aClass) {
    return false;
  }
}
