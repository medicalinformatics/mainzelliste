/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.pseudonymisierung.mainzelliste.util;

import ca.uhn.fhir.context.BaseRuntimeChildDefinition;
import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.RuntimeResourceDefinition;
import ca.uhn.fhir.parser.IParser;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.DomainResource;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.OperationOutcome.IssueSeverity;
import org.hl7.fhir.r4.model.OperationOutcome.IssueType;

public class FHIRWriter {

  private FhirContext ctx;
  private final Logger logger = LogManager.getLogger(FHIRWriter.class);

  private IParser parser;
  private Bundle bundle = null;
  private DomainResource resource = null;
  private boolean isBundle;

  /**
   * Create a new FHIRWriter instance for FhirContext. If not Context is set, R4 will be used by
   * default.
   *
   * @param ctx
   */
  public FHIRWriter(FhirContext ctx) {
    if (ctx != null) {
      this.ctx = ctx;
    } else {
      this.ctx = FhirContext.forR4();
    }
    this.parser = this.ctx.newJsonParser();
  }

  /**
   * Call the `encodeResourceToString` method of the Parser in this FHIRWriter, returns either the
   * bundle or the resource as an encoded String.
   *
   * @return String encoded resource or bundle, whichever is set currently.
   */
  public String encodeToString() {
    if (isBundle) {
      return this.parser.encodeResourceToString(this.bundle);
    }
    return this.parser.encodeResourceToString(this.resource);
  }

  /**
   * Puts a resource into this writer, effectively removing any bundle present.
   *
   * @param resource DomainResource to set to this FHIRWriter.
   * @return DomainResource that has been set to this FHIRWriter.
   */
  public DomainResource putResource(DomainResource resource) {
    this.resource = resource;
    this.bundle = null;
    this.isBundle = false;
    return this.resource;
  }

  /**
   * Puts a bundle into this writer, effectively removing any resource present.
   *
   * @param bundle Bundle to set to this FHIRWriter.
   * @return Bundle that has been set to this FHIRWriter.
   */
  public Bundle putBundle(Bundle bundle) {
    this.bundle = bundle;
    this.resource = null;
    this.isBundle = true;
    return this.bundle;
  }

  @Override
  public String toString() {
    return this.encodeToString();
  }

  /**
   * Returns the FHIR Parser from the FhirContext based on `_format`, `_summary` and `_pretty` query
   * parameters as well as the Accept HTTP header.
   *
   * @param headerAcceptValue String the HTTP header `Accept` value.
   * @param format MultivaluedMap<String, String> the parameters map from the UriInfo context
   *     variable.
   * @return IParser the parser matching the input from Accept, `_format`, `_summary` and `_pretty`.
   */
  public IParser setParser(String headerAcceptValue, MultivaluedMap<String, String> parameters) {
    // Format
    if (parameters != null && parameters.containsKey("_format")) {
      headerAcceptValue = parameters.getFirst("_format");
    }
    if (headerAcceptValue != null) {
      if (headerAcceptValue.contains("xml")) {
        this.parser = ctx.newXmlParser();
      }
      if (headerAcceptValue.contains("json")) {
        this.parser = ctx.newJsonParser();
      }
      if (headerAcceptValue.contains("ttl") || headerAcceptValue.contains("turtle")) {
        this.parser = ctx.newRDFParser();
      }
    }

    // Pretty Print
    if (parameters != null && parameters.containsKey("_pretty")) {
      this.parser.setPrettyPrint(Boolean.parseBoolean(parameters.getFirst("_pretty")));
    }

    // Summary (true or false)
    if (parameters != null && parameters.containsKey("_summary")) {
      this.parser.setSummaryMode(Boolean.parseBoolean(parameters.getFirst("_summary")));
    }

    return this.parser;
  }

  /**
   * Sets an element filter (includes _summary count, text and data params) to this writer. Needs
   * the QueryParameters map that is supplied by UriInfo context variables.
   *
   * @param parameters MultivaluedMap<String, String> instance that supplied the URI query
   *     parameters
   * @return Returns and sets the Parser according to any element filter related entry in
   *     `parameters`.
   */
  public IParser setElementFilter(MultivaluedMap<String, String> parameters) {
    if (isBundle) {
      // is summary == count on a bundle, remove the Bundle.entry list and set the
      // count
      if (parameters != null
          && parameters.containsKey("_summary")
          && parameters.getFirst("_summary").equals("count")) {
        this.bundle.setTotal(this.bundle.getEntry().size());
        this.bundle.setEntry(new ArrayList<BundleEntryComponent>());
        return this.parser;
      }

      // we handle bundles without count by iterating through each entry in the bundle
      // and setting it as our temporary resource in this FHIRWriter, allowing us to
      // call this method recursively.
      this.isBundle = false;
      for (BundleEntryComponent bec : this.bundle.getEntry()) {
        this.resource = (DomainResource) bec.getResource();
        this.setElementFilter(parameters);
      }
      this.isBundle = true;
      this.resource = null;
      return this.parser;
    }

    // Summary (text, data)
    RuntimeResourceDefinition rrd = this.ctx.getResourceDefinition(this.resource);
    if (parameters != null && parameters.containsKey("_summary")) {
      if (parameters.getFirst("_summary").equals("text")) {
        for (BaseRuntimeChildDefinition child : rrd.getChildren()) {
          if (child.getMin() > 0) {
            // keep
          } else if (child.getElementName().equals("text")) {
            // keep
          } else if (child.getElementName().equals("id")) {
            // keep
          } else if (child.getElementName().equals("meta")) {
            // keep
          } else {
            // remove anything else
            child.getMutator().setValue(this.resource, null);
          }
        }
      } else if (parameters.getFirst("_summary").equals("data")) {
        // remove the Text element
        this.resource.setText(null);
      }
    }

    // Elements
    if (parameters != null && parameters.containsKey("_elements")) {
      List<String> keepElements = Arrays.asList(parameters.getFirst("_elements").split(","));
      for (BaseRuntimeChildDefinition child : rrd.getChildren()) {
        if (child.getMin() > 0) {
          // keep
        } else if (keepElements.contains(child.getElementName())) {
          // keep
        } else {
          child.getMutator().setValue(this.resource, null);
        }
      }
    }

    return this.parser;
  }

  /**
   * Returns the Location URI of the Resource that is build using this FHIRWriter instance.
   *
   * @param uriInfoContext UriInfo context instance that supplies the UriBuilder to add the location
   *     URI to the base URI.
   * @return URI instance of the Location of `resource` (bundles yield `null`, as they do not have a
   *     location in Mainzelliste).
   */
  public URI getLocationUri(UriInfo uriInfoContext) {
    if (isBundle) {
      // we do not store an id for Bundles
      return null;
    }
    URI resourceUri = uriInfoContext.getRequestUriBuilder().path(this.resource.getId()).build();
    return resourceUri;
  }

  /**
   * Generic function to allow creating all types of standardized OperationOutcome Resources. This
   * method uses a CodeableConcept to describe the error.
   *
   * @param writer FHIRWriter instance to write to (parser should be already set).
   * @param statusCode int HTTP Status Code to use in the Response.
   * @param issueSeverity IssueSeverity value to set to the OperationOutcome.Issue.
   * @param issueType IssueType value to set to the OperationOutcome.Issue.
   * @param errorCode String error code from
   *     http://terminology.hl7.org/CodeSystem/operation-outcome.
   * @param display String display message from
   *     http://terminology.hl7.org/CodeSystem/operation-outcome.
   * @return Response instance ready to return containing an OperationOutcome Resource.
   */
  public Response genericErrorResponse(
      int statusCode,
      IssueSeverity issueSeverity,
      IssueType issueType,
      String errorCode,
      String display) {
    this.putResource(
        this.genericErrorOperationOutcome(
            statusCode, issueSeverity, issueType, errorCode, display));
    return Response.status(statusCode).entity(this.encodeToString()).build();
  }

  public OperationOutcome genericErrorOperationOutcome(
      int statusCode,
      IssueSeverity issueSeverity,
      IssueType issueType,
      String errorCode,
      String display) {
    OperationOutcome outcome = new OperationOutcome();
    CodeableConcept error = new CodeableConcept();
    error
        .addCoding()
        .setSystem("http://terminology.hl7.org/CodeSystem/operation-outcome")
        .setCode(errorCode)
        .setDisplay(display);
    outcome.addIssue().setSeverity(issueSeverity).setCode(issueType).setDetails(error);
    return outcome;
  }

  /**
   * Generic function to allow creating all types of standardized OperationOutcome Resources. This
   * method uses a String to explain the error further.
   *
   * @param writer FHIRWriter instance to write to (parser should be already set).
   * @param statusCode int HTTP Status Code to use in the Response.
   * @param issueSeverity IssueSeverity value to set to the OperationOutcome.Issue.
   * @param issueType IssueType value to set to the OperationOutcome.Issue.
   * @param diagnostics String diagnostics to set to the OperationOutcome.
   * @return Response instance ready to return containing an OperationOutcome Resource.
   */
  public Response genericErrorResponse(
      int statusCode, IssueSeverity issueSeverity, IssueType issueType, String diagnostics) {
    this.putResource(
        this.genericErrorOperationOutcome(statusCode, issueSeverity, issueType, diagnostics));
    return Response.status(statusCode).entity(this.encodeToString()).build();
  }

  public OperationOutcome genericErrorOperationOutcome(
      int statusCode, IssueSeverity issueSeverity, IssueType issueType, String diagnostics) {
    OperationOutcome outcome = new OperationOutcome();
    outcome.addIssue().setSeverity(issueSeverity).setCode(issueType).setDiagnostics(diagnostics);
    return outcome;
  }
}
