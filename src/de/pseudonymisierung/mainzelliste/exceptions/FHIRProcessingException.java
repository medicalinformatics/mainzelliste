package de.pseudonymisierung.mainzelliste.exceptions;

import de.pseudonymisierung.mainzelliste.util.FHIRWriter;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.OperationOutcome.IssueSeverity;
import org.hl7.fhir.r4.model.OperationOutcome.IssueType;

import ca.uhn.fhir.context.FhirContext;

public class FHIRProcessingException extends WebApplicationException {

  private static final long serialVersionUID = 120712987209L;

  int statusCode;
  IssueSeverity issueSeverity;
  IssueType issueType;
  String errorCode;
  String display;
  String diagnostics;

  boolean hasErrorCode;

  public FHIRProcessingException(
      int statusCode,
      IssueSeverity issueSeverity,
      IssueType issueType,
      String errorCode,
      String display) {
    this.statusCode = statusCode;
    this.issueSeverity = issueSeverity;
    this.issueType = issueType;
    this.errorCode = errorCode;
    this.display = display;
    this.hasErrorCode = true;
  }

  public FHIRProcessingException(
      int statusCode, IssueSeverity issueSeverity, IssueType issueType, String diagnostics) {
    this.statusCode = statusCode;
    this.issueSeverity = issueSeverity;
    this.issueType = issueType;
    this.diagnostics = diagnostics;
    this.hasErrorCode = false;
  }

  public Response getResponse() {
    return this.getResponse(new FHIRWriter(FhirContext.forR4()));
  }

  public Response getResponse(FHIRWriter writer) {
    if (this.hasErrorCode) {
      return writer.genericErrorResponse(
          this.statusCode, this.issueSeverity, this.issueType, this.errorCode, this.display);
    } else {
      return writer.genericErrorResponse(
          this.statusCode, this.issueSeverity, this.issueType, this.diagnostics);
    }
  }

  public OperationOutcome getOperationOutcomeResource(FHIRWriter writer) {
    if (this.hasErrorCode) {
      return writer.genericErrorOperationOutcome(
          this.statusCode, this.issueSeverity, this.issueType, this.errorCode, this.display);
    } else {
      return writer.genericErrorOperationOutcome(
          this.statusCode, this.issueSeverity, this.issueType, this.diagnostics);
    }
  }

  public int getStatus() {
    return this.statusCode;
  }
}
