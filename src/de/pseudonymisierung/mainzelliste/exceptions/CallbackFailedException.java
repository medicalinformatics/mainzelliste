package de.pseudonymisierung.mainzelliste.exceptions;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response.Status;
import java.util.function.Consumer;

public class CallbackFailedException extends WebApplicationException {

  public CallbackFailedException(String reason, String callbackUrl, Status status,
      Consumer<String> logger) {
    super("Request to callback url '[" + callbackUrl + "]' failed. Reason: " + reason, status);
    logger.accept(this.getMessage());
  }
}
