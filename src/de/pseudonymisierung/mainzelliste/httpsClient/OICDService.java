package de.pseudonymisierung.mainzelliste.httpsClient;


import de.pseudonymisierung.mainzelliste.Config;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class OICDService {

  private static final String USERINFOENDPOINTKEY = "userinfo_endpoint";
  private static final Logger logger = LogManager.getLogger(OICDService.class);
  private static final String METADATAURL = ".well-known/openid-configuration";

  /**
   * Return the UserInformation provided by the Userinfo endpoint from the openId Provider
   *
   * @return User information as JSONObject
   */
  public static JSONObject getIdTokenFromUserInfoEndpoint(String accessToken,
      String userInfoEndpointUrl, boolean noProxy) throws IOException {
    JSONObject idToken = new JSONObject();
    if (!userInfoEndpointUrl.isEmpty()) {
      MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
      headers.putSingle(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
      headers.putSingle(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
      idToken = getData(userInfoEndpointUrl, headers, noProxy);
      logger.debug("Userinfo: {}", idToken);
    }
    return idToken;
  }


  /**
   * Retrieves the Userinfo endpoint Url from the OpenId Configuration
   *
   * @return the Url to the Userinfo endpoint if the attribute exist in the metadata of the
   * authorization server, otherwise null
   */
  public static String getUserInfoEndPointURL(String iss, boolean noProxy) throws IOException {
    String url = getSpecificUrl(iss, METADATAURL);
    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
    headers.putSingle(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
    JSONObject metaData = getData(url, headers, noProxy);
    try {
      return metaData.getString(USERINFOENDPOINTKEY);
    } catch (JSONException e) {
      logger.warn(e);
      return "";
    }
  }

  private static JSONObject getData(String url, MultivaluedMap<String, Object> headers, boolean noProxy) throws IOException {
    Response response = (noProxy ? Config.instance.getHttpConnectorNoProxy() : Config.instance.getHttpConnector())
        .getJaxRsClient(url)
        .target(url)
        .request()
        .headers(headers)
        .get();
    JSONObject data;
    try {
      String responseString = response.readEntity(String.class);
      data = responseString .isEmpty() ? new JSONObject() : new JSONObject(responseString);
      logger.debug("{} Response: {}", url, data);
    } catch (JSONException e) {
      logger.error("Error parsing Response from " + url, e);
      throw new IOException(e);
    }
    return data;
  }

  /**
   * Builds a valid Url path
   *
   * @param base     Base Path of the Url
   * @param endpoint Path endpoint
   * @return the full path which contains base + appendix
   */
  private static String getSpecificUrl(String base, String endpoint) {
    // Removes Path component
    if (base.matches(".*/")) {
      return base + endpoint;
    } else {
      return base + "/" + endpoint;
    }
  }
}
