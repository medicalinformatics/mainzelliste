package de.pseudonymisierung.mainzelliste.api;

import java.util.List;
import java.util.Map;

public record ReadPatientResponse(List<IdResponse> ids, Map<String, String> fields, List<String> idTypes) {

}
