package de.pseudonymisierung.mainzelliste.auth.authorizationServer;

import de.pseudonymisierung.mainzelliste.auth.credentials.AuthorizationServerCredentials;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Represents the OAuth-Server
 */
public class OAuthAuthorizationServer implements AuthorizationServer {

  protected String issuer;
  protected String issuerInternalUrl;
  protected String metaDataUrl;

  protected String name;


  public OAuthAuthorizationServer(String issuer, String name) {
    this.issuer = issuer;
    this.metaDataUrl = ".well-known/oauth-authorization-server";
    this.name = name;
  }

  public OAuthAuthorizationServer(String issuer, String name, String internalIssBaseUrl) {
    this(issuer, name);
    if(internalIssBaseUrl == null || internalIssBaseUrl.isEmpty()){
      return;
    }
    try {
      URL issuerUrl = new URL(issuer);
      this.issuerInternalUrl = internalIssBaseUrl + issuerUrl.getPath()
          + (issuerUrl.getQuery() != null ? issuerUrl.getQuery() : "");
    } catch (MalformedURLException e) {
      throw new IllegalArgumentException("invalid issuer url " + issuer);
    }
  }

  public String getId(){return this.issuer; }

  public String getName(){return this.name;}

  public String getIssuerInternalUrl() {
    return issuerInternalUrl;
  }

  @Override
  public boolean authorize(AuthorizationServerCredentials oidcCredentials) {
    return this.issuer.equals(oidcCredentials.getServerId());
  }

}
