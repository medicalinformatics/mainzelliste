package de.pseudonymisierung.mainzelliste.auth.oidc;

import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.auth.authorizationServer.AuthorizationServer;
import de.pseudonymisierung.mainzelliste.auth.credentials.OIDCCredentials;
import de.pseudonymisierung.mainzelliste.auth.jwt.decodedJWT.IDecodedJWT;
import de.pseudonymisierung.mainzelliste.auth.jwt.decoder.Auth0JWTDecoder;
import de.pseudonymisierung.mainzelliste.httpsClient.OICDService;
import java.io.IOException;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

/**
 * Adapter which formats OIDC Properties to an authenticator conform format
 */
public class OIDCPropertiesAdapter {

  private static final Logger logger = LogManager.getLogger(OIDCPropertiesAdapter.class);

  /**
   * Returns the user claims provided by the authorization server
   *
   * @param accessToken the JWT encoded access token
   * @return the user claims provided by the authorization server
   */
  public static OIDCCredentials getOIDCInformationsByAccessToken(String accessToken) {
    try {
      logger.debug("Try to decode access token: " + accessToken);
      IDecodedJWT jwtPayload = new Auth0JWTDecoder().decode(accessToken);
      String iss = jwtPayload.getIssuer();
      Optional<AuthorizationServer> authorizationServer = Config.instance.getAuthorizationServers().find(iss);
      if (authorizationServer.isEmpty()) {
        logger.info("Issuer could not been verified: {}", iss);
        return null;
      }

      //override iss url if an internal base url was set
      boolean noProxy = false;
      if(authorizationServer.get().getIssuerInternalUrl() != null) {
        noProxy = true;
        iss = authorizationServer.get().getIssuerInternalUrl();
      }

      String userInfoEndpointUrl = OICDService.getUserInfoEndPointURL(iss, noProxy);
      JSONObject idToken = OICDService
          .getIdTokenFromUserInfoEndpoint(accessToken, userInfoEndpointUrl, noProxy);
      return new OIDCCredentials(idToken, jwtPayload);
    } catch (IOException ioException) {
      return null;
    }
  }
}
