/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.common;

import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.common.AbstractSearchCondition.Operator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractSearchConditions<O extends Operator> extends
    AbstractSearchCondition<List<AbstractSearchCondition<?, ?>>, O> {

  protected AbstractSearchConditions(O operator) {
    super(new ArrayList<>(), operator);
  }

  public <A extends AbstractSearchConditions<O>> A add(AbstractSearchCondition<?, ?> condition) {
    if (condition instanceof AbstractComparisonSearchCondition<?, ?>
        || condition instanceof AbstractSearchConditions<?> conditions && !conditions.isEmpty()) {
      this.value.add(condition);
    }
    return (A) this;
  }

  public boolean isEmpty() {
    return value.isEmpty();
  }

  protected StringBuilder createStringBuilder(StringBuilder stringBuilder, boolean useParenthesis,
      boolean isPostgres, List<Object> queryParameter) {
    if (useParenthesis) {
      stringBuilder.append("(");
    }
    Iterator<AbstractSearchCondition<?, ?>> iterator = value.iterator();
    while (iterator.hasNext()) {
      AbstractSearchCondition<?, ?> currentValue = iterator.next();
      stringBuilder.append(queryParameter == null ? currentValue.buildSearchConditions(isPostgres) :
          currentValue.buildSearchConditions(isPostgres, queryParameter));
      if (iterator.hasNext()) {
        stringBuilder.append(" ").append(getOperator().getValue(isPostgres)).append(" ");
      }
    }
    if (useParenthesis) {
      stringBuilder.append(")");
    }
    return stringBuilder;
  }

  @Override
  public String buildSearchConditions(boolean isPostgres) {
    return buildSearchConditions(isPostgres, null);
  }

  @Override
  public String buildSearchConditions(boolean isPostgres, List<Object> queryParameter) {
    return createStringBuilder(new StringBuilder(), this.value.size() > 1,
        isPostgres, queryParameter).toString();
  }
}
