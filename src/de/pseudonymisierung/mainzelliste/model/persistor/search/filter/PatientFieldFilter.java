/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.model.persistor.search.filter;

import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.common.SqlComparisonSearchCondition;
import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.common.SqlComparisonSearchCondition.SqlOperator;
import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.common.SqlSearchConditions;
import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.common.SqlSearchConditions.SqlLogicalOperator;
import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.mapper.PatientFieldSearchConditionField;
import jakarta.ws.rs.core.MultivaluedMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

public class PatientFieldFilter {

  private static final String SEARCH_PARAM_FIELD = "field";
  protected final SqlSearchConditions searchConditions;

  private final Map<String, String> patientFieldMap = new HashMap<>();


  public PatientFieldFilter(MultivaluedMap<String, String> queryParameters,
      Map<String, PatientFieldSearchConditionField> idatSqlSearchConditionFieldMap) {
    // check if a search parameter field was provided
    if (queryParameters.containsKey(SEARCH_PARAM_FIELD)) {
      searchConditions = new SqlSearchConditions(SqlLogicalOperator.OR);
      Optional.ofNullable(queryParameters.getFirst(SEARCH_PARAM_FIELD))
          .filter(StringUtils::isNotBlank)
          .map(v -> v.trim().toLowerCase())
          .filter(v -> !v.isEmpty())
          .ifPresent(v -> idatSqlSearchConditionFieldMap.forEach((k, c) -> {
                searchConditions.add(new SqlComparisonSearchCondition(v, SqlOperator.LIKE, c));
                patientFieldMap.put(k, v);
              })
          );
    } else {
      searchConditions = new SqlSearchConditions(SqlLogicalOperator.AND);
      for (Entry<String, PatientFieldSearchConditionField> entry : idatSqlSearchConditionFieldMap.entrySet()) {
        Optional.ofNullable(queryParameters.getFirst(entry.getKey()))
            .filter(StringUtils::isNotBlank)
            .map(v -> v.trim().toLowerCase())
            .filter(v -> !v.isEmpty())
            .ifPresent(v -> {
              searchConditions.add(
                  new SqlComparisonSearchCondition(v, SqlOperator.LIKE,
                      entry.getValue()));
              patientFieldMap.put(entry.getKey(), v);
            });
      }
    }
  }

  public String buildSearchConditions(boolean isPostgres, List<Object> queryParameter) {
    return searchConditions.isEmpty() ? ""
        : searchConditions.buildSearchConditions(isPostgres, queryParameter);
  }

  public boolean isEmpty() {
    return searchConditions.isEmpty();
  }

  public Map<String, String> getPatientFieldMap() {
    return patientFieldMap;
  }
}
