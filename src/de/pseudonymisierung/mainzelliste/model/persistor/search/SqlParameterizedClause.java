package de.pseudonymisierung.mainzelliste.model.persistor.search;

import java.util.List;

public record SqlParameterizedClause(String whereConditions, List<Object> queryParameters) {

}
