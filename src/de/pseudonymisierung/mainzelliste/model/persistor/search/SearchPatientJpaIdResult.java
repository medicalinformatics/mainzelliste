package de.pseudonymisierung.mainzelliste.model.persistor.search;

import java.util.List;

public record SearchPatientJpaIdResult(
    String patientJpaIds,
    long totalCount,
    List<Integer> patientJpaIdsInt
) {

}
