package de.pseudonymisierung.mainzelliste.model.persistor.search;

import de.pseudonymisierung.mainzelliste.Patient;
import java.util.List;

public record SearchPatientResult(
    List<Patient> patients,
    long totalCount
) {

}
