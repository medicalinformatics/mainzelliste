package de.pseudonymisierung.mainzelliste.model.persistor.search;

import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.PatientFieldFilter;
import de.pseudonymisierung.mainzelliste.webservice.token.ReadPatientsToken.PatientSearchId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class PersistorSearchUtils {

  public static SqlParameterizedClause generateSqlParameterizedClause(boolean isPostgres,
      List<String> idTypes, PatientFieldFilter patientFieldFilter) {

    List<Object> queryParameters = new ArrayList<>();

    // prepare patient field filter
    String patientFieldConditions = patientFieldFilter.buildSearchConditions(isPostgres, queryParameters);

    // prepare id types where clause
    StringBuilder idTypesConditions = new StringBuilder();
    if (idTypes != null && !idTypes.isEmpty()) {
      idTypesConditions.append("ID.type in (");
      int initialSize = queryParameters.size() + 1;
      int i = queryParameters.size() + 1;
      for (String idType : idTypes) {
        idTypesConditions.append(i > initialSize ? "," : "").append("?").append(i++);
        queryParameters.add(idType);
      }
      idTypesConditions.append(") ");
      if (!patientFieldConditions.isEmpty()) {
        idTypesConditions.append("and ");
      }
    }
    idTypesConditions.append(patientFieldConditions);

    return new SqlParameterizedClause(idTypesConditions.toString(), queryParameters);
  }

  public static SqlParameterizedClause generateSqlParameterizedClause(boolean isPostgres,
      Collection<PatientSearchId> patientSearchIds, PatientFieldFilter patientFieldFilter) {
    List<Object> queryParameters = new ArrayList<>();

    // prepare patient field filter
    String patientFieldConditions = patientFieldFilter.buildSearchConditions(isPostgres, queryParameters);

    //prepare search ids
    Map<String, Set<String>> searchIdsMap = new HashMap<>();
    for (PatientSearchId patientSearchId : patientSearchIds) {
      ID searchId = patientSearchId.getBaseId();
      searchIdsMap.computeIfAbsent(searchId.getType(), k -> new HashSet<>()).add(searchId.getIdString());
    }

    // prepare id types where clause
    StringBuilder idsConditions = new StringBuilder();
    int i = queryParameters.size() + 1;
    for (Entry<String, Set<String>> entry : searchIdsMap.entrySet()) {
      if (!idsConditions.isEmpty()) {
        idsConditions.append(" or ");
      }
      idsConditions.append("(ID.type = ").append("?").append(i++);
      queryParameters.add(entry.getKey());
      int initialSize = queryParameters.size() + 1;
      idsConditions.append(" and ID.idString in (");
      for (String idString : entry.getValue()) {
        idsConditions.append(i > initialSize ? "," : "").append("?").append(i++);
        queryParameters.add(idString);
      }
      idsConditions.append(")) ");
      if (!patientFieldConditions.isEmpty()) {
        idsConditions.append("and ");
      }
    }
    idsConditions.append(patientFieldConditions);

    return new SqlParameterizedClause(idsConditions.toString(), queryParameters);
  }
}
