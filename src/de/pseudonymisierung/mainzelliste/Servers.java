/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free 
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it 
 * with Jersey (https://jersey.java.net) (or a modified version of that 
 * library), containing parts covered by the terms of the General Public 
 * License, version 2.0, the licensors of this Program grant you additional 
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste;

import de.pseudonymisierung.mainzelliste.auth.authenticator.ApiKeyAuthenticator;
import de.pseudonymisierung.mainzelliste.auth.authenticator.AuthenticationEum;
import de.pseudonymisierung.mainzelliste.auth.authenticator.Authenticator;
import de.pseudonymisierung.mainzelliste.auth.credentials.ApiKeyCredentials;
import de.pseudonymisierung.mainzelliste.auth.credentials.OIDCCredentials;
import de.pseudonymisierung.mainzelliste.auth.oidc.OIDCPropertiesAdapter;
import de.pseudonymisierung.mainzelliste.configuration.claimConfiguration.ClaimConfigurations;
import de.pseudonymisierung.mainzelliste.configuration.permission.ExtendedPermissions;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidTokenException;
import de.pseudonymisierung.mainzelliste.exceptions.UnauthorizedException;
import de.pseudonymisierung.mainzelliste.permissions.PermissionEnum;
import de.pseudonymisierung.mainzelliste.requester.ClientList;
import de.pseudonymisierung.mainzelliste.requester.Requester;
import de.pseudonymisierung.mainzelliste.utils.AuthenticationUtils;
import de.pseudonymisierung.mainzelliste.webservice.AddPatientToken;
import de.pseudonymisierung.mainzelliste.webservice.Token;
import de.pseudonymisierung.mainzelliste.webservice.commons.ExtendedPermissionUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.regex.Pattern;
import org.apache.commons.net.util.SubnetUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Keeps track of servers, i.e. each communication partner that is not a user.
 * Implemented as a singleton object, which can be referenced by
 * Servers.instance.
 */
public enum Servers {

	/** The singleton instance. */
	instance;

	/**
	 * Represents one registered server.
	 */
	class Server extends Requester {

		/** Remote IP addresses that are accepted for requests by this server. */
		Set<String> allowedRemoteAdresses;
		/**
		 * Remote IP address ranges that are accepted for requests by this
		 * server.
		 */
		List<SubnetUtils> allowedRemoteAdressRanges;

		public Server(Set<String> permissions,
				Authenticator authenticator, String apiKey, String name) {
			super(permissions, authenticator, apiKey,name);
		}

	}

	/** All registerd servers. */
	private final Map<String, Server> servers = new HashMap<String, Server>();
	/**All registerd users  */
	private final ClientList clientList = new ClientList();
	/** All currently active sessions, identified by their session ids. */
	private final Map<String, Session> sessions = new HashMap<String, Session>();
	/** All currently valid tokens, identified by their token ids. */
	private final Map<String, Token> tokensByTid = new HashMap<String, Token>();
	/** All Remote IPs with valid tokens by token ids. */
	private final Map<String, String> IPsByTid = new HashMap<String, String>();


	/** Time of inactivity after which a session is invalidated. */
	private final long sessionTimeout;

	/** The regular time interval after which to check for timed out sessions */
	private final long sessionCleanupInterval = 60000;

	/** The session cleanup timer. */
	private final Timer sessionsCleanupTimer;

	/** The loggging instance. */
	Logger logger = LogManager.getLogger(Servers.class);

	/**
	 * Creates the singleton instance. Reads configuration properties and
	 * initializes the object.
	 */
	private Servers() {
		// read Server configuration from mainzelliste.conf
		Properties props = Config.instance.getProperties();
		createServers(props);

		if(servers.size() == 0) {
			logger.error("No servers or users added. Is your config complete?");
		}

		if (Config.instance.getProperty("debug") == "true")
		{
			Token t = new AddPatientToken();
			tokensByTid.put(t.getId(), t);
			//set localhost for debugging
			IPsByTid.put(t.getId(), "127.0.0.1");
		}

		// Read session timeout (maximum time a session can be inactive) from
		// config
		String sessionTimeout = Config.instance.getProperty("sessionTimeout");
		if (sessionTimeout == null) {
			this.sessionTimeout = 600000; // 10 min
		} else {
			try {
				this.sessionTimeout = Long.parseLong(sessionTimeout) * 60000;
				if (this.sessionTimeout <= 0)
					throw new NumberFormatException();
			} catch (NumberFormatException e) {
				throw new Error("Invalid session timout: " + sessionTimeout + ". Please specify a positive whole number.");
			}
		}

		// schedule a regular task to delete timed out sessions
		TimerTask sessionsCleanupThread = new TimerTask() {
			@Override
			public void run() {
				Servers.this.cleanUpSessions();
			}
		};
		// remember the timer instance to be able to shut it down
		sessionsCleanupTimer = new Timer();
		sessionsCleanupTimer.schedule(sessionsCleanupThread, new Date(), sessionCleanupInterval);
	}

	private void createServers(Properties props) {
		// TODO: 05.12.2020 Refactoring
		for (int i = 0; ; i++)
		{
			if (!props.containsKey("servers." + i + ".apiKey") ||
					!props.containsKey("servers." + i + ".permissions"))
				break;

			String name =  "server" + i;
			String apiKey = props.getProperty("servers." + i + ".apiKey").trim();
			Authenticator authenticator = new ApiKeyAuthenticator(apiKey);
			String permissions[] = props.getProperty("servers." + i + ".permissions").split("[;,]");

			Server s = new Server(new HashSet<String>(Arrays.asList(permissions)), authenticator, apiKey, name);


			String allowedRemoteAdressesString = props.getProperty("servers." + i + ".allowedRemoteAdresses");
			String allowedRemoteAdresses[] = (allowedRemoteAdressesString != null) ?  allowedRemoteAdressesString.split("[;,]") : new String[]{"127.0.0.1", "0:0:0:0:0:0:0:1"};
			logger.info("No AllowedRemoteAddresses are specified for servers." + i + ". Allowing localhost as default.");
			s.allowedRemoteAdressRanges = new LinkedList<SubnetUtils>();
			s.allowedRemoteAdresses = new HashSet<String>();
			for (String thisAddress : allowedRemoteAdresses) {
				// Check whether this is an IP mask in CIDR notation
				try {
					SubnetUtils thisAddressRange = new SubnetUtils(thisAddress);
					s.allowedRemoteAdressRanges.add(thisAddressRange);
				} catch (IllegalArgumentException e) {
					// If not, store as plain IP address
					s.allowedRemoteAdresses.add(thisAddress);
				}
			}
			servers.put(s.getId(), s);

		}
	}

	/**
	 * Shut down instance. This method is called upon undeployment and releases
	 * resources, such as stopping background threads or removing objects that
	 * would otherwise persist and cause a memory leak. Called by
	 * {@link de.pseudonymisierung.mainzelliste.webservice.ContextShutdownHook}.
	 */
	public void shutdown() {
		// shut down session cleanup timer
		logger.info("Stopping sessions cleanup timer");
		sessionsCleanupTimer.cancel();
	}

	/**
	 * Create a new session. The session is assigned a new unique session id.
	 * 
	 * @return The new session object.
	 */
	public Session newSession(String serverName) {
		String sid = UUID.randomUUID().toString();
		Session s = new Session(sid, serverName);
		synchronized (sessions) {
			sessions.put(sid, s);
		}
		return s;
	}

	/**
	 * Get a session by its session id. Caller MUST ensure proper
	 * synchronization on the session.
	 * 
	 * @param sid
	 *            The id of the session to get.
	 * @return The session or null if no session with the given id exists.
	 */
	public Session getSession(String sid) {
		synchronized (sessions) {
			return sessions.get(sid);
		}
	}

	/**
	 * Returns all known session ids.
	 * 
	 * @return The ids of all active sessions.
	 */
	public Set<String> getSessionIds() {
		synchronized (sessions) {
			return Collections.unmodifiableSet(new HashSet<String>(sessions.keySet()));
		}
	}

	/**
	 * Delete a session. This also deletes (i.e. invalidates) all tokens
	 * belonging to the session.
	 * 
	 * @param sid The id of the session to delete.
	 */
	public void deleteSession(String sid) {
		Session s;
		synchronized (sessions) {
			s = sessions.get(sid);
			// silently return if session does not exist
			if (s == null)
				return;

			for (Token t : s.getTokens()) {
				tokensByTid.remove(t.getId());
				IPsByTid.remove(t.getId());
			}
			s.deleteAllTokens();
			sessions.remove(sid);
		}
	}

	/**
	 * Check for time-out sessions. Deletes all sessions that have been inactive
	 * for at least the configured timeout.
	 */
	public void cleanUpSessions() {
		logger.trace("Cleaning up sessions...");
		LinkedList<String> sessionsToDelete = new LinkedList<String>();
		Date now = new Date();
		synchronized (sessions) {
			for (Session s : this.sessions.values()) {
				if (now.getTime() - s.getLastAccess().getTime() > this.sessionTimeout)
					sessionsToDelete.add(s.getId());
			}
			// Delete sessions in a separate loop to avoid
			// ConcurrentModificationException
			for (String sessionId : sessionsToDelete) {
				this.deleteSession(sessionId);
				logger.info("Session {} timed out", sessionId);
			}
			this.deleteRequestersWithoutSession();
		}
	}

	/**
	 * Deletes all Requesters without active Session
	 */
	public void deleteRequestersWithoutSession(){
		for(Requester requester: this.clientList.getClients()){
			boolean hasSession = false;
			for (Session s : this.sessions.values()) {
				if (s.getParentServerName().equals(requester.getId())) {
					hasSession = true;
					break;
				}
			}
			if(!hasSession){
				clientList.removeClient(requester.getId());
			}
		}
	}

	
	/**
	 * Check whether a client is authorized for a request. The IP address of the
	 * requester and the API key (HTTP header "mainzellisteApiKey") are read
	 * from the HttpServletRequest. It is checked whether )according to the
	 * configuration) a server with the provided API exists, if the IP address
	 * lies in the configured set or range, and if the the requested permission
	 * is set for the server.
	 * 
	 * If access is denied, an appropriate WebApplicationException is thrown.
	 * 
	 * @param req
	 *            The injected HTTPServletRequest.
	 * @param permission
	 *            The permission to check, e.g. "addPatient".
	 */
	public void checkPermission(HttpServletRequest req, String permission) {
		checkPermission(req, permission, null);
	}

	//TODO: This function is not only checking permissions. it's also adding the configured server permission to a session. The function should have another name and function should be separated.
	public void checkPermission(HttpServletRequest req, String permission, Token token) {
		@SuppressWarnings("unchecked")
		// TODO: 05.12.2020 Refactoring


		Set<String> perms = (Set<String>) req.getSession(true).getAttribute("permissions");
		Map<String, ExtendedPermissions> extendedPermissions = new HashMap<>();

		if(perms == null){

			// Gets the http header information which are nnecessaryfor authenticate a server/ user
			Map<AuthenticationEum, String> authenticationMap  = AuthenticationUtils.getAuthenticationHeader(req);
			Requester requester = null;
			String idKey = "";

			// If apy key AND access_token is provided throw error
			if(authenticationMap.containsKey(AuthenticationEum.APIKEY) && authenticationMap.containsKey(AuthenticationEum.ACCESS_TOKEN)){
					throw new WebApplicationException( Response.status(Status.BAD_REQUEST)
						.entity("Please supply either Api key or an access token")
						.build());
			}
			// server logic
			else if(authenticationMap.containsKey(AuthenticationEum.APIKEY)){
				String apiKey = authenticationMap.get(AuthenticationEum.APIKEY);
				Server server = servers.get(apiKey);
				idKey = apiKey;

				if(server != null && !server.allowedRemoteAdresses.contains(req.getRemoteAddr())){
					boolean addressInRange = false;
					for (SubnetUtils thisAddressRange : server.allowedRemoteAdressRanges) {
						try {
							if (thisAddressRange.getInfo().isInRange(req.getRemoteAddr())) {
								addressInRange = true;
								break;
							}
						} catch (IllegalArgumentException e) {
							// Occurs if an IPv6 address was transmitted
							logger.error("Could not parse IP address " + req.getRemoteAddr(), e);
							break;
						}
					}
					if (!addressInRange) {
						logger.info("IP address " + req.getRemoteAddr() +  " rejected");
						throw new WebApplicationException(Response
								.status(Status.UNAUTHORIZED)
								.entity(String.format("Rejecting your IP address %s.", req.getRemoteAddr()))
								.build());
					}
				}

				requester = server;

			}
			// user logic create Requester
			else if(authenticationMap.containsKey(AuthenticationEum.ACCESS_TOKEN)){
				String accessToken = authenticationMap.get(AuthenticationEum.ACCESS_TOKEN);
				requester = Servers.instance.getRequesterByAccessToken(accessToken);
				// Access Token should not be display to the Requester
				idKey = "****";
				if(requester == null){
					throw new WebApplicationException(Response.status(Status.UNAUTHORIZED)
							.entity("Access token could not been validated")
							.build());
				}
			}

			if(requester == null){
				logger.info("No server or user found with provided key \"" + idKey + "\"");
				throw new WebApplicationException(Response
						.status(Status.UNAUTHORIZED)
						.entity("Please supply your API key in HTTP header field 'mainzellisteApiKey'. " +
								"Or your access token in the HTTP header field Authorization")
						.build());
			}

			perms = requester.getPermissions();
			if (Config.instance.isExtendedPermissionCheck()) {
				extendedPermissions = requester.getExtendedPermissions();
			}

			req.getSession().setAttribute("permissions", perms);
			req.getSession().setAttribute("serverName", requester.getName());

			Requester finalRequester = requester;
			logger.info(() -> "Server " + req.getRemoteHost() + " logged in with permissions " + Arrays.toString(
					finalRequester.getPermissions().toArray()) + ".");
		} else if (Config.instance.isExtendedPermissionCheck() && token != null) {
				extendedPermissions = Servers.instance
						.getRequesterByName(token.getParentServerName()).getExtendedPermissions();
		}

		if(!perms.contains(permission) && ( extendedPermissions.isEmpty() || !extendedPermissions.containsKey(permission))){ // Check permission
			logger.info("Access from {} is denied since they lack permission {}.", req.getRemoteHost(), permission);
			throw new WebApplicationException(Response
					.status(Status.UNAUTHORIZED)
					.entity("Your permissions do not allow the requested access.")
					.build());
		}

		// check extended permissions
		// Note: extendedPermissions list is empty if extended permissions was disabled in the configuration
		ExtendedPermissions configuredExtendedPermissions = extendedPermissions.get(permission);
		if (configuredExtendedPermissions != null && token != null) {
			// TODO: bad hack to avoid deserializing the Token two times -> refactor TokenParam and Token classes first
			Map<String, Object> tokenObjectMap = new HashMap<>();
			tokenObjectMap.put("allowedUses", token.getAllowedUses());
			tokenObjectMap.put("data", token.getData());
			// -----
			ExtendedPermissionUtils.checkPermission(tokenObjectMap, configuredExtendedPermissions,
					Config.instance.isExtendedPermissionCheckCaseSensitive());
		}
	}

	/**
	 * Register a token in a session. This is necessary so that a token is
	 * recognized as valid.
	 * 
	 * @param sessionId
	 *            Id of the session in which to register the token.
	 * @param t
	 *            The token to register.
	 * @param remoteAddress
	 *            The IP address of the system trying to register this token.
	 */
	public void registerToken(String sessionId, Token t, String remoteAddress) {
		Session s = getSession(sessionId);
		String tid = UUID.randomUUID().toString();
		t.setId(tid);
		t.setURI(s.getURI().resolve("tokens/" + tid));

		getSession(sessionId).addToken(t);

		synchronized (tokensByTid) {
			// register token in server
			tokensByTid.put(t.getId(), t);
			IPsByTid.put(t.getId(), remoteAddress);
		}
	}

	/**
	 * Delete the token with the given id. If you know this token's sessionId, call deleteToken instead...
	 * @param tokenId Id of the token to delete.
	 */
	public void deleteToken(String tokenId) {
		String sessionId = null;

		synchronized (sessions) {
			for(String sid: sessions.keySet()){
				for(Token t: sessions.get(sid).getTokens()){
					if(tokenId.equals(t.getId())){
						sessionId = sid;
						break;
					}
				}
			}
		}

		deleteToken(sessionId, tokenId);
	}

	/**
	 * Delete the token with the given id and containing session. This is more
	 * efficient than {@link #deleteToken(String)} as there is no need to search
	 * the session the token belongs to.
	 * 
	 * @param sessionId
	 *            Id of the session the token belongs to.
	 * @param tokenId
	 *            Id of the token to delete.
	 */
	public void deleteToken(String sessionId, String tokenId) {
		if(sessionId != null){
			getSession(sessionId).deleteToken(tokensByTid.get(tokenId));
		}

		synchronized (tokensByTid) {
			tokensByTid.remove(tokenId);
			IPsByTid.remove(tokenId);
		}
	}

	/**
	 * Get all tokens of the given session.
	 * 
	 * @param sid
	 *            Id of the session whose tokens to get.
	 * @return The set of tokens.
	 */
	public Set<Token> getAllTokens(String sid) {
		Session s = getSession(sid);
		if(s == null) return Collections.emptySet();
		
		return s.getTokens();
	}

	/**
	 * Get a token by its id.
	 * 
	 * @param tokenId
	 *            Id of the token to get.
	 * @return The token or null if no token with the given id exists.
	 */
	public Token getTokenByTid(String tokenId) {
		synchronized (tokensByTid) {
			return tokensByTid.get(tokenId);
		}
	}

	/**
	 * a convenience method to find token with the given token id and token class
	 *
	 * @param tokenId    token id
	 * @param tokenClass token type
	 * @return token
	 */
	public <T extends Token> T findToken(String tokenId, Class<T> tokenClass, String tokenType) {
		Token token = getTokenByTid(tokenId);
		if (token == null) {
			logger.error("No token with id {} found", tokenId);
		} else if (!tokenClass.isInstance(token) || !token.getType().equals(tokenType)) {
			logger.error("Token {} is not of type '{}' but '{}'", tokenId, tokenType,
					token.getType());
		} else {
			return (T) token;
		}
		throw new InvalidTokenException("Please supply a valid '" + tokenType + "' token.",
				Status.UNAUTHORIZED);
	}

	/**
	 * Get the remote IP address of a specific token by its id.
	 *
	 * @param tokenId
	 *            Id of the token to get the remote IP from.
	 * @return The remote IP in String format or null if no token with the given id exists.
	 */
	public String getRemoteIpByTid(String tokenId) {
		synchronized (tokensByTid) {
			return IPsByTid.get(tokenId);
		}
	}

	/**
	 * Check if a token exists and has the given type. If
	 * 
	 * @param tid
	 *            Id of the token to check.
	 * @param type
	 *            Token type to check for (e.g. "addPatient").
	 * @throws InvalidTokenException
	 *             If no token with the given id and type exists.
	 */
	public void checkToken(String tid, String type) throws InvalidTokenException {
		Token t = getTokenByTid(tid);
		if (t == null || !type.equals(t.getType()) ) {
			logger.info(() -> "Token with id " + tid + " " + (t == null ? "is unknown." : ("has wrong type '" + t.getType() + "'")));
			throw new InvalidTokenException("Please supply a valid '" + type + "' token.");
		}
	}

	/**
	 * Represents the interface version, consisting of major and minor revision.
	 */
	public static class ApiVersion {
		/** The major revision. */
		public final int majorVersion;
		/** The minor revision. */
		public final int minorVersion;
		
		/**
		 * Create an instance from a version string. The version string is split
		 * by dots and the first two segments are used as major and minor
		 * version, respectively.
		 * 
		 * @param versionString A version string in the format "major.minor".
		 */
		public ApiVersion(String versionString) {
			majorVersion = Integer.parseInt(versionString.split("\\.")[0]);
			minorVersion = Integer.parseInt(versionString.split("\\.")[1]);
		}
		
		public String toString() {
			return majorVersion + "." + minorVersion;
		}
	}

	/**
	 * Get api version from a request. Reads the version string from either the
	 * "mainzellisteApiVersion" header or, if no such header is set, from an URL
	 * parameter of the same name. If neither exists, version 1.0 is assumed.
	 * 
	 * @param req
	 *            The injected HttpServletRequest.
	 * @return The api version inferred from the request.
	 */
	public ApiVersion getRequestApiVersion(HttpServletRequest req) {
		// First try to read saved version String (prevents multiple parsing of header etc.)
		String version = null;
		Object versionHeader =req.getAttribute("de.pseudonymisierung.mainzelliste.apiVersion");
		if (versionHeader != null) {
			version = versionHeader.toString();
		} else {
			// Try to read from header
			version = req.getHeader("mainzellisteApiVersion");
			// Try to read from URL parameter
			if (version == null) {
				version = req.getParameter("mainzellisteApiVersion");
			}
			// Otherwise assume 1.0
			if (version == null) {
				version = "1.0";
			}
			if (!Pattern.matches("\\d+\\.\\d+", version)) {
				throw new WebApplicationException(
						Response.status(Status.BAD_REQUEST)
						.entity(String.format("'%s' is not a valid API version. Please " +
								"supply API version in format MAJOR.MINOR as HTTP header or " +
								"URL parameter 'mainzellisteApiVersion'.", version))
						.build());
			}
			// Save in request scope
			req.setAttribute("de.pseudonymisierung.mainzelliste.apiVersion", version);
		}
		return new ApiVersion(version);
	}

	/**
	 * Get api version (major version only) from a request.
	 * 
	 * @param req
	 *            The injected HttpServletRequest.
	 * @return The major api version inferred from the request.
	 * 
	 * @see #getRequestApiVersion(HttpServletRequest)
	 */
	public int getRequestMajorApiVersion(HttpServletRequest req) {
		return this.getRequestApiVersion(req).majorVersion;
	}

	/**
	 * Get api version (minor version only) from a request.
	 * 
	 * @param req
	 *            The injected HttpServletRequest.
	 * @return The major api version inferred from the request.
	 * 
	 * @see #getRequestApiVersion(HttpServletRequest)
	 */
	public int getRequestMinorApiVersion(HttpServletRequest req) {
		return this.getRequestApiVersion(req).minorVersion;
	}

	public String getServerNameForApiKey(String apiKey){
		Server server = servers.get(apiKey);
		if(server != null){
			return server.getName();
		}
		return null;
	}

	public void checkPermissionByName(String serverName, PermissionEnum permission) {
		if (!hasPermissionByName(serverName, permission)) {
			throw new UnauthorizedException("Your permissions do not allow the requested access.");
		}
	}

	public boolean hasPermissionByName(String serverName, PermissionEnum permission){
		return hasPermissionByName(serverName, permission.getValue());
	}

	public boolean hasPermissionByName(String serverName, String permission){
		Requester requester = getRequesterByName(serverName);
		return requester != null && requester.getPermissions().contains(permission);
	}


	/**
	 * Return the requester which is registered with the unique api key
	 * @param apiKey the apiKey of the requester
	 * @return the authenticated requester, otherwise null
	 */
	public Requester getRequesterByAPIKey(String apiKey){
		ApiKeyCredentials apiKeyCredentials = new ApiKeyCredentials(apiKey);
		for(Server requester: servers.values()){
			if(requester.isAuthenticated(apiKeyCredentials)){
				return requester;
			}
		}
		return  null;
	}

	/**
	 * Return the requester which could be authenticated with the specific access token
	 * If no requester could be found, a new requester will be created
	 * @param accessToken the access token given by the requester
	 * @return the authenticated requester, otherwise null
	 */
	public Requester getRequesterByAccessToken(String accessToken){
		OIDCCredentials OIDCCredentials = OIDCPropertiesAdapter.getOIDCInformationsByAccessToken(accessToken);
		Requester requester = clientList.getRequesterByAuthentication(OIDCCredentials);
		if(requester == null){
			requester  = createRequesterByAccessToken(accessToken);
		}
		return requester;
	}


	/**
	 * Creates a Requester with his OIDC-Accesstoken and the Config-File
	 * @param accessToken the given accesstoken
	 * @return the created requester
	 */

	public Requester createRequesterByAccessToken(String accessToken){
		OIDCCredentials oIDCCredentials = OIDCPropertiesAdapter.getOIDCInformationsByAccessToken(accessToken);
		if(oIDCCredentials != null){
			ClaimConfigurations claimConfigurations = Config.instance.getClaimConfigurationSet();
			Requester requester =  claimConfigurations.createRequester(oIDCCredentials, oIDCCredentials);
			if(requester != null){
				clientList.add(requester);
			}
			return requester;
		}
		return null;
	}

	/**
	 * Returns the Requester by his Name
	 * @param name the name of the requester
	 * @return the authenticated requester, otherwise null
	 */
	public Requester getRequesterByName(String name){
		Requester client =  clientList.getRequesterByName(name);
		Requester server = this.getServerByName(name);

		if(server != null) return server;
		if(client != null) return client;
		return null;
	}

	public Server getServerByName(String serverName){
		for (Map.Entry<String, Server> entry : this.servers.entrySet()) {
			Server server = entry.getValue();
			if(serverName.equals(server.getName())){
				return server;
			}
		}
		return  null;
	}

}
