package de.pseudonymisierung.mainzelliste;

import de.pseudonymisierung.mainzelliste.exceptions.InvalidIDException;

public class WildcardID extends ID {

  public WildcardID(String type) {
    this.type = type;
  }

  @Override
  public String getIdString() {
    return "*";
  }

  @Override
  protected void setIdString(String idString) throws InvalidIDException {
    this.idString = "*";
  }
}
