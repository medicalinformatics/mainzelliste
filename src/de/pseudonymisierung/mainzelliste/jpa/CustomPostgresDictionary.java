package de.pseudonymisierung.mainzelliste.jpa;

import org.apache.openjpa.jdbc.schema.Column;
import org.apache.openjpa.jdbc.sql.PostgresDictionary;
import org.apache.openjpa.jdbc.sql.RowImpl;

public class CustomPostgresDictionary extends PostgresDictionary {
  public static final String TYPE_JSONB = "jsonb";
  public static final String JSON_TYPE_MARKER = "? ::jsonb";
  public static final String JSON_NULL_TYPE_MARKER = "to_jsonb(?)";

  public CustomPostgresDictionary() {
    super();
  }

  @Override
  public String getMarkerForInsertUpdate(Column col, Object val) {
    if (col.getTypeIdentifier().getName() != null
        && col.getTypeIdentifier().getName().equalsIgnoreCase(TYPE_JSONB)) {
      return val != RowImpl.NULL ? JSON_TYPE_MARKER : JSON_NULL_TYPE_MARKER;
    }
    return super.getMarkerForInsertUpdate(col, val);
  }
}

