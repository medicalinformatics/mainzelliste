package de.pseudonymisierung.mainzelliste.jpa;

import org.apache.openjpa.jdbc.schema.Column;
import org.apache.openjpa.jdbc.sql.MySQLDictionary;
import org.apache.openjpa.jdbc.sql.RowImpl;

public class CustomMysqlDictionary extends MySQLDictionary {

  public CustomMysqlDictionary(){
    super();
  }

  @Override
  public String getMarkerForInsertUpdate(Column col, Object val) {
    if (val != RowImpl.NULL && col.getTypeIdentifier().getName() != null
        && col.getTypeIdentifier().getName().equalsIgnoreCase("json")) {
      return "CONVERT(? using utf8mb4)";
    }
    return super.getMarkerForInsertUpdate(col, val);
  }
}

