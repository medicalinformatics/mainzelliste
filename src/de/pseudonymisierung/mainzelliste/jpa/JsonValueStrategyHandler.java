package de.pseudonymisierung.mainzelliste.jpa;

import org.apache.openjpa.jdbc.identifier.DBIdentifier;
import org.apache.openjpa.jdbc.kernel.JDBCStore;
import org.apache.openjpa.jdbc.meta.ValueMapping;
import org.apache.openjpa.jdbc.meta.strats.AbstractValueHandler;
import org.apache.openjpa.jdbc.schema.Column;
import org.apache.openjpa.jdbc.schema.ColumnIO;
import org.apache.openjpa.jdbc.sql.DBDictionary;
import org.apache.openjpa.meta.JavaTypes;

public class JsonValueStrategyHandler extends AbstractValueHandler {

  private static final JsonValueStrategyHandler _instance = new JsonValueStrategyHandler();

  public static JsonValueStrategyHandler getInstance() {
    return _instance;
  }

  @Override
  public Column[] map(ValueMapping vm, String name, ColumnIO io, boolean adapt) {
    DBDictionary dict = vm.getMappingRepository().getDBDictionary();
    Column column = new Column();
    column.setJavaType(JavaTypes.STRING);
    column.setSize(-1);
    column.setIdentifier(DBIdentifier.newColumn(name, dict != null && dict.delimitAll()));
    column.setTypeIdentifier(DBIdentifier.newColumnDefinition(
        dict instanceof CustomPostgresDictionary ? "JSONB" : "JSON"));
    return new Column[]{column};
  }

  @Override
  public Object toDataStoreValue(ValueMapping vm, Object value, JDBCStore store) {
    return value == null ? null : value.toString();
  }

  @Override
  public Object toObjectValue(ValueMapping vm, Object value) {
    return value == null ? null : value.toString();
  }
}
