/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.dto;

import de.pseudonymisierung.mainzelliste.AssociatedIds;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.Patient;
import de.pseudonymisierung.mainzelliste.blocker.BlockingKey;
import de.pseudonymisierung.mainzelliste.webservice.JobsResource.AddPatientsJobTask;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class CachedRepository implements Repository{
  Map<BlockingKey, List<Patient>> patientsByBlockingKeys;
  List<Patient> allPatients = new ArrayList<>();

  public CachedRepository(List<AddPatientsJobTask> requests) {
    this.patientsByBlockingKeys = new ConcurrentHashMap<>(requests.size()*3);

    for (AddPatientsJobTask jobTask : requests) {
      if(jobTask.getPatient() == null || jobTask.runSequentially) {
        continue;
      }

      //init patientByBlockingKeys
      for (BlockingKey blockingKey : jobTask.getPatient().getBlockingKeys()) {
        patientsByBlockingKeys.computeIfAbsent(blockingKey, k ->
          new ArrayList<>(Persistor.instance.getPatients(blockingKey))
        );
      }
    }

    // if bk disabled
    if(patientsByBlockingKeys.isEmpty()){
      allPatients = Persistor.instance.getPatients();
    }
  }

  @Override
  public Patient getPatient(ID id, int currentIndex, boolean loadBlockingKeys) {
    return Persistor.instance.getPatient(id, currentIndex, loadBlockingKeys);
  }

  @Override
  public List<Patient> getPatientsWithAssociatedIds(List<AssociatedIds> associatedIdsList,
      int currentIndex, boolean loadBlockingKeys) {
    return Persistor.instance.getPatientsWithAssociatedIds(associatedIdsList, currentIndex, loadBlockingKeys);
  }

  @Override
  public List<Patient> getPatients(Set<BlockingKey> bks, int currentIndex) {
    if(bks.isEmpty()){
      return allPatients;
    }
    List<Patient> result = new ArrayList<>();
    for (BlockingKey bk : bks) {
      result.addAll(patientsByBlockingKeys.get(bk));
    }
    return result;
  }
}