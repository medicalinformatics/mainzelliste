/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste;

import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * An ID Generator that uses fermats little theorem to generate a distinct sequence of BigIntegers.
 */
public class BigIntegerIDGenerator implements IDGenerator<BigIntegerID> {

  /** Internal counter. Incremented on every ID creation. */
  BigInteger counter;
  /** A prime that specifies the modulo. its the p in `a^p = a (mod p)`. */
  BigInteger prime;
  /** A iteration value, set in the configuration. Its the a in `a^p = a (mod p)`. */
  BigInteger iter;
  /** The state information of this generator. */
  IDGeneratorMemory mem;
  /** The ID type this generator instance creates. */
  String idType;
  /** Length in digits for the length of IDs to create. */
  int length;
  /** optional Prefix und Suffix settable in the configuration. */
  String prefix;
  String suffix;

  private Logger logger = LogManager.getLogger(BigIntegerIDGenerator.class);
  private List<String> eagerGenRelatedIdTypes;

  @Override
  public void init(
      IDGeneratorMemory mem, String idType, String[] eagerGenRelatedIdTypes, Properties props) {
    this.idType = idType;
    this.mem = mem;
    this.eagerGenRelatedIdTypes = Arrays.asList(eagerGenRelatedIdTypes);

    // Recall all values from mem, if available
    this.counter =
        mem.get("counter") == null ? BigInteger.ONE : new BigInteger(mem.get("counter"));
    this.prime = mem.get("prime") == null ? null : new BigInteger(mem.get("prime"));

    if (!props.containsKey("length") || !props.containsKey("iter")) {
      logger.fatal(
          "IDGenerator "
              + idType
              + " is missing legnth or iter in the mainzelliste configuration.");
      throw new InternalErrorException("Configuration Error for IDGenerator " + idType);
    }

    this.prefix = props.getProperty("prefix", "");
    this.suffix = props.getProperty("suffix", "");

    try {
      // calculate the bitsize for length
      this.length = Integer.parseInt(props.getProperty("length"));
      BigInteger maximum_number = BigInteger.TEN.pow(this.length);

      // set a new prime if not set
      if (this.prime == null) {
        Random rnd;
        // use seed set by `rnd` (if available)
        if (props.containsKey("rnd")) {
          rnd = new Random(Long.parseLong(props.getProperty("rnd")));
        } else {
          rnd = new Random();
        }
        this.prime = BigInteger.probablePrime(maximum_number.bitLength(), rnd);
        this.mem.set("prime", this.prime.toString());
      }

      // set the iterator (i.e. number that will be multiplied by getNext)
      this.iter = new BigInteger(props.getProperty("iter"));
      if(this.iter.compareTo(this.prime) >= 0) {
          logger.fatal("The Iterator of the BigIntegerIDGenerator " + idType + " is larger than the generated prime. This will clash during ID generator. Choose a value for `iter` that is lower than the maximum number that you set using `length` (i.e. 1 <= iter < 10^length). The offset does not count towards that.");
          throw new NumberFormatException();
      }

    } catch (NumberFormatException e) {
      throw new InternalErrorException("Number format error in configuration of BigIntegerIDGenerator " + idType);
    }
  }

  @Override
  public BigIntegerID getNext() {
    // This is based on a proof of fermats little theorem:
    // suppose a sequence of integers `a, 2a, 3a, ..., (p-1)a` with a an Integer and p is prime
    // reducing each one modulo p yields a permutation of `1,2,3, ..., (p-1)`
    // For more information: see https://en.wikipedia.org/wiki/Proofs_of_Fermat's_little_theorem#Proof_as_a_particular_case_of_Euler's_theorem

    // it is possible to create a newId with a length longer than configured. This can happen due to the random initialization of a prime when creating this type of IDGenerator. If this happens, this method will increase the counter and just take the next value.
    BigInteger newId;
    String newIdString;
    do {
      this.counter = this.counter.add(BigInteger.ONE);
      newId = this.counter.multiply(this.iter).mod(this.prime);
      newIdString = newId.toString();
    } while(newIdString.length() > this.length);

    this.mem.set("counter", this.counter.toString());

    // pad the ID with zeroes until length
    while(newIdString.length() < this.length) {
      newIdString = "0" + newIdString;
    }

    // return with prefix and suffix 
    return new BigIntegerID(this.prefix + newIdString + this.suffix, this.idType);
  }

  @Override
  public BigIntegerID buildId(String id) {
    return new BigIntegerID(id, this.idType);
  }

  @Override
  public boolean verify(String idString) {
    if(idString.length() != this.length + this.prefix.length() + this.suffix.length()) {
      return false;
    }
    try {
      String strippedId = idString.substring(this.prefix.length(), idString.length() - this.suffix.length());
      // try to parse the idString without prefix and suffix to a BigInteger
      new BigInteger(strippedId);
      // check if prefix and suffix match this IDGenerator
      return idString.startsWith(this.prefix) && idString.endsWith(this.suffix);
    } catch (NumberFormatException e) {
      return false;
    }
  }

  @Override
  public String correct(String idString) {
    if (this.verify(idString)) {
      return idString;
    }
    return null;
  }

  @Override
  public String getIdType() {
    return this.idType;
  }

  @Override
  public boolean isExternal() {
    return false;
  }

  @Override
  public boolean isPersistent() {
    return true;
  }

  @Override
  public Optional<IDGeneratorMemory> getMemory() {
    return Optional.of(this.mem);
  }

  @Override
  public boolean isEagerGenerationOn(String idType) {
    return eagerGenRelatedIdTypes.contains("*") || eagerGenRelatedIdTypes.contains(idType);
  }
}
