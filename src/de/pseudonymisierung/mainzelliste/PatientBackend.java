package de.pseudonymisierung.mainzelliste;

import de.pseudonymisierung.mainzelliste.dto.Persistor;
import de.pseudonymisierung.mainzelliste.dto.Repository;
import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidIDException;
import de.pseudonymisierung.mainzelliste.matcher.MatchResult;
import de.pseudonymisierung.mainzelliste.matcher.MatchResult.MatchResultType;
import de.pseudonymisierung.mainzelliste.matcher.Matcher;
import de.pseudonymisierung.mainzelliste.matcher.NullMatcher;
import de.pseudonymisierung.mainzelliste.model.persistor.search.SearchPatientResult;
import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.PatientFieldFilter;
import de.pseudonymisierung.mainzelliste.permissions.PermissionEnum;
import de.pseudonymisierung.mainzelliste.webservice.Constants;
import de.pseudonymisierung.mainzelliste.webservice.RequestedIdTypes;
import de.pseudonymisierung.mainzelliste.webservice.Token;
import de.pseudonymisierung.mainzelliste.webservice.token.ReadPatientsToken;
import de.pseudonymisierung.mainzelliste.webservice.token.ReadPatientsToken.PatientSearchId;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Backend methods for handling patients. Implemented as a singleton object,
 * which can be referenced by PatientBackend.instance.
 */
public class PatientBackend {

	/** The singleton instance. */
  private static final PatientBackend instance = new PatientBackend(Persistor.instance);

  private static final ReentrantLock lock = new ReentrantLock();

	/** The logging instance */
	private static final Logger logger = LogManager.getLogger(PatientBackend.class);

  private Repository repository;

	/**
	 * Creates an instance. Invoked on first access to {@link PatientBackend#instance}.
	 */
  public PatientBackend(Repository repository) {
    this.repository = repository;
  }

  public static PatientBackend getDefaultInstance(){
    return instance;
  }

  public Repository getRepository() {
    return this.repository;
  }

  public void setRepository(Repository repository) {
    this.repository = repository;
  }

  public IDRequest createNewPatient(Patient inputPatient, RequestedIdTypes requestedIdTypes,
      boolean sureness, String tokenId) {
        // find match with the given IDAT of the input patient
        MatchResult match = findMatch(inputPatient, null);
        Patient assignedPatient; // The "real" patient that is assigned (match result or new patient)

        Set<String> idTypes = requestedIdTypes.getIdTypes();
        Set<String> transientIdTypes = requestedIdTypes.getTransientIdTypes();
        MultiValuedMap<String, String> associatedIdTypesMap = requestedIdTypes.getAssociatedIdTypesMap();

        MultiValuedMap<String, ID> generatedAssociatedIds = new ArrayListValuedHashMap<>();

        // generate requested ids and update fields if patient found
        switch (match.getResultType()) {
          case MATCH:
            assignedPatient = match.getBestMatchedPatient();
            // Firstly Generate/update from existing patient the persistent id types
            assignedPatient.updateFrom(inputPatient, IDGeneratorFactory.instance.getExternalIdTypes(),
                IDGeneratorFactory.instance.getExternalAssociatedIdTypes());
            for (String idType : idTypes) {
              assignedPatient.getOriginal().createId(idType);
            }
            for (String idType : transientIdTypes) {
              assignedPatient.getOriginal().createId(idType);
            }

            // find external associated ids from input patient
            List<ID> externalIds = inputPatient.getAssociatedIdsList().stream()
                .map(a -> a.getIds().iterator().next()) // only one id
                .collect(Collectors.toList());
            //create or update associatedIds with new generated ids
            generatedAssociatedIds = assignedPatient.createAssociatedIds(associatedIdTypesMap,
                assignedPatient.getAssociatedIdsList(externalIds));

            // log token to separate concurrent request in the log file
            // Log message is not informative if new ID types were requested
            // TODO: Discuss, which ID should we log in this case
            if (logger.isInfoEnabled()) {
              ID returnedId = null;
              if (!idTypes.isEmpty()) {
                returnedId = assignedPatient.getOriginal().getId(idTypes.iterator().next());
              } else if (!transientIdTypes.isEmpty()) {
                returnedId = assignedPatient.getOriginal()
                    .getTransientId(transientIdTypes.iterator().next());
              } else if (!generatedAssociatedIds.isEmpty()) {
                returnedId = generatedAssociatedIds.get(generatedAssociatedIds.mapIterator().next())
                    .iterator().next();
              }
              if (returnedId != null) {
                logger.info("Found match with ID {} for ID request {}", returnedId.getIdString(),
                    tokenId);
              }
            }
            break;

          case NON_MATCH:
          case POSSIBLE_MATCH:
            if (match.getResultType() == MatchResultType.POSSIBLE_MATCH && !sureness) {
              return new IDRequest(inputPatient.getInputFields(), idTypes, generatedAssociatedIds,
                  match, null);
            }

            // Generate internal IDs
            // Note: inputPatient already contain all externals ids
            if(IDGeneratorFactory.instance.isEagerGenerationOn()) {
              IDGeneratorFactory.instance.generateIds().forEach(inputPatient::addId);
            } else {
              // first generate all persistent ids
              idTypes.forEach(inputPatient::createId);
              transientIdTypes.forEach(inputPatient::createId);
            }

            generatedAssociatedIds = inputPatient.createAssociatedIds(associatedIdTypesMap);

            for (String idType : idTypes) {
              ID currentId = inputPatient.getId(idType);
              logger.debug("Created new ID {} for ID request {}", currentId.getIdString(), tokenId);
            }

            for (String idType : transientIdTypes) {
              ID currentId = inputPatient.getTransientId(idType);
              logger.debug("Created new transient ID {} for ID request {}", currentId.getIdString(), tokenId);
            }
            if (match.getResultType() == MatchResultType.POSSIBLE_MATCH) {
              inputPatient.setTentative(true);
              Patient bestMatchedPatient = match.getBestMatchedPatient();
              // log tentative and possible match ids
              inputPatient.getIds().stream()
                  .filter(id -> bestMatchedPatient.getId(id.getType()) != null)
                  .forEach(id -> logger.info("New ID {} is tentative. Found possible match "
                      + "with ID {}", id.getIdString(), bestMatchedPatient.getId(id.getType()).getIdString()));
            }
            assignedPatient = inputPatient;
            break;

          default:
            logger.error("Illegal match result: {}", match.getResultType());
            throw new InternalErrorException();
        }

        logger.info("Weight of best match: {}", match.getBestMatchedWeight());

        // persist id request and new patient
        return new IDRequest(inputPatient.getInputFields(), requestedIdTypes.getAllIdTypes(),
            generatedAssociatedIds, match, assignedPatient);
  }

  /**
   * PID request. Looks for a patient with the specified data in the database. If a match is found,
   * the ID of the matching patient is returned. If no match or possible match is found, a new
   * patient with the specified data is created. If a possible match is found and the given
   * "sureness" value is true, a new patient is created.
   *
   * @param fields  input fields from the HTTP request and Token.
   * @param externalIds externalIds and associatedIds from the HTTP request and Token.
   * @param requestedIdTypes  Input fields from the HTTP request.
   * @param sureness if true, add possible match patient.
   * @return A representation of the request and its result as an instance of {@link IDRequest}.
   */
  public IDRequest createAndPersistPatient(Map<String, String> fields,
      Map<String, List<String>>  externalIds, RequestedIdTypes requestedIdTypes, boolean sureness,
      boolean ignoreInvalidIDAT, String tokenId) {
    // deserialize patient from form
    Patient inputPatient = createPatientFrom(fields, externalIds, ignoreInvalidIDAT);
    return createAndPersistPatient(inputPatient, requestedIdTypes, sureness, tokenId);
  }

  public IDRequest createAndPersistPatient(Patient inputPatient,
      RequestedIdTypes requestedIdTypes, boolean sureness, String tokenId) {
    lock.lock();
    IDRequest request;
    try {
      request = createNewPatient(inputPatient, requestedIdTypes, sureness, tokenId);
      if (request.getAssignedPatient() != null) {
        persistIDRequest(request, tokenId);
      }
    } finally {
      lock.unlock();
    }
    return request;
  }

  public void lockCreatingPatient(){
    lock.lock();
  }

  public void unlockCreatingPatient(){
    lock.unlock();
  }

  public void persistIDRequest(IDRequest request, String tokenId) {
    // persist
    Persistor.instance.addIdRequest(request);

    // audit trail entry
    if (Config.instance.auditTrailIsOn()) {
      for (ID id : request.getAssignedPatient().getIds()) {
        AuditTrail at = buildAuditTrailRecord(tokenId,
            id.getIdString(),
            id.getType(),
            request.getMatchResult().getResultType(),
            null,
            request.getAssignedPatient().toString()
        );
        Persistor.instance.persistAuditTrailRecords(at);
      }
    }
  }

    /**
     * Set fields of a patient to new values.
     *
     * @param patientId      ID of the patient to edit.
     * @param newFieldValues Field values to set. Fields that do not appear as map keys are
     *                       left as they are. In order to delete a field value, provide an
     *                       empty string. All values are processed by field transformation
     *                       as defined in the configuration file.
     */
	public void editPatient(ID patientId, Map<String, String> newFieldValues, boolean sureness, String tokenId) {
        // Check that provided ID is valid
        if (patientId == null) {
            // Calling methods should provide a legal id, therefore log an error if id is null
            logger.error("editPatient called with null id.");
            throw new InternalErrorException("An internal error occured: editPatients called with null. Please contact the administrator.");
        }
        Patient pToEdit = Persistor.instance.getPatient(patientId, true);
        if (pToEdit == null) {
            logger.info("Request to edit patient with unknown ID {}", patientId.toString());
            throw new InvalidIDException("No patient found with ID " + patientId.toString());
        }

        // validate input
        Validator.instance.validateForm(newFieldValues, false);
        // read input fields from form
        Patient pInput = new Patient();
        Map<String, Field<?>> chars = new HashMap<String, Field<?>>();

		for (String fieldName : pToEdit.getInputFields().keySet()) {
			// If a field is not in the map, keep the old value
			if (!newFieldValues.containsKey(fieldName))
				chars.put(fieldName, pToEdit.getInputFields().get(fieldName));
			else {
				chars.put(fieldName, Field.build(fieldName, newFieldValues.get(fieldName)));
			}
		}

        pInput.setFields(chars);

        // transform input fields
        Patient pNormalized = Config.instance.getRecordTransformer().transform(pInput);
        // set input fields
        pNormalized.setInputFields(chars);
      // Save existing patient as string for audit trail
      String pOld = pToEdit.toString();
      // Save old patient fields
      Patient oldPatient = new Patient();
      oldPatient.setFields(pToEdit.getFields());
      oldPatient.setInputFields(pToEdit.getInputFields());

        // check if the patient field changed
        boolean fieldsChanged = Config.instance.getFieldKeys().stream().anyMatch(fieldName ->
            !pNormalized.getInputFieldString(fieldName).trim()
                .equals(pToEdit.getInputFieldString(fieldName))
        );

        // assign changed fields to patient in database, persist
        pToEdit.setFields(pNormalized.getFields());
        pToEdit.setInputFields(pNormalized.getInputFields());

        // edit external ids
        boolean idsChanged = false;
        for (Entry<String, String> inputId : newFieldValues.entrySet()) {
          if (!IDGeneratorFactory.instance.isExternalIdType(inputId.getKey())) {
            continue;
          }

          ID oldExtId = pToEdit.getId(inputId.getKey());
          if (oldExtId != null) {
            // edit existing id.
            // Note: empty ids will be removed in Persitor.updatePatient(...)
            if(!oldExtId.getIdString().equals(inputId.getValue().trim())) {
              oldExtId.setIdString(inputId.getValue().trim());
              idsChanged = true;
            }
          } else if (!inputId.getValue().trim().isEmpty()) {
            // add new id
            pToEdit.addId(IDGeneratorFactory.instance.buildId(inputId.getKey(), inputId.getValue()));
            idsChanged = true;
          }
        }

        // return if no changes
        if(!fieldsChanged && !idsChanged)
          return;

        // find match
        MatchResult match = findMatch(new Patient(pToEdit.getIds(), pToEdit.getAssociatedIdsList(),
            pToEdit.getFields()), patientId);

        switch (match.getResultType()) {
            case MATCH:
                logger.debug("Best matching weight on ID Matching: {}", match.getBestMatchedWeight());
                logger.error("Editing patient not possible because of matching with the existing patient!");
                throw new WebApplicationException(
                        Response.status(Status.CONFLICT)
                                .entity("Editing patient not possible because of matching with the existing patient!")
                                .build());

            case NON_MATCH:
                pToEdit.setTentative(false);
                break;
            case POSSIBLE_MATCH:
              // also ignore if the old patient fields cause an unsure match with the same tentative patient.
              MatchResult matchWithOldData = Config.instance.getMatcher()
                  .match(oldPatient, Collections.singletonList(match.getBestMatchedPatient()));
                if (!sureness && matchWithOldData.getResultType() != MatchResultType.POSSIBLE_MATCH) {
                    logger.debug("Best matching weight on ID Matching: {}", match.getBestMatchedWeight());
                    logger.error("Editing patient not possible because of tentative matching with the existing patient! Use" +
                            "sureness flag, if you are sure the data is correct and can be editted");
                    throw new WebApplicationException(
                            Response.status(Status.CONFLICT)
                                    .entity("Editing patient not possible because of tentative matching with the existing patient! Use " +
                                            "sureness flag, if you are sure the data is correct and can be edited")
                                    .build());
                }

                pToEdit.setTentative(true);
                break;
            default:
                logger.error("Illegal match result: {}", match.getResultType());
                throw new InternalErrorException();
        }

        // Save to database
    Persistor.instance.updatePatient(pToEdit);
		if (Config.instance.auditTrailIsOn()) {
			for (ID id : pToEdit.getIds()) {
				//Prepare the audit trail record
				AuditTrail at = buildAuditTrailRecord(tokenId,
						id.getIdString(),
						id.getType(),
						"edit",
						pOld,
						pToEdit.toString());
				//Persist patient and audit trail accordingly
				Persistor.instance.persistAuditTrailRecords(at);
			}
		}

    }

    /**
     * Check if the patient with the given idat exist
     * @param inputFields idat
     * @param externalIds external ids
     * @return best match patient with matching weight
     */
    public MatchResult findMatch(Map<String, String> inputFields, Map<String, List<String>> externalIds){
        return findMatch(createPatientFrom(inputFields, externalIds, false), null, true, 0);
    }

    private MatchResult findMatch(Patient inputPatient, ID patientId){
      return findMatch(inputPatient, patientId, 0);
    }

    public MatchResult findMatch(Patient inputPatient, ID patientId, int currentIndex){
      return findMatch(inputPatient, patientId, Config.instance.isFieldRecordLinkageEnabled(), currentIndex);
    }

    private MatchResult findMatch(Patient inputPatient, ID patientId, boolean enableFieldRecordLinkage, int currentIndex) {
        // input patient (AddPatient) should contain only external Ids
        // input patient (EditPatient) contains all types of Ids
        Set<ID> externalIds = inputPatient.getIds().stream().filter(id -> id.getFactory().isExternal()).collect(Collectors.toSet());
//        Set<ID> externalIds = inputPatient.getIds();
        boolean containAssociatedIds = inputPatient.getAssociatedIdsList().stream()
            .anyMatch(a -> !a.getIds().isEmpty());
        // External Id matching
        if (!externalIds.isEmpty() || containAssociatedIds) {
            // Find matches with all given external IDs
            Set<Patient> idMatches = externalIds.stream().map( id -> this.repository.getPatient(id, currentIndex, true))
                    .filter(Objects::nonNull).collect(Collectors.toSet());
            if(containAssociatedIds && idMatches.size() <= 1) {
              idMatches.addAll(this.repository.getPatientsWithAssociatedIds(inputPatient.getAssociatedIdsList(), currentIndex, true));
            }

            if (patientId != null) {
                // Remove the original patient from the list
                Iterator<Patient> it = idMatches.iterator();
                while (it.hasNext()) {
                    ID matchId = it.next().getId(patientId.getType());
                    if (matchId.getIdString().equals(patientId.getIdString())){
                        it.remove();
                        break;
                    }
                }
            }

            if (idMatches.size() > 1) {// Found multiple patients with matching external ID
                throw new WebApplicationException(Response.status(Status.CONFLICT)
                        .entity("Multiple patients found with the given external IDs!").build());
            } else if (idMatches.isEmpty()) { // No patient with matching external ID
                MatchResult idatMatchResult = findMatchWithIdat(inputPatient, patientId, enableFieldRecordLinkage, currentIndex);
                if (idatMatchResult != null && idatMatchResult.getResultType() == MatchResultType.MATCH) {
                    // No match with ID, but with IDAT.
                    // Check that no conflicting external ID exists
                    boolean conflict = !externalIds.stream().allMatch(id -> {
                        ID idOfMatch = idatMatchResult.getBestMatchedPatient().getId(id.getType());
                        return (idOfMatch == null || id.getIdString().equals(idOfMatch.getIdString()));
                    });

                    if (conflict) {
                        throw new WebApplicationException(
                                Response.status(Status.CONFLICT)
                                        .entity("Found existing patient with matching IDAT but conflicting external ID(s).")
                                        .build());
                    }

                    return idatMatchResult;
                } else { // No id match, no IDAT match
                    return new MatchResult(MatchResultType.NON_MATCH, null, 0);
                }
            } else { // idMatches.size() == 1 : Found patient with matching external ID
                final Patient idMatch = idMatches.iterator().next();
                // Check if IDAT of input and match (if present) matches
                if (!inputPatient.getFields().isEmpty()
                    && idMatch.getFields().values().stream().anyMatch(f -> !f.isEmpty())
                    && Validator.instance.getRequiredFields().stream()
                    .allMatch(f -> idMatch.getFields().containsKey(f) && !idMatch.getFields().get(f).isEmpty())
                ) {
                    MatchResult matchWithIdMatch = Config.instance.getMatcher().match(inputPatient, Collections.singletonList(idMatch));
                    if (matchWithIdMatch.getResultType() != MatchResultType.MATCH) {
                        logger.debug("Best matching weight on ID Matching: {}", matchWithIdMatch.getBestMatchedWeight());
                        throw new WebApplicationException(
                                Response.status(Status.CONFLICT)
                                        .entity("Found existing patient with matching external ID but conflicting IDAT!")
                                        .build());
                    }
                }

                //compare input fields
                if (idMatch.containInvalidOrMissedRequiredField()
                    && !inputPatient.getInputFields().isEmpty()
                    && !idMatch.checkIfInputFieldsIn(inputPatient.getInputFields()) ||
                    !idMatch.getFields().isEmpty() && // if idMatch contain valid and all requiredField
                    inputPatient.containInvalidOrMissedRequiredField()) {
                  throw new WebApplicationException(Response.status(Status.CONFLICT)
                      .entity("Found existing patient with matching external ID but conflicting IDAT!")
                      .build());
                }

                // If an IDAT match exists, check if it is the same patient
                MatchResult idatMatchResult = findMatchWithIdat(inputPatient, patientId, enableFieldRecordLinkage, currentIndex);
                if (idatMatchResult != null && idatMatchResult.getResultType() == MatchResultType.MATCH &&
                        !idatMatchResult.getBestMatchedPatient().equals(idMatch)) {
                    throw new WebApplicationException(
                            Response.status(Status.CONFLICT)
                                    .entity("External ID and IDAT match with different patients, respectively!")
                                    .build());
                }

                return new MatchResult(MatchResultType.MATCH, idMatch, 1.0);
            }
        } else if (inputPatient.getFields().isEmpty()) {
            throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
                    .entity("Neither complete IDAT nor an external ID has been given as input!").build());
        }
        return findMatchWithIdat(inputPatient, patientId, enableFieldRecordLinkage, currentIndex);
    }

    private MatchResult findMatchWithIdat(Patient inputPatient, ID patientId, boolean enableFieldRecordLinkage, int currentIndex) {
      MatchResult result = new MatchResult(MatchResultType.NON_MATCH, null, 0);
      if (inputPatient.getFields().isEmpty()) {
        return result;
      }
      // Blocking key extraction
      inputPatient.initBlockingKeys();


      if(!enableFieldRecordLinkage) {
        return result;
      }

      // Matching
      final Matcher matcher = Config.instance.getMatcher();
      List<Patient> candidatePatients;
      if (matcher instanceof NullMatcher) {
        candidatePatients = new ArrayList<>();
      } else {
        candidatePatients = this.repository.getPatients(inputPatient.getBlockingKeys(), currentIndex);
        if (patientId != null) {
          candidatePatients = candidatePatients.stream()
              .filter(e -> {
                ID id = e.getId(patientId.getType());
                return id == null || !id.getIdString().equals(patientId.getIdString());
              })
              .collect(Collectors.toList());
        }
      }
      result = matcher.match(inputPatient, candidatePatients);
      logger.debug("Best matching weight for IDAT matching: {}", result.getBestMatchedWeight());
      return result;
    }

    public static Patient createPatientFrom(Map<String, String> fields,
        Map<String, List<String>> externalIdsMap, boolean ignoreInvalidIDAT) {
        // create external Id list
      List<ID> externalIds = IDGeneratorFactory.instance.getExternalIdTypes().stream()
          .filter(externalIdsMap::containsKey)
          .map(idType -> IDGeneratorFactory.instance.buildId(idType,
              externalIdsMap.get(idType).get(0)))
          .collect(Collectors.toList());

      // create external associatedIds list
      List<AssociatedIds> associatedIdsList = IDGeneratorFactory.instance
          .getExternalAssociatedIdTypes().stream()
          .filter(externalIdsMap::containsKey)
          // find external associatedIds with the same type and return a stream of ID Objects
          .flatMap(idType -> externalIdsMap.get(idType).stream()
              .map(idString -> new ExternalID(idString, idType))
              .map(ID.class::cast))
          .map(AssociatedIds::createAssociatedIds)
          .collect(Collectors.toList());

        // check if required fields are available
        if (fields.keySet().containsAll(Validator.instance.getRequiredFields())) {
          boolean validInputFields = true;
          try {
            Validator.instance.validateForm(fields, false);
          } catch (IllegalArgumentException | WebApplicationException e) {
            if(!ignoreInvalidIDAT) {
              throw e;
            }
            validInputFields = false;
          }

          // normalize and transform fields of the new patient
          Map<String, Field<?>> inputFields = mapMapWithConfigFields(fields);
          if(validInputFields) {
            Patient inputPatient = Config.instance.getRecordTransformer()
                .transform(new Patient(new HashSet<>(externalIds), new ArrayList<>(associatedIdsList),
                        inputFields));
            inputPatient.setInputFields(inputFields);
            inputPatient.initBlockingKeys();
            return inputPatient;
          } else {
            Patient inputPatient = new Patient(new HashSet<>(externalIds), new ArrayList<>(associatedIdsList));
            inputPatient.setInputFieldsOnly(inputFields);
            return inputPatient;
          }
        } else {
            return new Patient(new HashSet<>(externalIds), new ArrayList<>(associatedIdsList), null);
        }
    }

  public SearchPatientResult findPatients(ReadPatientsToken token, HttpServletRequest request,
      PatientFieldFilter patientFieldFilter, List<PatientSearchId> patientSearchIds,
      boolean ignoreOrder, int limit, int offset)
  {
    SearchPatientResult searchPatientResult;
    // find patients
    if (token.searchWithWildCard()) { // searchIds contains id with wildcard e.g idType="pid" idString="*"
      searchPatientResult = PatientBackend.getDefaultInstance().findPatients(
          token.getBaseSearchIdTypes(), token.containAssociatedSearchId(), limit, offset,
          patientFieldFilter, token.isReturnAssociatedIds());
    } else if(token.isReadAllPatients()) { // searchIds contains idType="*" idString="*"
      Servers.instance.checkPermissionByName(token.getParentServerName(), PermissionEnum.READ_ALL_PATIENTS);
      searchPatientResult = PatientBackend.getDefaultInstance()
          .findPatients(limit, offset, patientFieldFilter, token.isReturnAssociatedIds());
    } else if (limit > 0 && !ignoreOrder) { // searchIds contains ids
      patientSearchIds = patientSearchIds.stream()
          .filter( e -> e.getOrder() >= offset && e.getOrder() < offset + limit).toList();
      searchPatientResult = PatientBackend.getDefaultInstance().findPatients(
          patientSearchIds, token.containAssociatedSearchId(), 0, 0,
          patientFieldFilter, token.isReturnAssociatedIds());
    } else {
      searchPatientResult = PatientBackend.getDefaultInstance().findPatients(
          token.getPatientSearchIds(), token.containAssociatedSearchId(), limit, offset,
          patientFieldFilter, token.isReturnAssociatedIds() || !ignoreOrder);
    }
    logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "fetching patients from DB done");
    return searchPatientResult;
  }

  private SearchPatientResult findPatients(int limit, int offset,
      PatientFieldFilter patientFieldFilter, boolean fetchAssociatedIds) {
    return Persistor.instance.findPatients((List<String>) null, false, limit, offset,
        patientFieldFilter, fetchAssociatedIds);
  }

  private SearchPatientResult findPatients(List<String> searchIdTypes,
      boolean containAssociatedIdTypes, int limit, int offset,
      PatientFieldFilter patientFieldFilter, boolean fetchAssociatedIds) {
    return Persistor.instance.findPatients(searchIdTypes, containAssociatedIdTypes, limit, offset,
        patientFieldFilter, fetchAssociatedIds);
  }

  private SearchPatientResult findPatients(Collection<PatientSearchId> patientSearchIds,
      boolean containAssociatedIdTypes, int limit, int offset,
      PatientFieldFilter patientFieldFilter, boolean fetchAssociatedIds) {

    // find patient with internal search id
    return Persistor.instance.findPatients(
        patientSearchIds, containAssociatedIdTypes, limit, offset, patientFieldFilter,
        fetchAssociatedIds);
  }

  public AuditTrail buildAuditTrailRecord(String tokenId, String idString, String idType,
      MatchResultType matchResult, String oldRecord, String newRecord) {
    String changeType;
    switch (matchResult) {
      case POSSIBLE_MATCH:
        changeType = "tentative";
        break;
      case MATCH:
        changeType = "match";
        break;
      default: // if = "NON_MATCH" Note: "ambiguous" is never user
        changeType = "create";
        break;
    }
    return buildAuditTrailRecord(tokenId, idString, idType, changeType, oldRecord, newRecord);
  }

  public AuditTrail buildReadAuditTrailRecord(String tokenId, ID searchId, String patientRecord) {
    return buildAuditTrailRecord(tokenId, searchId.getIdString(), searchId.getType(),
        "read", patientRecord, null);
  }

	public AuditTrail buildAuditTrailRecord(String tokenId, String idString, String idType, String changeType, String oldRecord, String newRecord) {
		// Get token for this action, its ID has allready been checked by the caller's parent
		Token t = Servers.instance.getTokenByTid(tokenId);

		// Get remote IP for this token
		String remoteIP = Servers.instance.getRemoteIpByTid(tokenId);
		if (remoteIP == null) {
			String infoLog = "Remote IP for Token with ID " + tokenId + " could not be determined. Token was invalidated by a concurrent request or the session timed out during this request.";
			logger.info(infoLog);
			throw new WebApplicationException(Response
					.status(Status.BAD_REQUEST)
					.entity("Please supply a valid 'editPatient' token.")
					.build());
		}
		//Build audit trail record, debug aware to prevent NPE and keep audit trail consistent
		AuditTrail at = new AuditTrail( new Date(),
				idString,
				idType,
				(Config.instance.debugIsOn()) ? "debug" : t.getDataItemMap("auditTrail")
						.get("username")
						.toString(),
				(Config.instance.debugIsOn()) ? "debug" : t.getDataItemMap("auditTrail")
						.get("remoteSystem")
						.toString(),
				remoteIP,
				changeType,
				(Config.instance.debugIsOn()) ? "debug" : t.getDataItemMap("auditTrail")
						.get("reasonForChange")
						.toString(),
				oldRecord,
				newRecord);
		return at;
	}

	private static Map<String, Field<?>> mapMapWithConfigFields(Map<String, String> fields) {
		Map<String, Field<?>> fieldMap = new HashMap<>();
		//Compare Fields from Config with requested Fields a
		for(String fieldName: Config.instance.getFieldKeys()){
			if (fields.containsKey(fieldName)) {
				try {
					fieldMap.put(fieldName, Field.build(fieldName, fields.get(fieldName)));
				} catch (WebApplicationException we) {
					logger.error(String.format("Error while building field %s with input %s", fieldName, fields.get(fieldName)));
					throw we;
				}
			}
		}
		return fieldMap;
	}
}
