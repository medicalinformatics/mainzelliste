/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.JsonObject;
import de.pseudonymisierung.mainzelliste.blocker.BlockingKey;
import de.pseudonymisierung.mainzelliste.client.fttp.bloomfilter.RecordBloomFilter;
import de.pseudonymisierung.mainzelliste.exceptions.CircularDuplicateRelationException;
import de.pseudonymisierung.mainzelliste.exceptions.ConflictingDataException;
import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import jakarta.xml.bind.annotation.XmlRootElement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Transient;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.openjpa.persistence.Persistent;
import org.apache.openjpa.persistence.jdbc.Strategy;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hl7.fhir.common.hapi.validation.validator.FHIRPathResourceGeneratorR4;

/**
 * A patient entity identified by at least one ID and described by a number of
 * Fields.
 */
@XmlRootElement
@Entity
public class Patient extends Identifiable{

	/**
	 * JSON serialization of a map of fields. Used for persisting fields in the
	 * database.
	 *
	 * @param fields Map of fields to serialize.
	 * @return JSON serialization of the given fields.
	 * @see #stringToFields(String)
	 */
	public static String fieldsToString(Map<String, Field<?>> fields) {
			JsonObject fieldsJson = new JsonObject();
			for (Entry<String, Field<?>> field : fields.entrySet()) {
				fieldsJson.add(field.getKey(), field.getValue().toJSON());
			}
			return fieldsJson.toString();
	}

	/**
	 * Creates a map of fields from its JSON representation. Used to read persisted
	 * fields from the database.
	 *
	 * @param fieldsJsonString JSON string as created by
	 *                         {@link #fieldsToString(Map)}.
	 * @return The map of fields.
	 */
	public static Map<String, Field<?>> stringToFields(String fieldsJsonString) {
		try {
			Map<String, Field<?>> fields = new HashMap<String, Field<?>>();
			if (fieldsJsonString == null)
				return fields;
			JSONObject fieldsJson = new JSONObject(fieldsJsonString);
			Iterator<?> it = fieldsJson.keys();
			while (it.hasNext()) {
				String fieldName = (String) it.next();
				JSONObject thisFieldJson = fieldsJson.getJSONObject(fieldName);
				String fieldClass = thisFieldJson.getString("class");
				String fieldValue = thisFieldJson.has("value") ? thisFieldJson.getString("value") : null;
				Field<?> thisField = (Field<?>)Config.instance.getFieldTypeByClass(fieldClass).newInstance();
				thisField.setValue(fieldValue);
				fields.put(fieldName, thisField);
			}
			return fields;
		} catch (JSONException e) {
			LogManager.getLogger(Patient.class).error("JSON error while parsing patient fields: " + e.getMessage(), e);
			throw new InternalErrorException();
		} catch (Exception e) {
			Logger logger = LogManager.getLogger(Patient.class);
			logger.error("Exception while parsing patient fields from string: " + fieldsJsonString);
			LogManager.getLogger(Patient.class).error("Cause: " + e.getMessage(), e);
			throw new InternalErrorException();
		}
	}

	/**
	 * Updates empty or missing fields and external Ids from another Patient object.
	 * Modifies the object and returns it.
	 *
	 * @param from The patient object from which to update fields.
	 * @param externalIdTypes external id types
	 * @param extAssociatedIdTypes external associated id types
	 * @return The modified patient object on which the method is called.
	 */
	public Patient updateFrom(Patient from, Set<String> externalIdTypes,
			List<String> extAssociatedIdTypes) {

		// update patient if field is null or empty, otherwise leave old value
		AtomicBoolean fieldChanged = new AtomicBoolean(false);
		from.getFields().entrySet().stream()
				.filter(field -> !field.getValue().isEmpty() && (!this.fields.containsKey(field.getKey()) || this.fields.get(field.getKey()).isEmpty()))
				.forEach(field -> { this.fields.put(field.getKey(), field.getValue()); fieldChanged.set(true);});
		if(fieldChanged.get()) {
			this.fieldsString = fieldsToString(this.fields);
			//update blocking keys
			for (BlockingKey newBk : Config.instance.getBlockingKeyExtractors().extract(this)) {
				if (!getBlockingKeys().contains(newBk)) {
					blockingKeys.add(newBk);
				}
			}
		}

		// update patient if input field is null or empty, otherwise leave old value
		AtomicBoolean inputFieldChanged = new AtomicBoolean(false);
		from.getInputFields().entrySet().stream()
				.filter(field -> !field.getValue().isEmpty() && (!this.inputFields.containsKey(field.getKey()) || this.inputFields.get(field.getKey()).isEmpty()))
				.forEach(field -> { this.inputFields.put(field.getKey(), field.getValue()); inputFieldChanged.set(true);});
		if(inputFieldChanged.get()) {
			this.inputFieldsString = fieldsToString(this.inputFields);
		}

		// update external ids
		updateIds(from.getIds().stream()
				.filter(id -> externalIdTypes.contains(id.getType()))
				.collect(Collectors.toList()));

		for (ID thisId : from.getIds()) {
			if (externalIdTypes.contains(thisId.getType())) {
				String idType = thisId.getType();
				ID myId = this.getId(idType);
				if (myId == null) {
					this.addId(thisId);
				} else {
					if (!myId.equals(thisId)) {
						throw new ConflictingDataException(
								String.format("ID of type %s should be updated with value %s but already has value %s",
										idType, thisId.getIdString(), myId.getIdString()));
					}
				}
			}
		}

		//update associatedIds
		for (AssociatedIds newAssociatedIds : from.getAssociatedIdsList()) {
			// get only external ids from new associatedIds
			List<ID> newExternalIds = newAssociatedIds.getIds().stream()
					.filter(id -> extAssociatedIdTypes.contains(id.getType()))
					.collect(Collectors.toList());

			// find an associatedIds with least one external id from the current newAssociatedIds
			Optional<AssociatedIds> matchedAssociatedIds = associatedIdsList.stream()
					.filter(a -> a.getType().equals(newAssociatedIds.getType()) &&
							a.getIds().stream().anyMatch(newExternalIds::contains))
					.findAny();

			if (matchedAssociatedIds.isPresent()) {
				// update matched associatedIds with external Ids from input patient
				matchedAssociatedIds.get().updateIds(newExternalIds);
			} else {
				// add new associatedIds from input patient
				this.associatedIdsList.add(newAssociatedIds);
			}
		}
		return this;
	}

	/**
	 * The map of fields of this patient. Field names are map keys, the
	 * corresponding Field objects are the values. The fields are not persisted by
	 * this map (therefore the {@literal @Transient} annotation), as this leads to
	 * poor performance. Instead, fields are serialized as a JSON object (handled by
	 * {@link #setFields(Map)} and de-serialized by {@link #postLoad()} upon loading
	 * from the database.
	 *
	 * @see #fieldsString
	 */
	@Transient
	private Map<String, Field<?>> fields = new HashMap<>();

	/**
	 * Serialization of the fields as JSON string for efficient storage in the
	 * database.
	 *
	 * @see #fields
	 */
	@Lob
	@JsonIgnore
	private String fieldsString;

	/**
	 * Set of IDs for this patient.
	 */
	@OneToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private Set<ID> ids = new HashSet<>();

	/**
	 * Set of associated ids for this patient.
	 */
	@OneToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH}, fetch = FetchType.LAZY)
	protected List<AssociatedIds> associatedIdsList = new ArrayList<>();

	/**
	 * Input fields as read from form (before transformation). Used to display field
	 * values as they were entered in first place.
	 *
	 * @see #fields
	 * @see #inputFieldsString
	 */
	@Transient
	private Map<String, Field<?>> inputFields = new HashMap<>();

	@Transient
	private Map<String, RecordBloomFilter> recordBloomFilters = new HashMap<>();

	/**
	 * Serialization of the input fields as JSON string for efficient storage in the
	 * database.
	 *
	 * @see #inputFields
	 */
	@Persistent
	@Strategy("de.pseudonymisierung.mainzelliste.jpa.JsonValueStrategyHandler")
	private String inputFieldsString;

	/**
	 * Set of IDs for this patient.
	 */
	@OneToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH}, fetch = FetchType.LAZY, mappedBy = "patient")
	private Set<BlockingKey> blockingKeys = new HashSet<>();

	/**
	 * True if this patient is suspected to be a duplicate.
	 */
	private boolean isTentative = false;

	/** The logging instance. */
	@Transient
	@JsonIgnore
	private final static Logger logger = LogManager.getLogger(Patient.class);

	/**
	 * The patient of which this patient is a duplicate of. If p.original is not
	 * null, p is considered a duplicate of p.original.
	 */
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JsonIgnore
	private Patient original = null;

	/** Database id. */
	@Id
	@GeneratedValue
	@JsonIgnore
	private int patientJpaId;

	/**
	 * Construct an empty patient object.
	 */
	public Patient() {
	}

	/**
	 * Construct a patient object with the specified ids and fields.
	 *
	 * @param ids               A set of ID objects that identify the patient.
	 * @param associatedIdsList A list of associated IDs that identify the patient
	 */
	public Patient(Set<ID> ids, List<AssociatedIds> associatedIdsList) {
		if (ids != null && !ids.isEmpty()) {
			this.ids = ids;
		}
		if (associatedIdsList != null && !associatedIdsList.isEmpty()) {
			this.associatedIdsList = associatedIdsList;
		}
	}

	/**
	 * Construct a patient object with the specified ids and fields.
	 *
	 * @param ids
	 *            A set of ID objects that identify the patient.
	 * @param associatedIdsList
	 * 						A list of associated IDs that identify the patient
	 * @param fields
	 *            The fields of the patient. A map with field names as keys and
	 *            the corresponding Field objects as values.
	 */
	public Patient(Set<ID> ids, List<AssociatedIds> associatedIdsList, Map<String, Field<?>> fields) {
		this(ids, associatedIdsList);
		if(fields != null) {
			this.setFields(fields);
		}
	}

	/**
	 * Get the fields of this patient.
	 *
	 * @return An unmodifiable map with field names as keys and corresponding field
	 *         objects as values. Although the map itself is unmodifiable,
	 *         modifications of its members affect the patient object.
	 */
	public Map<String, Field<?>> getFields() {
		return Collections.unmodifiableMap(fields);
	}

	public ID createAssociatedIds(String type, String idType) {
		AssociatedIds associatedIds = new AssociatedIds(type);
		ID id = associatedIds.createId(idType);
		if(id != null)
			this.associatedIdsList.add(associatedIds);
		return id;
	}

	public AssociatedIds createAssociatedIds(String type, Collection<String> idTypes) {
		AssociatedIds associatedIds = new AssociatedIds(type);
		idTypes.forEach(associatedIds::createId);
		this.associatedIdsList.add(associatedIds);
		return associatedIds;
	}

	/**
	 * found associatedIds with the given type (map key) and generate new ids with the corresponding
	 * requested id types (map value), otherwise create new associatedIds instance with new generated
	 * requested ids and added it to the patient.
	 *
	 * @param associatedIdTypesMap associatedIds as key and requested id types as value
	 * @return a map of new generated or already found requested ids with there associatedIds type as
	 * key
	 */
	public MultiValuedMap<String, ID> createAssociatedIds(
			MultiValuedMap<String, String> associatedIdTypesMap) {
		return createAssociatedIds(associatedIdTypesMap, associatedIdsList);
	}

	public MultiValuedMap<String, ID> createAssociatedIds(
			MultiValuedMap<String, String> associatedIdTypesMap, List<AssociatedIds> associatedIdsList) {
		MultiValuedMap<String, ID> generatedAssociatedIds = new ArrayListValuedHashMap<>();
		for (Entry<String, Collection<String>> entry : associatedIdTypesMap.asMap().entrySet()) {
			List<AssociatedIds> currentAssociatedIdsList = associatedIdsList.stream()
					.filter(a -> a.getType().equals(entry.getKey()))
					.collect(Collectors.toList());
			if (!currentAssociatedIdsList.isEmpty()) {
				// update existing associatedIds with new generated ids
				currentAssociatedIdsList.forEach(a -> entry.getValue().stream()
						.map(a::createId)
						.filter(Objects::nonNull)
						.forEachOrdered(id -> generatedAssociatedIds.put(id.getType(), id)) // result
				);
			} else { // generate new associatedIds for requested id type
				AssociatedIds associatedIds = createAssociatedIds(entry.getKey(), entry.getValue());
				//add created ids to result list
				entry.getValue().stream().map(associatedIds::getId)
						.forEachOrdered(id -> generatedAssociatedIds.put(id.getType(), id));
			}
		}
		return generatedAssociatedIds;
	}

	@Override
	protected Set<ID> getInternalIds() {
		return ids;
	}

	/**
	 * Get associatedIds of this patient, which contains the searched id.
	 *
	 * @return associatedIds.
	 */
	public AssociatedIds getAssociatedIds(ID id) {
		// first find the type of associatedIds
		String associatedIdsType;
		try
		{
			associatedIdsType = IDGeneratorFactory.instance.getAssociatedIdsType(id.getType());
		} catch (IllegalArgumentException e) {
			return null;
		}

		// id search on a smaller subset of associatedIds
		return associatedIdsList.stream()
				.filter(a -> a.getType().equals(associatedIdsType))
				.filter(a -> a.contain(id) ||
						IDGeneratorFactory.instance.getTransientIdTypes().contains(id.getType()) &&
								a.getTransientId(id.getType()).equals(id))
				.findFirst()
				.orElse(null);
	}

	/**
	 * Get the list of associatedIds of this patient.
	 *
	 * @return The AssociatedIds of the patient as unmodifiable list. While the List itself is
	 * unmodifiable, modification of the elements (AssociatedIds objects) affect the patient object.
	 */
	public List<AssociatedIds> getAssociatedIdsList() {
		return Collections.unmodifiableList(this.associatedIdsList);
	}

	/**
	 * Get a list of associatedIds of this patient, which contains at least one searched id.
	 *
	 * @return modification of the elements (AssociatedIds objects) affect the patient object.
	 */
	public List<AssociatedIds> getAssociatedIdsList(List<ID> searchedIds) {
		return this.associatedIdsList.stream()
				.filter(a -> searchedIds.stream().anyMatch(a::contain))
				.collect(Collectors.toList());
	}

	public ID findAssociatedId(String searchIdType, ID associatedId){
		return findId(searchIdType, this.getAssociatedIds(associatedId));
	}

	public ID findId(String searchIdType){
		return findId(searchIdType, this);
	}

	private ID findId(String searchIdType, Identifiable identifiable){
		//find id
		ID id = identifiable.getId(searchIdType);
		if(id == null) { // get non persistent id
			id = identifiable.getTransientId(searchIdType);
		}
		return id;
	}

	public boolean matchInputFields(Map<String, String> patientFieldMap) {
		for (Entry<String, String> searchField : patientFieldMap.entrySet()) {
			boolean result = false;
			Field<?> field = this.inputFields.get(searchField.getKey());
			if (field != null) {
				if (field instanceof PlainTextField plainTextField) {
					result = StringUtils.trimToEmpty(plainTextField.getValue().trim().toLowerCase()).matches(
							".*" + searchField.getValue() + ".*");
				} else if (field instanceof IntegerField integerField) {
					try {
						result = Objects.equals(integerField.getValue(),
								Integer.valueOf(searchField.getValue()));
					} catch (NumberFormatException ignored) {
					}
				} // else return false for non supported fields
			}
			if (!result) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns the input fields, i.e. as they were transmitted in the last request
	 * that modified this patient, before transformations. Used for displaying the
	 * field values as they had been entered in the first place.
	 *
	 * @return Map with field names as keys and the corresponding Field objects as
	 *         values.
	 */
	public Map<String, Field<?>> getInputFields() {
		return inputFields;
	}

	public String getInputFieldString(String name) {
		Field<?> field = inputFields.get(name);
		return field == null ? "" : field.toString();
	}

	public boolean containInvalidOrMissedRequiredField(){
		return fields.isEmpty() && !inputFields.isEmpty();
	}

	/**
	 * Gets the original of this patient, i.e. the patient of which this patient is
	 * a duplicate. More precisely: A patient p_1 is the original of a patient p_n
	 * if either p_1 and p_n are the same or if there exists a chain p_1, p_2, ... ,
	 * p_n of patients where p_k is a duplicate of p_k+1 for 1<=k<n.
	 *
	 * PID requests that find p as the best matching patient should return the PID
	 * of p.getOriginal().
	 *
	 * @return The original of this patient, returns this if this patient is not a
	 *         duplicate.
	 *
	 * @see #setOriginal(Patient)
	 */
	public Patient getOriginal() {
		Patient p = this;
		while (p.original != null && p.original != p)
			p = p.original;
		return p;
	}

	/**
	 * Returns the internal ID of the persistency engine. Needed to determine if two
	 * Patient object refer to the same database entry.
	 *
	 * @return the patientJpaId
	 */
	public int getPatientJpaId() {
		return patientJpaId;
	}

	/**
	 * Check whether this patient is the duplicate of another.
	 *
	 * @return True if this patient is the duplicate of another.
	 * @see #getOriginal()
	 * @see #setOriginal(Patient)
	 */
	public boolean isDuplicate() {
		return (this.original != null);
	}

	/**
	 * Check if this patient is suspected to be a duplicate of another one.
	 *
	 * @return True if this patient is suspected to be a duplicate of another.
	 */
	public boolean isTentative() {
		return isTentative;
	}

	/**
	 * De-serializes fields and input fields from their JSON representation.
	 * Automatically called by the JPA engine right after loading the object from
	 * the database. Fields that are missing in the database due to a later change
	 * of configuration are added with empty values.
	 */
	@PostLoad
	public void postLoad() {
		this.fields = stringToFields(this.fieldsString);
		this.inputFields = stringToFields(this.inputFieldsString);
		for (String thisFieldKey : Config.instance.getFieldKeys()) {
			if (!this.fields.isEmpty())
				this.fields.computeIfAbsent(thisFieldKey, k -> Field.build(k, ""));
			this.inputFields.computeIfAbsent(thisFieldKey, k -> Field.build(k, ""));
		}
		if(!this.fields.isEmpty()) {
			initRecordBloomFilters();
		}
	}

	/**
	 * Check whether p and this patient are the same in the database (i.e. their
	 * patientJpaId values are equal).
	 *
	 * @param p A patient object.
	 * @return true if this and p refer to the same database entry.
	 */
	public boolean sameAs(Patient p) {
		return (this.getPatientJpaId() == p.getPatientJpaId());
	}

	/**
	 * Set the fields of this patient.
	 *
	 * @param fields A map with field names as keys and corresponding Field objects
	 *               as values. The map is copied by reference.
	 */
	public void setFields(Map<String, Field<?>> fields) {
		this.fields = fields;
		this.fieldsString = fieldsToString(this.fields);
	}

	/**
	 * Set the IDs for this patient.
	 *
	 * @param ids Set of IDs. The set is copied by reference.
	 */
	public void setIds(Set<ID> ids) {
		this.ids = ids;
	}

	/**
	 * Set the transient IDs for this patient.
	 *
	 * @param transientIds Set of transient IDs
	 */
	public void setTransientIds(Set<ID> transientIds) {
		this.transientIds = transientIds;
	}

	public void setInputFieldsOnly(Map<String, Field<?>> inputFields) {
		this.inputFields = inputFields;
		this.inputFieldsString = fieldsToString(inputFields);
	}

	/**
	 * Set the input fields. Whenever a request modifies this patient object (or
	 * upon creation) the input fields as transmitted in the request, before
	 * transformation, should be set with this method. This allows redisplaying them
	 * in the admin interface or to users by means of a "readPatients" token.
	 *
	 * @param inputFields Map with field names as keys and corresponding Field
	 *                    objects as values.
	 */
	public void setInputFields(Map<String, Field<?>> inputFields) {
		this.inputFields = inputFields;
		this.inputFieldsString = fieldsToString(inputFields);
		initRecordBloomFilters();
	}

	private void initRecordBloomFilters() {
		Config.instance.getRecordBloomFilterFactories()
				.forEach((n, g) -> this.recordBloomFilters.put(n, g.generate(inputFields.entrySet().stream()
						.collect(Collectors.toMap(Entry::getKey, e -> e.getValue().toString())))));
	}

	/**
	 * Set the original of a patient (see {@link #getOriginal() for a definition}.
	 * This effectively marks this patient as a duplicate of the argument.
	 *
	 * @param original The patient which is to be set as the original of this. Can
	 *                 be null, which means that this patient is not a duplicate at
	 *                 all.
	 */
	public void setOriginal(Patient original) {
		if (original == null || original.sameAs(this)) {
			this.original = null;
			return;
		}
		// Check if operation would lead to a circular relation
		// (setting a as duplicate of b when b is a duplicate of a)
		if (original.getOriginal().sameAs(this)) {
			CircularDuplicateRelationException e = new CircularDuplicateRelationException(
					this.getId(IDGeneratorFactory.instance.getDefaultIDType()).getIdString(),
					original.getId(IDGeneratorFactory.instance.getDefaultIDType()).getIdString());
			logger.error(e.getMessage());
			throw e;
		}
		this.original = original;
	}

	/**
	 * Sets the "tentative" status of this patient, i.e. if it is suspected that the
	 * patient is a duplicate of another.
	 *
	 * @param isTentative The new tentative status.
	 */
	@Override
	public void setTentative(boolean isTentative) {
		this.isTentative = isTentative;
		this.ids.forEach(id -> id.setTentative(isTentative));
		this.associatedIdsList.forEach(a -> a.setTentative(isTentative));
	}

	/**
	 * Returns a string representation of this patient by calling toString on the
	 * map of fields, i.e. the result looks like "[vorname=Peter, nachname=Meier,
	 * ...]".
	 */
	@Override
	public String toString() {
		return fields.toString();
	}

	/**
	 * Determine if another patient object is equal to this. Two patient objects p1
	 * and p2 are considered equal if they are equal by reference (p1==p2) or if
	 * they refer to the same object in the database.
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (o == this)
			return true;
		if (o instanceof Patient && this.getPatientJpaId() == ((Patient) o).getPatientJpaId())
			return true;

		// Default case
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(patientJpaId);
	}

	/**
	 * Returns a [FHIR Patient Resource](http://hl7.org/fhir/patient.html) matching
	 * the Patient as configured in the FHIR mapping.
	 * 
	 * @return org.hl7.fhir.r4.model.Patient The Patient Resource of the Patient
	 *         instance.
	 */
	public org.hl7.fhir.r4.model.Patient toFHIR() {
		return this.toFHIR(new FHIRPathResourceGeneratorR4<>(Config.instance.getFHIRMapping()));
	}

	/**
	 * Returns a [FHIR Patient Resource](http://hl7.org/fhir/patient.html) matching
	 * the Patient as configured in the FHIR mapping.
	 * 
	 * @param FHIRPathResourceGeneratorR4<org.hl7.fhir.r4.model.Patient> An instance 
	 * 		  of a resourceGenerator to use. If you need to to create multiple FHIR 
	 *        Patients Resources, consider supplying the Resource Generator as creating 
	 *        a new Generator is quite expensive.
	 * @return org.hl7.fhir.r4.model.Patient The Patient Resource of the Patient
	 *         instance.
	 */
	public org.hl7.fhir.r4.model.Patient toFHIR(FHIRPathResourceGeneratorR4<org.hl7.fhir.r4.model.Patient> resourceGen) {
		// Get fhir mapping and default Patient ID
		Map<String, String> fhirMapping = Config.instance.getFHIRMapping();
		Map<String, String> reverseMapping = new HashMap<>();
		for(Map.Entry<String, String> entry: fhirMapping.entrySet()) {
			Field<?> entryField = this.inputFields.get(entry.getKey());
			String entryFieldValue = null;

			if (entryField != null) {
				entryFieldValue = entryField.toString();
			} else {
				// TODO: all `date.` validator fields only support calendar type outputs
				if (entry.getKey().startsWith("date.")) {
					// Get the fields and concatenate them to a date-String
					String dateFieldKeys = Config.instance.getProperty("validator." + entry.getKey() + ".fields");
					StringBuilder dateString = new StringBuilder();
					for (String dateFieldKey : dateFieldKeys.split(",")) {
						entryField = this.inputFields.get(dateFieldKey.trim());
						if(entryField != null) {
							dateString.append(this.inputFields.get(dateFieldKey.trim()).toString());
						} else {
							// a part of the date is missing, stop
							dateString = null;
							break;
						}
					}

					if(dateString != null) {
						// Get the format string of the date from the config
						String dateFormatString = Config.instance.getProperty("validator." + entry.getKey() + ".format");
						SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);
						try {
							// parse the date according to the validator
							Date date = dateFormat.parse(dateString.toString());
							// format the parsed date into isoformat for FHIR
							SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd");
							entryFieldValue = isoFormat.format(date);
						} catch (ParseException pe) {
							logger.warn("Unable to parse the patients date to FHIR: " + entry.getKey());
						}
					}
				}
			}

			if(entryFieldValue != null) {
				reverseMapping.put(entry.getValue(), entryFieldValue);
			}
		}
		ID defaultId = this.getId(IDGeneratorFactory.instance.getDefaultIDType());

		// Create Patient Resource with default ID and Terser
		resourceGen.setMapping(reverseMapping);
		org.hl7.fhir.r4.model.Patient myself = resourceGen.generateResource(org.hl7.fhir.r4.model.Patient.class);
		myself.setId(defaultId.getIdString());

		// Add all available identifiers
		for (ID id : this.ids) {
			myself.addIdentifier(id.toFHIR());
		}

		return myself;
	}

	@Override
	protected FederatedID generateFederatedId(FederatedIDGenerator factory) {
		return factory.getId(this);
	}

	public RecordBloomFilter getRecordBloomFilter(String name) {
		return recordBloomFilters.get(name);
	}

	@Override
	protected IDGenerator<? extends ID> getIDGeneratorFactory(String idType) {
		return IDGeneratorFactory.instance.getFactory(idType);
	}

	@Override
	protected List<IDGenerator<? extends ID>> getIDGeneratorFactories(Collection<String> idTypes) {
		return IDGeneratorFactory.instance.getFactories(idTypes);
	}

	@Override
	protected Collection<IDGenerator<? extends ID>> getNonExternalIdGenerators() {
		return IDGeneratorFactory.instance.getNonExternalIdGenerators().values();
	}

	public Set<BlockingKey> getBlockingKeys() {
		return blockingKeys;
	}

	public void initBlockingKeys() {
		blockingKeys = Config.instance.getBlockingKeyExtractors().extract(this);
	}

	public Set<BlockingKey> updateBlockingKeys() {
		Set<BlockingKey> removedBlockingKeys = new HashSet<>(blockingKeys);
		Set<BlockingKey> newBlockingKeys = Config.instance.getBlockingKeyExtractors().extract(this);
		for (BlockingKey newBlockingKey : newBlockingKeys){
			if(!removedBlockingKeys.remove(newBlockingKey)) {
				blockingKeys.add(newBlockingKey);
			}
		}
		blockingKeys.removeAll(removedBlockingKeys);
		return removedBlockingKeys;
	}

	/**
	 * checks if the given input field map contain all non-empty input fields
	 * @param newInputFields the given input field map
	 * @return true if the given input field map contain all non-empty input fields. Otherwise, false.
	 */
	public boolean checkIfInputFieldsIn(Map<String, Field<?>> newInputFields) {
		for(Entry<String, Field<?>> e : this.inputFields.entrySet()) {
			if(e.getValue().isEmpty()){
				continue;
			}
			Field<?> newField = newInputFields.get(e.getKey());
			if(newField == null || newField.isEmpty() || !newField.equals(e.getValue())) {
				return false;
			}
		}
		return true;
	}

	public boolean hasId(ID searchId) {
		if(IDGeneratorFactory.instance.isIdTypeExist(searchId.getType())){
			return contain(searchId);
		} else if(associatedIdsList != null){
			for(AssociatedIds associatedIds : associatedIdsList){
				if(associatedIds.contain(searchId)){
					return true;
				}
			}
		}
		return false;
	}
}
