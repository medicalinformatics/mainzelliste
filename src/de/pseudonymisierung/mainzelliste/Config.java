/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste;

import de.pseudonymisierung.mainzelliste.auth.authorizationServer.AuthorizationServer;
import de.pseudonymisierung.mainzelliste.auth.authorizationServer.AuthorizationServers;
import de.pseudonymisierung.mainzelliste.blocker.BlockingKeyExtractors;
import de.pseudonymisierung.mainzelliste.client.fttp.bloomfilter.RecordBloomFilterGenerator;
import de.pseudonymisierung.mainzelliste.client.fttp.normalization.FieldsNormalization;
import de.pseudonymisierung.mainzelliste.configuration.CallbackConfig;
import de.pseudonymisierung.mainzelliste.configuration.ConfigReader;
import de.pseudonymisierung.mainzelliste.configuration.authorizationServer.AuthorizationServerParser;
import de.pseudonymisierung.mainzelliste.configuration.claimConfiguration.ClaimConfiguration;
import de.pseudonymisierung.mainzelliste.configuration.claimConfiguration.ClaimConfigurationParser;
import de.pseudonymisierung.mainzelliste.configuration.claimConfiguration.ClaimConfigurations;
import de.pseudonymisierung.mainzelliste.crypto.CryptoUtil;
import de.pseudonymisierung.mainzelliste.crypto.Decryption;
import de.pseudonymisierung.mainzelliste.crypto.Encryption;
import de.pseudonymisierung.mainzelliste.crypto.SymmetricCipher;
import de.pseudonymisierung.mainzelliste.crypto.key.CryptoKey;
import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidConfigurationException;
import de.pseudonymisierung.mainzelliste.factory.RecordBloomFilterFactory;
import de.pseudonymisierung.mainzelliste.matcher.Matcher;
import de.pseudonymisierung.mainzelliste.util.ConfigUtils;
import de.samply.common.http.HttpConnector;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Configuration of the patient list. Implemented as a singleton object, which
 * can be referenced by Config.instance. The configuration is read from the
 * properties file specified as parameter
 * de.pseudonymisierung.mainzelliste.ConfigurationFile in context.xml (see
 * {@link java.util.Properties#load(InputStream) java.util.Properties}).
 */
public enum Config {

	/** The singleton instance */
	instance;

	/** The software version of this instance. */
	private final String version;

	/** Default paths from where configuration is read if no path is given in the context descriptor */
	private final String defaultConfigPaths[] = {"/etc/mainzelliste/mainzelliste.conf", "/WEB-INF/classes/mainzelliste.conf"};

	/** The configured fields, keys are field names, values the respective field types. */
	private final Map<String,Class<? extends Field<?>>> FieldTypes;

	private final Map<String,Class<?>> fieldTypesByClassName;

	private final Map<String, RecordBloomFilterFactory> recordBloomFilterFactories = new HashMap<>();

	/** Properties object that holds the configuration parameters. */
	private Properties props;

	private final ConfigReader configReader;

	/** The record transformer matching the configured field transformations. */
	private RecordTransformer recordTransformer;

	/** The configured matcher */
	private Matcher matcher;

	/** Enable or Disable record linkage with fields */
	private final boolean enableFieldRecordLinkage;

	private final boolean extendedPermissionCheck;
	private final boolean extendedPermissionCheckCaseSensitive;

	private int dbBatchInsertLimit;

	/** The configured blockingkey extractors */
	private BlockingKeyExtractors blockingKeyExtractors;

	/** The configured cryptographic key */
	private final Map<String, CryptoKey> cryptographicKeys = new HashMap<>();

	/** The configured encryption */
	private final Map<String, Encryption> encryptionMap = new HashMap<>();

	/** The configured decryption */
	private final Map<String, Decryption> decryptionMap = new HashMap<>();

	/** The configured symmetric cipher */
	private final Map<String, SymmetricCipher> symmetricCipherMap = new HashMap<>();

	/** The configured FHIR mapping */
	private Map<String, String> fhirMapping;

	/** Logging instance */
	private Logger logger = LogManager.getLogger(Config.class);

	/** Allowed origins for Cross Domain Resource Sharing. */
	private Set<String> allowedOrigins;

	/** Allowed headers for Cross Domain Resource Sharing */
	private String allowedHeaders;
	/** Allowed methods for Cross Domain Resource Sharing */
	private String allowedMethods;
	/** Allowed caching time for Cross Domain Resource Sharing Preflight Requests */
	private final String exposedHeaders;
	private int allowedMaxAge;

	/**List of allowed OIDC-Servers*/
	private AuthorizationServers authorizationServers;
	/**List of Claims*/
	private ClaimConfigurations claimConfigurations = new ClaimConfigurations(new HashSet<>());

	/** some gui configurations */
	private final GUI guiConfig;

	/** a http connector for getting proxy ready http clients */
	private final HttpConnector httpConnector;
	private final HttpConnector httpConnectorNoProxy;
	private final HttpClientBuilder httpClientBuilder;

	private final Map<String, List<CallbackConfig>> idGeneratorCallbackConfigs;

	/**
	 * Creates an instance. Invoked on first access to Config.instance. Reads
	 * the configuration file.
	 *
	 * @throws InternalErrorException
	 *             If an error occurs during initialization. This signals a
	 *             fatal error and prevents starting the application.
	 */
	@SuppressWarnings("unchecked")
	Config() throws InternalErrorException {
		try {
			// try to read config from general config path for all components
			props = readConfigFromEnv("MAINZELLISTE_CONFIG_DIRS");

			if (props == null) {
				// Check if path to configuration file is given in context descriptor
				ServletContext context = Initializer.getServletContext();
				String configPath = context.getInitParameter("de.pseudonymisierung.mainzelliste.ConfigurationFile");

				// try to read config from configured path
				if (configPath != null) {
					logger.info("Reading config from path {}...", configPath);
					props = readConfigFromFile(configPath);
					if (props == null) {
						throw new Error("Configuration file could not be read from provided location " + configPath);
					}
				} else {
					// otherwise, try default locations
					logger.info("No configuration file configured. Try to read from default locations...");
					for (String defaultConfigPath : defaultConfigPaths) {
						logger.info("Try to read configuration from default location {}", defaultConfigPath);
						props = readConfigFromFile(defaultConfigPath);
						if (props != null) {
							logger.info("Found configuration file at default location {}", defaultConfigPath);
							break;
						}
					}
					if (props == null) {
						throw new Error("Configuration file could not be found at any default location");
					}
				}
			}
			this.configReader = new ConfigReader(props);

			HashMap<String, String> proxyConfig = new HashMap<>();

			proxyConfig.put(HttpConnector.PROXY_HTTP_HOST, props.getProperty(HttpConnector.PROXY_HTTP_HOST, ""));
			proxyConfig.put(HttpConnector.PROXY_HTTP_PORT, props.getProperty(HttpConnector.PROXY_HTTP_PORT, "80"));
			proxyConfig.put(HttpConnector.PROXY_HTTP_USERNAME, props.getProperty(HttpConnector.PROXY_HTTP_USERNAME, ""));
			proxyConfig.put(HttpConnector.PROXY_HTTP_PASSWORD, props.getProperty(HttpConnector.PROXY_HTTP_PASSWORD, ""));

			proxyConfig.put(HttpConnector.PROXY_HTTPS_HOST, props.getProperty(HttpConnector.PROXY_HTTPS_HOST, ""));
			proxyConfig.put(HttpConnector.PROXY_HTTPS_PORT, props.getProperty(HttpConnector.PROXY_HTTPS_PORT, "80"));
			proxyConfig.put(HttpConnector.PROXY_HTTPS_USERNAME, props.getProperty(HttpConnector.PROXY_HTTPS_USERNAME, ""));
			proxyConfig.put(HttpConnector.PROXY_HTTPS_PASSWORD, props.getProperty(HttpConnector.PROXY_HTTPS_PASSWORD, ""));

			proxyConfig.put(HttpConnector.PROXY_BYPASS_PRIVATE_NETWORKS, props.getProperty(HttpConnector.PROXY_BYPASS_PRIVATE_NETWORKS, "false"));

			httpConnector = new HttpConnector(proxyConfig);

			httpConnectorNoProxy = new HttpConnector();

			// init HttpClientBuilder
			this.httpClientBuilder = configReader.initHttpClientBuilder();

			addSubConfigurationPropertiesToProps();

			logger.info("Config read successfully");
			logger.debug(props);

		} catch (IOException e)	{
			logger.fatal("Error reading configuration file. Please configure according to installation manual.", e);
			throw new Error(e);
		}

		this.recordTransformer = new RecordTransformer(props);

		try {
			Class<?> matcherClass = Class.forName("de.pseudonymisierung.mainzelliste.matcher." + props.getProperty("matcher"));
			matcher = (Matcher) matcherClass.newInstance();
			matcher.initialize(props);
			logger.info("Matcher of class {} initialized.", matcher.getClass());
		} catch (Exception e){
			logger.fatal("Initialization of matcher failed: " + e.getMessage(), e);
			throw new InternalErrorException();
		}

		// Read field types from configuration
		Pattern pattern = Pattern.compile("^field\\.(\\w+(?:\\.\\w+)*)\\.type");
		java.util.regex.Matcher patternMatcher;
		this.FieldTypes = new HashMap<>();
		this.fieldTypesByClassName = new HashMap<>();
		for (String propKey : props.stringPropertyNames()) {
			patternMatcher = pattern.matcher(propKey);
			if (patternMatcher.find())
			{
				String fieldName = patternMatcher.group(1);
				String fieldClassStr = props.getProperty(propKey).trim();
				try {
					Class<? extends Field<?>> fieldClass;
					try {
						fieldClass = (Class<? extends Field<?>>) Class.forName(fieldClassStr);
					} catch (ClassNotFoundException e) {
						// Try with "de.pseudonymisierung.mainzelliste..."
						fieldClass = (Class<? extends Field<?>>) Class.forName("de.pseudonymisierung.mainzelliste." + fieldClassStr);
					}
					this.FieldTypes.put(fieldName, fieldClass);
					this.fieldTypesByClassName.put(fieldClass.getName(), fieldClass);
					logger.debug("Initialized field {} with class {}", fieldName, fieldClass);
				} catch (Exception e) {
					logger.fatal("Initialization of field " + fieldName + " failed: ", e);
					throw new InternalErrorException();
				}
			}
		}
		this.fieldTypesByClassName.put(CompoundField.class.getName(), CompoundField.class);
		this.fieldTypesByClassName.put(HashedField.class.getName(), HashedField.class);

		//read record bloom filter generators
		ConfigUtils.getVariableSubProperties(props, "recordBloomFilter").forEach((n, c) ->
			recordBloomFilterFactories.put(n, new RecordBloomFilterFactory(new FieldsNormalization(c),
					ConfigUtils.createInstance("de.pseudonymisierung.mainzelliste.client.fttp.bloomfilter.",
					c.getProperty("generator", "").trim(), RecordBloomFilterGenerator.class, Properties.class, c))));

		// Parse blockingkey extractors after the matcher as the blocking may depend on the matcher config
		this.blockingKeyExtractors = new BlockingKeyExtractors(props);

		// read if record linkage with fields is enabled
		this.enableFieldRecordLinkage = ConfigUtils.readValue(props, "enableFieldRecordLinkage", true);

		// read if record linkage with fields is enabled
		this.dbBatchInsertLimit = ConfigUtils.readValue(props, "dbBatchInsertLimit", 100000);

		// read if extendedPermissionCheck is enabled
		this.extendedPermissionCheck = ConfigUtils.readValue(props, "extendedPermissionCheck", false);
		this.extendedPermissionCheckCaseSensitive = ConfigUtils.readValue(props, "extendedPermissionCheck.caseSensitive", false);

		// Read allowed origins for cross domain resource sharing (CORS)
		allowedOrigins = new HashSet<>();
		String allowedOriginsString = props.getProperty("servers.allowedOrigins");
		if (allowedOriginsString != null)
			allowedOrigins.addAll(Arrays.asList(allowedOriginsString.trim().split("[;,]")));

		allowedHeaders = props.getProperty("servers.allowedHeaders","mainzellisteApiVersion,mainzellisteApiKey")
				.trim()
				.replace(';', ',');
		allowedMethods = props.getProperty("servers.allowedMethods", "OPTIONS,GET,POST")
				.trim()
				.replace(';', ',');
		exposedHeaders = props.getProperty("servers.exposedHeaders","X-Total-Count")
				.trim()
				.replace(';', ',');

		try {
			String allowedMaxAgeString = props.getProperty("servers.allowedMaxAge", "600");
			allowedMaxAge = Integer.parseInt(allowedMaxAgeString);
			if(allowedMaxAge < -1){
				throw new InvalidConfigurationException("The servers.allowedMaxAge parameter is in an unexpected format: " + allowedMaxAge + ". Expected number greater than -1");
			}
		} catch (NumberFormatException e){
			throw new InvalidConfigurationException("The servers.allowedMaxAge parameter is in an unexpected format: " + allowedMaxAge + ". Expected numeric value", e);
		}

		// Read Callbacks
		idGeneratorCallbackConfigs = this.configReader.readIDGeneratorsCallbacks(FieldTypes.keySet());

		// Read cryptographic key
		ConfigUtils.getVariableSubProperties(props, "crypto.key").forEach((v, p) -> {
			try {
				cryptographicKeys.put(v, CryptoUtil.readKey(p.getProperty("type"),
						readFileFromURL(p.getProperty("uri").trim())));
			} catch (IOException e) {
				InvalidConfigurationException exception = new InvalidConfigurationException(
						"crypto.key." + v + ".uri", p.getProperty("uri"), e);
				logger.error(exception.getMessage(), e);
				throw exception;
			} catch (IllegalArgumentException e) {
				// unsupported key type
				InvalidConfigurationException exception = new InvalidConfigurationException(
						"crypto.key." + v + ".type", p.getProperty("type"), e);
				logger.error(exception.getMessage(), e);
				throw exception;
			} catch (UnsupportedOperationException e) {
				InvalidConfigurationException exception = new InvalidConfigurationException(
						"crypto.key." + v + ".*", "Instantiation of a cryptographic key failed", e);
				logger.error(exception.getMessage(), e);
				throw exception;
			}
		});

		// Read cipher
		ConfigUtils.getVariableSubProperties(props,"crypto.cipher").forEach((v, p) -> {
			try {
				Class<?> cipherClass = CryptoUtil.findCipherClass(p.getProperty("type"));
				if(SymmetricCipher.class.isAssignableFrom(cipherClass)){
					symmetricCipherMap.put(v, SymmetricCipher.getInstance(cipherClass,
							cryptographicKeys.get(p.getProperty("key"))));
				} else if (Encryption.class.isAssignableFrom(cipherClass)) {
					encryptionMap.put(v, Encryption.getInstance(cipherClass,
							cryptographicKeys.get(p.getProperty("key"))));
				} else if (Decryption.class.isAssignableFrom(cipherClass)) {
					decryptionMap.put(v, Decryption.getInstance(cipherClass,
							cryptographicKeys.get(p.getProperty("key"))));
				} else {
					throw new IllegalArgumentException(
							"the given encryption type " + p.getProperty("type") + " is not a cipher");
				}
			} catch (InvalidKeySpecException e) {
				InvalidConfigurationException exception = new InvalidConfigurationException(
						"crypto.cipher." + v + ".key", p.getProperty("key"), e);
				logger.error(exception.getMessage(), e);
				throw exception;
			} catch (IllegalArgumentException e) { // unsupported encryption type
				InvalidConfigurationException exception = new InvalidConfigurationException(
						"crypto.cipher." + v + ".type", p.getProperty("type"), e);
				logger.error(exception.getMessage(), e);
				throw exception;
			} catch (UnsupportedOperationException e) {
				InvalidConfigurationException exception = new InvalidConfigurationException(
						"crypto.cipher." + v + ".*", "Instantiation of cipher failed", e);
				logger.fatal(exception.getMessage(), e);
				throw exception;
			}
		});

		// Read FHIR Mapping
		if(Boolean.parseBoolean(props.getProperty("experimental.fhir.enable", "false"))) {
			logger.warn("You have enabled the FHIR API feature. Note that this feature is experimental and is subject to change without any notice!");
			this.fhirMapping = props.stringPropertyNames()
				.stream()
				.filter(k -> k.startsWith("fhir.map."))
				.collect(Collectors.toMap(k -> k.substring("fhir.map.".length()), props::getProperty));
			// TODO: Preflight check: are the mapping configuration consistent with fields and dates?
		}

		// Read version number provided by pom.xml
		version = readVersion();

		// read gui configuration
		this.guiConfig = new GUI(props);

		Set<AuthorizationServer> oidcServerSet = AuthorizationServerParser.parseOIDCServerConfiguration(props);
		this.authorizationServers = new AuthorizationServers(oidcServerSet);
		Set<ClaimConfiguration> claimConfigurationSet = ClaimConfigurationParser.parseConfiguration(props, this.authorizationServers);
		this.claimConfigurations = new ClaimConfigurations(claimConfigurationSet);

	}

	/**
	 * Attempts to read config from path specified in an environment variable.
	 * @param env The name of the environment variable. Different paths should be separated by "::"
	 * @return The configuration as a Properties object or null if the given
	 *         file was not found.
	 */
	public Properties readConfigFromEnv(String env) {
		Properties props = null;

		if (System.getenv(env) != null) {
			String configDirsAsString = System.getenv(env);
			String[] configDirs =  configDirsAsString.split("::");
			for (String configDir : configDirs) {
				File configFile = new File (configDir, "mainzelliste.conf");
				logger.info("Try to read configuration from path " + configFile.getAbsolutePath() + "...");
				try {
					props = readConfigFromFile(configFile.getAbsolutePath());
				} catch (IOException e)	{
					logger.fatal("Error reading configuration file. Please configure according to installation manual.", e);
					throw new Error(e);
				}
				if (props != null) {
					break;
				}
			}

		}
		return props;
	}

	/**
     * Reads subConfiguration.{n}.uri(s) and adds the values to Config.props
     *
     */
    private void addSubConfigurationPropertiesToProps() {
		// get custom config attributes
		List<String> subConfigurations = props.stringPropertyNames().stream().filter(k -> Pattern.matches("subConfiguration\\.\\d+\\.uri", k.trim()))
				.map(k -> Integer.parseInt(k.split("\\.")[1])).sorted()
				.map(d -> "subConfiguration." + d + ".uri")
				.collect(Collectors.toList());


        for (String attribute : subConfigurations) {
            // read custom configuration properties from url
            Properties subConfigurationProperties;
            try {
                URL subConfigurationURL = new URL(props.getProperty(attribute).trim());
                subConfigurationProperties = readConfigFromUrl(subConfigurationURL);
                logger.info("Sub configuration file {} = {} has been read in.", attribute, subConfigurationURL);
            }  catch (MalformedURLException e) {
                logger.fatal("Custom configuration file '" + attribute +
                        "' could not be read from provided URL " + attribute, e);
                throw new Error(e);
            } catch (IOException e) {
                logger.fatal("Error reading custom configuration file '" + attribute +
                        "'. Please configure according to installation manual.", e);
                throw new Error(e);
            }

            // merge configuration files
            for (String currentKey : subConfigurationProperties.stringPropertyNames()) {
                if(props.containsKey(currentKey) && !subConfigurationProperties.getProperty(currentKey).trim()
                        .equals(props.getProperty(currentKey).trim())) {
                    String msg = "Sub configuration tries to override main config or former sub config values. This is not allowed. Property key: " + currentKey +
                            ", custom configuration file: " + attribute;
                    logger.fatal(msg);
                    throw new Error(msg);
                }
            }
            props.putAll(subConfigurationProperties);
        }
    }

    /**
	 * Get the {@link RecordTransformer} instance configured for this instance.
	 * @return The {@link RecordTransformer} instance configured for this instance.
	 */
	public RecordTransformer getRecordTransformer() {
		return recordTransformer;
	}

	public Map<Character, String> getTransformerCharReplacements(){
		return recordTransformer.getCharReplacements();
	}

	public Set<Character> getTransformerCharDelimiterChars() {
		return recordTransformer.getCharDelimiters();
	}

	/**
	 * Get the {@link BlockingKeyExtractors} instance configured for this instance.
	 * @return The {@link BlockingKeyExtractors} instance configured for this instance.
	 */
	public BlockingKeyExtractors getBlockingKeyExtractors() {
		return this.blockingKeyExtractors;
	}

	/**
	 * Get all FHIR mappings configured for this instance.
	 * @return A HashMap of fieldName -> FHIRTerserPath mappings.
	 */
	public Map<String, String> getFHIRMapping() {
		return this.fhirMapping;
	}

	/**
	 * Get configuration as Properties object.
	 * @return Properties object as read from the configuration file.
	 */
	public Properties getProperties() {
		return props;
	}

	/**
	 * Get the matcher configured for this instance.
	 * @return The matcher configured for this instance.
	 */
	public Matcher getMatcher() {
		return matcher;
	}

	/**
	 * Get the specified property from the configuration.
	 * @param propKey Property name.
	 * @return The property value or null if no such property exists.
	 */
	public String getProperty(String propKey){
		return props.getProperty(propKey);
	}

	/**
	 * Get the names of fields configured for this instance.
	 * @return The names of fields configured for this instance.
	 */
	public Set<String> getFieldKeys(){
		return FieldTypes.keySet();
	}

	/**
	 * Get the type of the given field.
	 * @param fieldKey Name of the field as defined in configuration.
	 * @return The field type as the matching subclass of Field.
	 */
	public Class<? extends Field<?>> getFieldType(String fieldKey){
		return FieldTypes.get(fieldKey);
	}

	public Class<?> getFieldTypeByClass(String fieldKey){
		return fieldTypesByClassName.get(fieldKey);
	}

	/**
	 * Check whether a field with the given name is configured.
	 * @param fieldName The field name to check
	 * @return true if field fieldName is configured.
	 */
	public boolean fieldExists(String fieldName) {
		return this.FieldTypes.containsKey(fieldName);
	}

	/**
	 * Get the distribution instance (e.g. the name of the project this instance
	 * runs for).
	 *
	 * @return The value of configuration parameter "dist".
	 * */
	public String getDist() {
		return getProperty("dist");
	}

	/**
	 * Get the software version of this instance.
	 *
	 * @return The software version of this instance.
	 *
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Checks whether this instance is run in debug mode, i.e. authentication is
	 * disabled and tokens are not invalidated.
	 *
	 * @return true if debug mode is enabled.
	 */
	public boolean debugIsOn()
	{
		String debugMode = this.props.getProperty("debug");
		return (debugMode != null && debugMode.equals("true"));
	}

	/**
	 * Checks whether this instance has audit trailing enabled by the administrator.
	 *
	 * @return true if audit trail is enabled.
	 */
	public boolean auditTrailIsOn() {
		String audittrail = this.props.getProperty("gcp.audittrail");
		return (audittrail != null && audittrail.equals("true"));
	}

	public boolean isExtendedPermissionCheck() {
		return extendedPermissionCheck;
	}

	public boolean isExtendedPermissionCheckCaseSensitive() {
		return extendedPermissionCheckCaseSensitive;
	}

	/**
	 * Checks whether the given origin is allowed. Used to check the origin in
	 * Cross Domain Resource Sharing.
	 *
	 * @param origin
	 *            The origin to check.
	 * @return true if resource sharing with this origin is allowed.
	 */
	public boolean originAllowed(String origin) {
		return this.allowedOrigins.contains(origin);
	}

	/**
	 * Returns the configured allowed Headers for Cross Domain Resource Sharing
	 * @return list of headers set in config servers.allowedHeaders, default is: "mainzellisteApiVersion,mainzellisteApiKey"
	 */
	public String getAllowedHeaders() {
		return String.join(",", this.allowedHeaders);
	}

	/**
	 * Returns the configured allowed methods for Cross Domain Resource Sharing
	 * @return list of headers set in config servers.allowedMethods, default is: "OPTIONS,GET,POST"
	 */
	public String getAllowedMethods() {
		return String.join(",", this.allowedMethods);
	}

	public String getExposedHeaders() {
		return String.join(",", this.exposedHeaders);
	}

	/**
	 * Returns the configured allowed time CORS Preflight requests should be cached
	 * @return list of headers set in config servers.allowedMaxAge, default is: "600"
	 */
	public int getAllowedMaxAge() {
		return this.allowedMaxAge;
	}

	/**
	 * Get the logo file from the path defined by configuration parameter
	 * 'operator.logo'.
	 *
	 * @return The file object. It is checked that the file exists.
	 * @throws FileNotFoundException
	 *             if the logo file cannot be found at the specified location.
	 */
	public URL getLogo() throws FileNotFoundException {
		String logoFileName = this.getProperty("operator.logo");
		if (logoFileName == null || logoFileName.equals(""))
			throw new FileNotFoundException("No logo file configured.");
		File logoFile;
		URL logoURL;
		try {
			logoURL = Initializer.getServletContext().getResource(logoFileName);
		} catch (MalformedURLException e) {
			throw new FileNotFoundException(e.toString());
		}
		if (logoURL != null) {
			return logoURL;
		} else {
			logoFile = new File(logoFileName);

			try {
				if (logoFile.exists())
					return logoFile.toURI().toURL();
				throw new FileNotFoundException("No logo file found at " + logoFileName + ".");
			} catch (MalformedURLException e) {
				throw new FileNotFoundException("No logo file found at " + logoFileName + ". (" + e.toString() + ")");
			}
		}
	}

	/**
	 * Read configuration from the given file. Tries to read the file in the
	 * following order:
	 * <ol>
	 * <li>From inside the application via getResourceAsStream().
	 * <li>From the file system
	 * </ol>
	 *
	 * @param configPath
	 *            Path to configuration file.
	 * @return The configuration as a Properties object or null if the given
	 *         file was not found.
	 * @throws IOException
	 *             If an I/O error occurs while reading the configuration file.
	 */
	private Properties readConfigFromFile(String configPath) throws IOException {
		ServletContext context = Initializer.getServletContext();
		// First, try to read from resource (e.g. within the war file)
		InputStream configInputStream = context.getResourceAsStream(configPath);
		// Else: read from file System
		if (configInputStream == null) {
			File f = new File(configPath);
			if (f.exists())
				configInputStream = new FileInputStream(configPath);
			else return null;
		}
		return readConfigFromInputStream(configInputStream);
	}

	/**
	 * Read configuration from the given URL.
	 *
	 * @param url
	 *            url of the configuration file.
	 * @return The configuration as a Properties object
	 * @throws IOException
	 *             If an I/O error occurs while reading the configuration file.
	 */
	private Properties readConfigFromUrl(URL url) throws IOException {
		InputStream inputStream;
		if(url.getProtocol().equals("file")) {
			inputStream = url.openStream();
		} else {
			HttpHost httpHost = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());
			HttpClient httpClient = httpConnector.getHttpClient(httpHost);
			HttpResponse response = httpClient.execute(httpHost, new HttpGet(url.getPath()));
			inputStream = response.getEntity().getContent();
		}

		try (BufferedInputStream in = new BufferedInputStream(inputStream) ) {
			return readConfigFromInputStream(in);
		}
	}

	/**
	 * Read configuration from the given input stream.
	 *
	 * @param configInputStream
	 *            input stream of the configuration file.
	 * @return The configuration as a Properties object
	 * @throws IOException
	 *             If an I/O error occurs while reading the configuration file.
	 */
	private Properties readConfigFromInputStream(InputStream configInputStream) throws IOException {
		Reader reader = new InputStreamReader(configInputStream, StandardCharsets.UTF_8);
		Properties props = new Properties();
		props.load(reader);
		// trim property values
		for (String key : props.stringPropertyNames()) {
			String value = props.getProperty(key);
			if (!value.equals(value.trim())) {
				props.setProperty(key, value.trim());
			}
		}
		configInputStream.close();
		return props;
	}

	/**
	 * Get resource bundle for the best matching locale of the request. The best
	 * matching locale is determined by iterating through the following list of
	 * language codes and choosing the first for which a resource bundle file is
	 * found:
	 * <ol>
	 * <li>If present, value of the URL parameter 'language'.
	 * <li>Any language codes provided in HTTP header "Accept-Language" in the
	 * given order.
	 * <li>"en" as fallback (i.e. English is the default language).
	 * </ol>
	 *
	 * @param req
	 *            The servlet request.
	 * @return The matching resource bundle.
	 */
	public ResourceBundle getResourceBundle(HttpServletRequest req) {

		// Look if there is a cached ResourceBundle instance for this request
		final String storedResourceBundleKey = this.getClass().getName() + ".selectedResourceBundle";
		Object storedResourceBundle = req.getAttribute(storedResourceBundleKey);
		if (storedResourceBundle != null && storedResourceBundle instanceof ResourceBundle)
				return (ResourceBundle) storedResourceBundle;

		// Base name of resource bundle files
		String baseName = "MessageBundle";

		// Build a list of preferred locales
		LinkedList<Locale> preferredLocales = new LinkedList<Locale>();
		// First preference: Fixed language in configuration file
		String languageConfig = Config.instance.getProperty("language");
		if (languageConfig != null) {
			Locale configLocale = new Locale(languageConfig);
			preferredLocales.add(configLocale);
		}
		// Second preference: URL parameter "language", if set.
		String languageParam = req.getParameter("language");
		if (languageParam != null) {
			Locale urlLocale = new Locale(languageParam);
			preferredLocales.add(urlLocale);
		}

		// Next, add all preferred locales from the request (header "Language").
		Enumeration<Locale> requestLocales = req.getLocales();
		while (requestLocales.hasMoreElements()) {
			preferredLocales.add(requestLocales.nextElement());
		}

		// Finally, add English as fallback
		preferredLocales.add(Locale.ENGLISH);

		// Instantiate control object for searching resources without using default locale,
		// as default locale is searched for explicitly.
		ResourceBundle.Control noFallback = ResourceBundle.Control.getNoFallbackControl(ResourceBundle.Control.FORMAT_DEFAULT);

		// Iterate over list of locales and return the first matching resource bundle
		for (Locale thisLocale : preferredLocales) {
			  // Try to get ResourceBundle for current locale
			   try {
				   ResourceBundle selectedResourceBundle = ResourceBundle.getBundle("MessageBundle", thisLocale, noFallback);
				   // Cache ResourceBundle object for further calls to this method within the same HTTP request
				   req.setAttribute(storedResourceBundleKey, selectedResourceBundle);
				   return selectedResourceBundle;
			   } catch (MissingResourceException e) {
				   // Silently try next preferred locale
			   }
		}

		// If this line is reached, no resource bundle (including system default) could be found, which is an error
		throw new Error ("Could not find resource bundle with base name '" + baseName + "' for any of the locales: " + preferredLocales);
	}

	/**
	 * Returns application name and version for use in HTTP headers Server and
	 * User-Agent. Format: "Mainzelliste/x.y.z", with the version as returned by
	 * {@link #getVersion()}.
	 *
	 * @return The version string.
	 */
	public String getUserAgentString() {
		return "Mainzelliste/" + getVersion();
	}

	/**
	 * Get configured log level (DEBUG by default).
	 * @return The log level.
	 */
	Level getLogLevel() {
		String level = this.props.getProperty("log.level");
		Level ret = Level.DEBUG;

		if (level == null || level.equals("DEBUG"))
			ret = Level.DEBUG;
		else if (level.equals("WARN"))
			ret = Level.WARN;
		else if (level.equals("ERROR"))
			ret = Level.ERROR;
		else if (level.equals("FATAL"))
			ret = Level.FATAL;
		else if (level.equals("INFO"))
			ret = Level.INFO;

		return ret;
	}

	/**
	 * Get gui configuration
	 */
	public GUI getGuiConfiguration()
	{
		return guiConfig;
	}

	/**
	 * Read version string from properties file "version.properties",
	 * which is copied from pom.xml by Maven.
	 * @return The version string.
	 */
	private String readVersion() {
		try {
			Properties props = new Properties();
			InputStream versionInputStream = Initializer.getServletContext().getResourceAsStream("/WEB-INF/classes/version.properties");
			if (versionInputStream == null) {
				// Try alternate way of reading file (necessary for running test via the Jersey Test Framework)
				versionInputStream = this.getClass().getResourceAsStream("/version.properties");
			}
			if (versionInputStream == null) {
				throw new Error("File version.properties not found!");
			}
			props.load(versionInputStream);
			String version = props.getProperty("version");
			if (version == null)
				throw new Error("property 'version' undefined in version.properties");
			return version;
		} catch (IOException e) {
			throw new Error ("I/O error while reading version.properties", e);
		}
	}

	/**
	 * Returns the Claims which are defined in the configuration file
	 * @return the stored claims
	 */
	public ClaimConfigurations getClaimConfigurationSet() {
		return claimConfigurations;
	}

	/**
	 * Returns the OIDCServers which are defined in the configuration file
	 * @return the stored OIDCServers
	 */
	public AuthorizationServers getAuthorizationServers() {
		return authorizationServers;
	}

	public Encryption getEncryption(String encryptionName) {
		return encryptionMap.get(encryptionName);
	}

	public Decryption getDecryption(String decryptionName) {
		return decryptionMap.get(decryptionName);
	}

	public SymmetricCipher getSymmetricCipher(String cipherName) {
		return symmetricCipherMap.get(cipherName);
	}

	public Map<String, RecordBloomFilterFactory> getRecordBloomFilterFactories() {
		return recordBloomFilterFactories;
	}

	public HttpConnector getHttpConnector() {
		return httpConnector;
	}

	public HttpConnector getHttpConnectorNoProxy() {
		return httpConnectorNoProxy;
	}

	public HttpHost getProxyUrl(String target) throws MalformedURLException {
		return httpConnector.getProxy(target);
	}

	public HttpClientBuilder getHttpClientBuilder() {
		return httpClientBuilder;
	}

	public RecordBloomFilterGenerator getRecordBloomFilterGenerator(String name) {
		return recordBloomFilterFactories.get(name).getGenerator();
	}

	public boolean isFieldRecordLinkageEnabled() {
		return enableFieldRecordLinkage;
	}

	public int getDbBatchInsertLimit() {
		return dbBatchInsertLimit;
	}

	public Map<String, List<CallbackConfig>> getCallbackConfigs() {
		return idGeneratorCallbackConfigs;
	}

	public static class GUI {
		/** url of control number generator */
		public final String cngUrl;
		/** api key of control number generator */
		public final String cgnApiKey;
		/** version of mainzelliste (ml) rest api */
		public final String mlApiVersion;

		private GUI(Properties properties) {
			this.cngUrl = Optional.ofNullable((String)properties.get("gui.cng.url")).orElse("");
			this.cgnApiKey = Optional.ofNullable((String)properties.get("gui.cng.apiKey")).orElse("");
			this.mlApiVersion = Optional.ofNullable((String)properties.get("gui.ml.apiVersion")).orElse("");
		}
	}

	// HELPERS

	/**
	 * read file from the given url
	 * @param fileURL url of the file (e.g. file:///etc/keys/private.der)
	 * @return byte array
	 */
	private byte[] readFileFromURL(String fileURL) throws IOException {
		try (InputStream inputStream = new URL(fileURL).openStream() ) {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[0xFFFF];
			for (int len = inputStream.read(buffer); len != -1; len = inputStream.read(buffer)) {
				outputStream.write(buffer, 0, len);
			}
			return outputStream.toByteArray();
		}
	}
}
