package de.pseudonymisierung.mainzelliste.factory;

import de.pseudonymisierung.mainzelliste.client.fttp.bloomfilter.RecordBloomFilter;
import de.pseudonymisierung.mainzelliste.client.fttp.bloomfilter.RecordBloomFilterGenerator;
import de.pseudonymisierung.mainzelliste.client.fttp.normalization.FieldsNormalization;
import java.util.Map;

public class RecordBloomFilterFactory {

  private final FieldsNormalization fieldsNormalization;
  private final RecordBloomFilterGenerator generator;

  public RecordBloomFilterFactory(FieldsNormalization fieldsNormalization,
      RecordBloomFilterGenerator generator) {
    this.fieldsNormalization = fieldsNormalization;
    this.generator = generator;
  }

  public RecordBloomFilter generate(Map<String, String> fields) {
    return generator.generate(fieldsNormalization.process(fields));
  }

  public RecordBloomFilterGenerator getGenerator() {
    return generator;
  }
}
