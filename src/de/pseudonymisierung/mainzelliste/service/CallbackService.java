package de.pseudonymisierung.mainzelliste.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.Field;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.configuration.CallbackConfig;
import de.pseudonymisierung.mainzelliste.exceptions.CallbackFailedException;
import de.pseudonymisierung.mainzelliste.util.gson.RecordExclusionStrategy;
import de.pseudonymisierung.mainzelliste.webservice.commons.Callback;
import de.pseudonymisierung.mainzelliste.webservice.commons.CallbackFactory;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CallbackService {

  private static final Logger logger = LogManager.getLogger(CallbackService.class);
  private static final Gson gson = new GsonBuilder().setExclusionStrategies(new RecordExclusionStrategy()).create();

  public void sendCallbacks(HttpServletRequest request, String tokenId,
      Collection<String> generatedIdTypes, List<ID> newIds, Map<String, Field<?>> inputFields) {
    executes(Config.instance.getCallbackConfigs().entrySet().stream()
        .filter(e -> generatedIdTypes.contains(e.getKey()))
        .flatMap(e -> e.getValue().stream().map(c ->
            createCallback(request, tokenId, c, newIds, generatedIdTypes, inputFields))
        )
        .toList()
    );
  }

  public void sendCallback(HttpServletRequest request, String tokenId, String url,
      List<ID> newIds, Collection<String> resultIdTypes,
      Map<String, Field<?>> fields, Collection<Double> similarityScores) {
    executes(Collections.singletonList(createCallback(request, tokenId, url, newIds, resultIdTypes,
        fields, similarityScores)));
  }

  protected Callback createCallback(HttpServletRequest request, String tokenId,
      CallbackConfig callbackConfig, List<ID> newIds, Collection<String> generatedIdTypes,
      Map<String, Field<?>> inputFields) {
    Collection<String> resultIdTypes = callbackConfig.idTypes() == null ? generatedIdTypes : callbackConfig.idTypes();
    return CallbackFactory.createCallback(Servers.instance.getRequestApiVersion(request),
        callbackConfig.url(), callbackConfig.headers(), tokenId, filterIds(newIds, resultIdTypes),
        filterFields(inputFields, callbackConfig.fields()), null, null);
  }

  protected Callback createCallback(HttpServletRequest request, String tokenId, String url,
      List<ID> newIds, Collection<String> resultIdTypes, Map<String, Field<?>> fields,
      Collection<Double> similarityScores) {
    return CallbackFactory.createCallback(Servers.instance.getRequestApiVersion(request), url,
        new HashMap<>(), tokenId, filterIds(newIds, resultIdTypes), fields, null, similarityScores);
  }

  public void executes(List<Callback> callbacks) {

    try (CloseableHttpClient httpClient = Config.instance.getHttpClientBuilder().build()) {
      for (Callback callback : callbacks) {
        logger.info(() -> "Executing Mainzelliste Callback on url " + callback.url() + " with "
            + "tokenId '" + callback.tokenId() + "'");
        sendCallback(httpClient, callback);
      }
    } catch (IOException e) {
      throw new CallbackFailedException(e.getMessage(), callbacks.get(0).url(),
          Status.GATEWAY_TIMEOUT, logger::error);
    }
  }

  private void sendCallback(CloseableHttpClient httpClient, Callback callback) {
    try (CloseableHttpResponse response = httpClient.execute(createRequest(callback))) {
      StatusLine statusLine = response.getStatusLine();
      if ((statusLine.getStatusCode() < 200) || statusLine.getStatusCode() >= 300) {
        throw new CallbackFailedException("Received invalid status '" + statusLine
            + "' from callback", callback.url(), Status.BAD_GATEWAY, logger::error);
      }
    } catch (IOException e) {
      logger.error("Request to callback url '[{}]' failed. Reason: {}",
          callback.url(), e.getMessage());
    }
  }

  private HttpPost createRequest(Callback callback) {
    HttpPost httpPost = new HttpPost(callback.url());
    httpPost.setHeader("Content-Type", MediaType.APPLICATION_JSON);
    httpPost.setHeader("User-Agent", Config.instance.getUserAgentString());
    callback.headers().entrySet().stream().filter( e -> !StringUtils.isEmpty(e.getValue()))
        .forEach( e -> httpPost.setHeader(e.getKey(), e.getValue()));
    httpPost.setEntity(new StringEntity(gson.toJson(callback), ContentType.APPLICATION_JSON));
    return httpPost;
  }

  /***
   * return only id with the given id types
   * @param ids id list to filter
   * @param idTypes id types
   * @return filtered id list
   */
  public List<ID> filterIds(List<ID> ids, Collection<String> idTypes) {
    if (idTypes == null) {
      return ids;
    }
    List<ID> filteredIds = new ArrayList<>();
    for (ID id : ids) {
      if (idTypes.contains(id.getType())) {
        filteredIds.add(id);
      }
    }
    return filteredIds;
  }

  public Map<String, Field<?>> filterFields(Map<String, Field<?>> fields, Collection<String> fieldNames) {
    if (fieldNames == null) {
      return new HashMap<>();
    }
    Map<String, Field<?>> filteredFields = new HashMap<>();
    for (String fieldName : fieldNames) {
      Field<?> field = fields.get(fieldName);
      if (field != null) {
        filteredFields.put(fieldName, field);
      }
    }
    return filteredFields;
  }
}
