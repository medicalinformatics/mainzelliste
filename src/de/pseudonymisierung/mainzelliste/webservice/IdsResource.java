package de.pseudonymisierung.mainzelliste.webservice;

import com.google.gson.Gson;
import de.pseudonymisierung.mainzelliste.AssociatedIds;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.IDGenerator;
import de.pseudonymisierung.mainzelliste.IDGeneratorFactory;
import de.pseudonymisierung.mainzelliste.Identifiable;
import de.pseudonymisierung.mainzelliste.Patient;
import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.api.IdResponse;
import de.pseudonymisierung.mainzelliste.dto.Persistor;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidTokenException;
import de.pseudonymisierung.mainzelliste.exceptions.UnauthorizedException;
import de.pseudonymisierung.mainzelliste.service.CallbackService;
import jakarta.inject.Singleton;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Path("/ids")
@Singleton
public class IdsResource {

  private static final Logger logger = LogManager.getLogger(IdsResource.class);
  private final Gson gson = new Gson();

  private static final CallbackService callbackService = new CallbackService();

  @Path("/{idType}/")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response getIdResult(@PathParam("idType") String idType,
      @QueryParam("tokenId") String tokenId,
      @Context HttpServletRequest request,
      @Context UriInfo context) {

    CreateIdsToken token = (CreateIdsToken) readToken(tokenId);
    if(!token.containIdTypesIgnoreCase(idType.trim()))
      throw new UnauthorizedException("Your permissions do not allow to generate an id with the "
          + "requested id type '" + idType.trim() + "'");
    // check id type and find the type of AssociatedIds
    String associatedIdsType = getAssociatedIdsType(idType.trim());

    List<IdResponse> idsResponse = new ArrayList<>();
    for (ID searchID : token.getSearchIds()) {
      // find patient or AssociatedIds object
      Identifiable identifiable = getIdentifiable(searchID, associatedIdsType);

      // generate id
      ID id = null;
      if (associatedIdsType != null && identifiable instanceof Patient patient) {
        id = patient.createAssociatedIds(associatedIdsType, idType.trim());
      } else if (identifiable != null) {
        id = identifiable.createId(idType);
      }

      //send callbacks
      if(identifiable instanceof Patient patient) {
        callbackService.sendCallbacks(request, token.getId(), Collections.singletonList(idType),
            Collections.singletonList(id), patient.getInputFields());
      } else if (identifiable instanceof AssociatedIds) {
        // TODO sendcallbacks if identifiable is instance of AssociatedIds
        logger.error("Not supported yet: defined callbacks in the idgenerator config. of '"
            + searchID.getType() + "' can't be executed. Search id type is an associated Id ");
      }

      // deserialize id to json
      IdResponse idResponse = new IdResponse();
      if (id != null) {
        updateIdentifiable(identifiable);
        String idString = id.getEncryptedIdStringFirst();
        URI uri = context.getBaseUriBuilder().path(IdsResource.class)
            .path("/{idtype}/{idvalue}")
            .build(id.getType().trim(), idString);
        idResponse = new IdResponse(id.getType(), idString, id.isTentative(), uri.toString());
      }
      idsResponse.add(idResponse);
    }

    return Response.ok(gson.toJson(idsResponse)).build();
  }

  private Identifiable getIdentifiable(ID id, String associatedIdsType) {
    // Note: return 'AssociatedIds' object, if the associated resource id and search id types are related
    return associatedIdsType != null && IDGeneratorFactory.instance.containAssociatedIdType(
        associatedIdsType, id.getType()) ? Persistor.instance.getAssociatedIds(id) :
        Persistor.instance.getPatient(id);
  }

  private void updateIdentifiable(Identifiable identifiable) {
    if (identifiable instanceof Patient patient) {
      Persistor.instance.updatePatient(patient, true);
    } else {
      Persistor.instance.updateAssociatedIds((AssociatedIds) identifiable, true);
    }
  }

  private String getAssociatedIdsType(String idType) {
    String associatedIdsType = null;
    IDGenerator<?> idGenerator = IDGeneratorFactory.instance.getIdGenerator(idType);
    if (idGenerator == null) {
      Pair<String, IDGenerator<?>> idGeneratorWithType = IDGeneratorFactory.instance
          .getAssociatedIdGeneratorWithType(idType);
      if (idGeneratorWithType != null) {
        idGenerator = idGeneratorWithType.getValue();
        associatedIdsType = idGeneratorWithType.getKey();
      }
    }
    if (idGenerator == null || idGenerator.isExternal()) {
      throw new NotFoundException("resource /ids/" + idType + " not exist");
    }
    return associatedIdsType;
  }


  private Token readToken(String tokenId) {
    return Optional.ofNullable(Servers.instance.getTokenByTid(tokenId))
        .orElseThrow(() -> {
          logger.error("No token with id {} found", tokenId);
          return new InvalidTokenException("Please supply a valid 'createIds' token.",
              Status.UNAUTHORIZED);
        });
  }
}
