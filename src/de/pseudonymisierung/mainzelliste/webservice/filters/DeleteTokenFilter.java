package de.pseudonymisierung.mainzelliste.webservice.filters;

import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.webservice.Token;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DeleteTokenFilter implements ContainerResponseFilter {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public void filter(ContainerRequestContext requestContext,
        ContainerResponseContext responseContext) throws IOException {
        // Tokens are only used in patient resource
        String absolutePath = requestContext.getUriInfo().getAbsolutePath().toString();
        if(absolutePath.contains("/patients") || absolutePath.contains("/ids")){
            if(100 <= responseContext.getStatus() && responseContext.getStatus() < 400){
                String tokenId = getTokenId(requestContext);
                logger.info("Checking if token with id {} can be be deleted ...", tokenId);
                Token token = Servers.instance.getTokenByTid(tokenId);
                if(token != null){
                    int remainingUses = token.decreaseRemainingUses();
                    if(remainingUses > 0){
                        logger.info("Token with id {} should not be deleted. Remaining uses are: {}", token.getId(), remainingUses);
                    } else {
                        logger.info("Deleting Token with id {} from Mainzelliste", token.getId());
                        Servers.instance.deleteToken(token.getId());
                    }
                }
            }
        }
    }

    private String getTokenId(ContainerRequestContext requestContext) {
        String requestPath = requestContext.getUriInfo().getAbsolutePath().toString();
        switch (requestContext.getMethod().toUpperCase()){
            case "GET":
            case "PUT":
                if(requestPath.contains("/tokenId/")) {
                    return requestPath.split("/tokenId/")[1];
                } else {
                    return requestContext.getUriInfo().getQueryParameters().getFirst("tokenId");
                }
            case "POST":
                if(requestPath.contains("checkMatch"))
                    return requestPath.split("checkMatch/")[1];
                else
                    return requestContext.getUriInfo().getQueryParameters().getFirst("tokenId");
            case "DELETE":
                String[] splitedRequestPath = requestPath.split("/");
                return splitedRequestPath[splitedRequestPath.length - 3];
            default:
                return "";
        }
    }
}
