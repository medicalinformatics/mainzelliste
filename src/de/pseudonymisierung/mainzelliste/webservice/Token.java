/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.webservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.IDGeneratorFactory;
import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.Servers.ApiVersion;
import de.pseudonymisierung.mainzelliste.Session;
import de.pseudonymisierung.mainzelliste.dto.Persistor;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidTokenException;
import de.pseudonymisierung.mainzelliste.webservice.commons.MainzellisteCallbackUtil;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.codehaus.jettison.json.JSONObject;
import org.glassfish.jersey.uri.UriTemplate;


/**
 * A temporary "ticket" to realize authorization and/or access to a resource.
 * Tokens are used by providing their token id (e.g. GET
 * /patients/tokenId/{tid}), but also connected to a {@link Session} (e.g.
 * DELETE /sessions/{sid}). Thus, they are created using a session.
 */
public class Token {

	public static final String TYPE_CHECK_MATCH = "checkMatch";

	/** A unique identifier. */
	private String id;
	/** URI of the token. */
	private URI uri = null;
	/** Type, such as "addPatient", "readPatient" etc. */
	private String type;
	/**
	 * Data depending on the token type, such as: The ID to create, the patient
	 * to edit, ...
	 */
	private Map<String, ?> data;

	private String parentSessionId;

	private String parentServerName;

	/** This is the amount of uses set at the creation of a Token. Defaults to 1 */
	private int allowedUses;

	/** The remaining amount of uses allowed for the token */
	private int remainingUses;


	/**
	 * Create emtpy instance. Used internally only.
	 */
	Token() {
	    this.id = UUID.randomUUID().toString();
	    this.allowedUses = 1;
	    this.remainingUses = 1;
	}

	/**
	 * Create token with the type. Initializes empty container for
	 * token data. Performs no checking if the provided token type is known.
	 * The token is allowed to be used one time.
	 * @param type
	 *            The token type.
	 */
	public Token(String type) {
	    this(type, 1);
	}

	/**
	 * Create Token with given type and amount of uses. Initializes empty container for
	 * token data. Performs no checking if the provided token type is known.
	 * @param type
	 * 				The token type
	 * @param allowedUses
	 * 				The amount of uses allowed for this Token.
	 * 			 	This defaults to 1 and it is not possible to set lower than 1
	 */
	public Token(String type, int allowedUses){
		this.id = UUID.randomUUID().toString();
		this.type = type;
		this.data = new HashMap<>();
		this.allowedUses = Math.max(allowedUses, 1);
		this.remainingUses = Math.max(allowedUses, 1);
	}

	/**
	 * Check if a token is valid, i.e. it has a known type and the data items
	 * for the specific type have the correct format.
	 *
	 * @param apiVersion
	 *            API version to use for the check.
	 *
	 * @throws InvalidTokenException
	 *             if the format is incorrect. A specific error message is
	 *             returned to the client with status code 400 (bad request).
	 */
	public void checkValidity(ApiVersion apiVersion)
			throws InvalidTokenException {
		if (this.type.equals("addPatient"))
			this.checkAddPatient(apiVersion);
		else if (this.type.equals("editPatient"))
			this.checkEditPatient(apiVersion);
		else if (this.type.equals("deletePatient")) {
			if (apiVersion.majorVersion >= 3 && apiVersion.minorVersion > 2) {
				this.checkDeletePatient();
			}
		}
		else if (this.type.equals("checkMatch"))
			this.checkCheckMatch();
		else
			throw new InvalidTokenException("Token type " + this.type
					+ " unknown!");
		if (Config.instance.auditTrailIsOn()) {
			this.checkAuditTrail();
		}
	}

	/**
	 * Check if this token has the expected type.
	 *
	 * @param expected
	 *            The expected token type.
	 * @throws InvalidTokenException
	 *             If this token's type does not match the expected type.
	 */
	public void checkTokenType(String expected) {
		if (expected == null || !expected.equals(this.getType()))
			throw new InvalidTokenException("Invalid token type: Expected: "
					+ expected + ", actual: " + this.getType(), Status.UNAUTHORIZED);
	}

	/**
	 * Get the unique id of this token.
	 *
	 * @return The token id.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the unique id of this token. Performs no check of uniqueness.
	 *
	 * @param id
	 *            The new token id.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Get the URI of this token.
	 *
	 * @return The token URI.
	 */
	public URI getURI() {
		return this.uri;
	}

	/**
	 * Set the URI of this token.
	 *
	 * @param uri
	 *            The new token URI.
	 */
	public void setURI(URI uri) {
		this.uri = uri;
	}

	/**
	 * Get the type of this token, e.g. "addPatient", "editPatient" etc.
	 *
	 * @return The token type.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Get the data container of this token.
	 *
	 * @return A map where keys are the names of data items and values the data
	 *         items. Data items can be map or collection types again.
	 */
	public Map<String, ?> getData() {
		return data;
	}

	/**
	 * Get a particular data element by its key. This method is preferable to
	 * getData().get() as it handles the case data==null safely.
	 *
	 * @param item
	 *            The name of the data item to get.
	 * @return The requested data item. Null if no such item exists or if no
	 *         data is attached to the token (data==null).
	 */
	public String getDataItemString(String item) {
		if (this.data == null)
			return null;
		else
			return (String) data.get(item);
	}

	/**
	 * Get a particular data element by its key. Assumes that the requested data
	 * item is a list.
	 *
	 * @param item
	 *            The name of the data item to get.
	 * @return The requested data item. Null if no such item exists or if no
	 *         data is attached to the token (data==null).
	 * @throws ClassCastException
	 *             If the requested data item exists but is not a list.
	 */
	public List<?> getDataItemList(String item) throws ClassCastException {
		if (this.data == null)
			return null;
		else
			return (List<?>) data.get(item);
	}

	protected List<String> getDataItemAsStringList(String itemName) {
		try {
			List<String> itemStringList = (List<String>) this.getDataItemList(itemName);
			return itemStringList == null ? new ArrayList<>() : itemStringList;
		} catch (ClassCastException e) {
			throw new InvalidTokenException("Illegal format for data item '" + itemName + "'! Please "
					+ "provide a list of field names.");
		}
	}

	protected boolean getDataItemAsBoolean(String itemName) {
		return Boolean.TRUE.equals(data.get(itemName));
	}

	/**
	 * Check whether the token has the given data item.
	 *
	 * @param item
	 *            The name of the data item to check.
	 * @return true if a data item with the given name exists.
	 */
	public boolean hasDataItem(String item) {
		return this.data != null && this.data.containsKey(item);
	}

	/**
	 * Get a particular data element by its key. Assumes that the requested data
	 * item is a map.
	 *
	 * @param item
	 *            The name of the data item to get.
	 * @return The requested data item. Null if no such item exists or if no
	 *         data is attached to the token (data==null).
	 * @throws ClassCastException
	 *             If the requested data item exists but is not a map.
	 */
	@SuppressWarnings("unchecked")
	public Map<String, ?> getDataItemMap(String item) throws ClassCastException {
		if (this.data == null)
			return null;
		else
			return (Map<String, Object>) data.get(item);
	}

	/**
	 * Set the data container to the provided map.
	 *
	 * @param data
	 *            The new data container, copied by reference.
	 */
	public void setData(Map<String, ?> data) {
		this.data = data;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Token))
			return false;

		Token t2 = (Token) obj;
		return t2.id.equals(this.id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	/**
	 * Check whether this is a valid addPatient token.
	 *
	 * @param apiVersion
	 *            The API version to use.
	 */
	@SuppressWarnings("unchecked")
	private void checkAddPatient(ApiVersion apiVersion) {
		// check requested id types
		if (this.hasDataItem("idTypes")) {
			this.checkIdTypes((List<String>) this.getDataItemList("idTypes"));
		} else if (this.hasDataItem("idtypes")) {
			this.checkIdTypes((List<String>) this.getDataItemList("idtypes"));
		} else if (this.hasDataItem("idtype")) {
			LinkedList<String> idTypes = new LinkedList<String>();
			String requestedIdType = this.getDataItemString("idtype");
			// If id type is not specified in api version 1.0, the default id
			// type is used
			if (requestedIdType != null) {
				idTypes.add(requestedIdType);
				this.checkIdTypes(idTypes);
			}
		}

		// Check callback URL
		String callback = this.getDataItemString("callback");
		if (callback != null && !callback.equals("") && Servers.instance.hasPermissionByName(getParentServerName(), "callback"))
			MainzellisteCallbackUtil.checkCallbackUrl(callback);
		String redirect = this.getDataItemString("redirect");
		if (redirect != null && !redirect.equals("") && Servers.instance.hasPermissionByName(getParentServerName(), "redirect")){
			// @Florian paste code here
		}

		// Check redirect URL
		if (this.hasDataItem("redirect")) {
			UriTemplate redirectURITempl;
			try {
				redirectURITempl = new UriTemplate(
						this.getDataItemString("redirect"));
			} catch (Throwable t) {
				throw new InvalidTokenException(
						"The URI template for the redirect address seems to be malformed: "
								+ this.getDataItemString("redirect"));
			}

			List<String> definedIdTypes = Arrays
					.asList(IDGeneratorFactory.instance.getIDTypes());
			for (String templateVar : redirectURITempl.getTemplateVariables()) {
				if (!templateVar.equals("tokenId")
						&& !definedIdTypes.contains(templateVar))
					throw new InvalidTokenException(
							"The URI template for the redirect address contains the undefined id type "
									+ templateVar + ".");
			}
		}

		// Check provided field values
		if (this.getData().containsKey("fields")) {
			try {
				Map<String, ?> fields = this.getDataItemMap("fields");
				for (String thisField : fields.keySet()) {
					if (!Config.instance.getFieldKeys().contains(thisField))
						throw new InvalidTokenException("Field " + thisField
								+ " provided in field list is unknown!");
				}
			} catch (ClassCastException e) {
				throw new InvalidTokenException(
						"Illegal format for data item 'fields'! "
								+ "Please provide a list of fields as key-value pairs.");
			}
		}
	}

	/**
	 * Check whether this is a valid editPatient token.
	 * Incompatible change in version 3:
	 * It is not possible anymore to use the token without fields in order to edit all fields
	 * Edit token without ids and fields throws an exception
	 */
	private void checkEditPatient(ApiVersion apiVersion) {
        // if API version < 3 and there are no fields in token, all fields can be edited
        // in this case all fields from configuration are added to token
        if (!this.getData().containsKey("fields") && apiVersion.majorVersion < 3) {
            Map<String, Object> dataWithAllFields = (Map<String, Object>)this.getData();

			ArrayList<String> fieldKeys = new ArrayList<String>();
            for (String fieldKey : Config.instance.getFieldKeys()) {
                fieldKeys.add(fieldKey);
            }

            dataWithAllFields.put("fields", fieldKeys);
            this.setData(dataWithAllFields);
        }

        // if there are no fields and ids in token, throw an exception
		if (!this.getData().containsKey("ids") && !this.getData().containsKey("fields")) {
			throw new InvalidTokenException("Token must contain at least one field or id to edit");
		}
	}

	/**
	 * Check whether this is a valid deletePatient token.
	 * Delete token without patient id throws an exception
	 * Delete token with not existent patient id throws an exception
	 */
	private void checkDeletePatient() {
		// if there are no patient id in token, throw an exception
		if (!this.getData().containsKey("patientId")) {
			throw new InvalidTokenException("Token must contain a patient id to delete");
		}

		Map<String,String> patientId = (Map<String, String>) this.getDataItemMap("patientId");
		String idType = patientId.get("idType");
		String idString = patientId.get("idString");

		ID id = IDGeneratorFactory.instance.buildId(idType, idString);
		if(!Persistor.instance.patientExists(id)) {
			throw new InvalidTokenException(
					"No patient found with provided " + id.getType() + " '"
							+ id.getIdString() + "'!");
		}

		return;
	}

	/**
	 * Check whether this is a valid checkMatch token.
	 * CheckMatch token without idtypes throws an exception
	 */
	private void checkCheckMatch() {
		Set<String> idTypes = readIdTypesFromJsonArray("idTypes");
		if (idTypes == null || idTypes.isEmpty()) {
			throw new InvalidTokenException("No idtypes defined! Token must contain at least one idtype for best match patient.");
		}

		// read and check result id types
		String jsonArrayName = "resultIds";
		Set<String> resultIds = readIdTypesFromJsonArray(jsonArrayName);
		checkResultIdTypes(resultIds, idTypes, jsonArrayName);

		// read and check callback result id types
		jsonArrayName = "callbackResultIds";
		Set<String> callbackResultIds = readIdTypesFromJsonArray(jsonArrayName);
		checkResultIdTypes(callbackResultIds, idTypes, jsonArrayName);
	}

	/**
	 * Check that the provided list contains only valid id types.
	 *
	 * @param listIdTypes
	 *            The list of ID types to check.
	 * @throws InvalidTokenException
	 *             if the check fails.
	 */
	protected void checkIdTypes(Collection<String> listIdTypes)
			throws InvalidTokenException {
		// if no idTypes are defined, there is nothing to check
		if (listIdTypes == null)
			return;

		try {
			// Get defined ID types
			// Check for every type supplied in the token if it is in the list
			// of defined types
			for (Object thisIdType : listIdTypes) {
				String thisIdTString = (String) thisIdType;
				checkIdType(thisIdTString);
			}
		} catch (ClassCastException e) {
			// If one of the casts went wrong, the format of the input JSON data
			// was probably incorrect.
			throw new InvalidTokenException(
					"Illegal format for data item 'idtypes': Must be array of Strings.");
		}
	}

	/**
	 * Check if an ID with the given type is configured.
	 *
	 * @param idType
	 *            The name of the ID type to check.
	 * @throws InvalidTokenException
	 *             If the given ID type is not known.
	 */
	protected void checkIdType(String idType) {
		if (!IDGeneratorFactory.instance.isIdTypeConfigured(idType))
			throw new InvalidTokenException("'" + idType + "'"
					+ " is not a known ID type!");
	}

	/**
	 * Get this token as JSON.
	 *
	 * @param apiVersion
	 *            The API version to use.
	 * @return A JSON representation of the token.
	 */
	public JSONObject toJSON(ApiVersion apiVersion) {
		JSONObject ret = new JSONObject();
		try {
			if (apiVersion.majorVersion >= 2) {
				ret.put("id", this.id).put("type", this.type).put("allowedUses", this.allowedUses).put("remainingUses", this.remainingUses);
				ObjectMapper mapper = new ObjectMapper();
				String dataString = mapper.writeValueAsString(data);
				ret.put("data", new JSONObject(dataString));
			} else {
				ret.put("tokenId", this.id);
			}
			ret.put("uri", this.getURI().toString());
			return ret;
		} catch (Exception e) {
			// As no external data is processed, this method should not fail.
			// If it does, this indicates a bug
			throw new Error(e);
		}
	}

	/**
	 * Reduces the remaining amount of uses for this {@link Token} by 1.
	 * @return
	 * 			the remaining amount of uses for this token
	 */
	public int decreaseRemainingUses(){
		remainingUses = getRemainingUses() - 1;
		return remainingUses;
	}

	public int getAllowedUses() {
		return allowedUses;
	}

	public int getRemainingUses() {
		return remainingUses;
	}

	public String getParentSessionId() {
        return parentSessionId;
    }

    public void setParentSessionId(String parentSessionId) {
        this.parentSessionId = parentSessionId;
    }

    public String getParentServerName() {
        return parentServerName;
    }

    public void setParentServerName(String parentServerName) {
        this.parentServerName = parentServerName;
    }

	protected void checkAuditTrail() {
		if (!this.getData().containsKey("auditTrail"))
			throw new InvalidTokenException("Invalid Token object, audtiTrail key is not specified", Response.Status.BAD_REQUEST);

		// check format
		Map<String,?> auditTrail;
		try {
			auditTrail = this.getDataItemMap("auditTrail");
		} catch (ClassCastException e) {
			throw new InvalidTokenException(
					"Field 'auditTrail' has wrong format. Expected map of key/value pairs, received: "
							+ this.getData().get("auditTrail"));
		}

		// check keys
		if (   auditTrail == null ||
				!auditTrail.keySet().contains("username") ||
				!auditTrail.keySet().contains("remoteSystem") ||
				!auditTrail.keySet().contains("reasonForChange"))
			throw new InvalidTokenException(
					"Field 'auditTrail' has wrong format. One or more keys are missing, received: "
							+ this.getData().get("auditTrail"));

		// check for null or missing values
		if (    auditTrail.values().contains(null) || auditTrail.values().contains(""))
			throw new InvalidTokenException(
					"Field 'auditTrail' has wrong format. One or more values are missing, received: "
							+ this.getData().get("auditTrail"));
	}

	public Set<String> readIdTypesFromJsonArray(String jsonArrayName) {
		try {
			List<String> idTypes = (List<String>) this.getDataItemList(jsonArrayName);
			if(idTypes != null) {
				return new HashSet<>(idTypes);
			} else {
				return null;
			}
		} catch (ClassCastException e) {
			throw new InvalidTokenException("Field '" + jsonArrayName + "' has wrong format.");
		}
	}

	protected void checkResultIdTypes(Set<String> resultIdTypes, Set<String> idTypes, String jsonArrayName) {
		if(resultIdTypes == null) {
			return;
		}
		for (String resultIdType : resultIdTypes) {
			if (!idTypes.contains(resultIdType)) {
				throw new InvalidTokenException("id type '" + resultIdType
						+ "' from '" + jsonArrayName + "' must be defined in the given 'idTypes' array.");
			} else if (!IDGeneratorFactory.instance.isIdTypeConfigured(resultIdType)) {
				throw new InvalidTokenException("Unknown id type '" + resultIdType);
			}
		}
	}
}
