/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.webservice;

import de.pseudonymisierung.mainzelliste.IDGeneratorFactory;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

public class RequestedIdTypes {

  /**
   * The ID types that should be returned when making the ID request.
   */
  private Set<String> allIdTypes = new LinkedHashSet<>();
  private final Set<String> idTypes = new HashSet<>();
  private final Set<String> transientIdTypes = new HashSet<>();
  private final MultiValuedMap<String, String> associatedIdTypesMap = new ArrayListValuedHashMap<>();


  public RequestedIdTypes() {
  }

  public RequestedIdTypes(Set<String> allIdTypes) {
    this.allIdTypes = allIdTypes;
    // use the default ID type
    if (allIdTypes.isEmpty()) {
      allIdTypes.add(IDGeneratorFactory.instance.getDefaultIDType());
    }
    for (String requestedIdType : allIdTypes) {
      if (IDGeneratorFactory.instance.getTransientIdTypes().contains(requestedIdType)) {
        transientIdTypes.add(requestedIdType);
      } else if (IDGeneratorFactory.instance.isIdTypeExist(requestedIdType)) {
        idTypes.add(requestedIdType);
      } else if (IDGeneratorFactory.instance.isAssociatedIdTypeExist(requestedIdType)) {
        associatedIdTypesMap.put(IDGeneratorFactory.instance.getAssociatedIdsType(
            requestedIdType), requestedIdType.trim());
      }
    }
  }

  public Set<String> getAllIdTypes() {
    return allIdTypes;
  }

  public Set<String> getIdTypes() {
    return idTypes;
  }

  public Set<String> getTransientIdTypes() {
    return transientIdTypes;
  }

  public MultiValuedMap<String, String> getAssociatedIdTypesMap() {
    return associatedIdTypesMap;
  }
}
