/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.pseudonymisierung.mainzelliste.webservice;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.DataFormatException;
import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.IDGeneratorFactory;
import de.pseudonymisierung.mainzelliste.IDRequest;
import de.pseudonymisierung.mainzelliste.Patient;
import de.pseudonymisierung.mainzelliste.PatientBackend;
import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.Session;
import de.pseudonymisierung.mainzelliste.dto.Persistor;
import de.pseudonymisierung.mainzelliste.exceptions.FHIRProcessingException;
import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import de.pseudonymisierung.mainzelliste.util.FHIRReader;
import de.pseudonymisierung.mainzelliste.util.FHIRWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Bundle.BundleEntrySearchComponent;
import org.hl7.fhir.r4.model.OperationOutcome.IssueSeverity;
import org.hl7.fhir.r4.model.OperationOutcome.IssueType;

@Path("/fhir/Patient")
public class FHIRPatientResource {

  /** Fhir Context (needed for parsing and serializing) */
  private FhirContext ctx = FhirContext.forR4();

  /** The logging instance. */
  private final Logger logger = LogManager.getLogger(FHIRPatientResource.class);

  /**
   * Performs the [read operation](http://hl7.org/fhir/http.html#read) of FHIR for Patient
   * Resources.
   *
   * <p>Internally, the supplied idString as a path is the ID of the default IDGenerator which is
   * retrieved from the database.
   */
  @GET
  @Path("{idString}")
  @Produces(
      "application/fhir+json,application/json+fhir,application/json,application/fhir+xml,application/xml+fhir,application/xml")
  public Response readPatient(
      @Context HttpServletRequest req,
      @Context UriInfo uriInfoContext,
      @PathParam("idString") String idString) {

    // Check if fhir is enabled in the Configuration
    if (!Boolean.parseBoolean(Config.instance.getProperty("experimental.fhir.enable"))) {
      return Response.status(404).build();
    }

    // TODO: define api key authorization for fhir
    Servers.instance.checkPermission(req, "fhir_read");

    // TODO: audit trail relevant (?) information here
    logger.info(
        "GET FHIR Patient Resource read: "
            + idString
            + " (API Key: "
            + req.getHeader("mainzellisteApiKey")
            + ", Server: "
            + req.getRemoteHost()
            + ")");

    FHIRWriter writer = new FHIRWriter(this.ctx);
    writer.setParser(req.getHeader("Accept"), uriInfoContext.getQueryParameters());

    Patient p = null;
    try {
      p = this.getPatientByIdString(idString, req);
    } catch (FHIRProcessingException fpe) {
      return fpe.getResponse(writer);
    }

    if (p == null) {
      // No patient: return 404
      return writer.genericErrorResponse(
          404,
          IssueSeverity.ERROR,
          IssueType.NOTFOUND,
          "MSG_NO_EXIST",
          "Resource Id " + idString + " does not exist");
    }

    // Create the parser for responding with the correct FHIR Resource
    writer.putResource(p.toFHIR());
    writer.setElementFilter(uriInfoContext.getQueryParameters());

    return Response.status(200)
        .entity(writer.encodeToString())
        .location(writer.getLocationUri(uriInfoContext))
        .build();
  }

  /**
   * Performs a [update operation](http://hl7.org/fhir/http.html#update) for Patient FHIR Resources.
   *
   * <p>The update operation will be based on the PathParameters (aka defaultId), which will be used
   * to retrieve the patient from the database and update it.
   */
  @PUT
  @Path("{idString}")
  @Consumes(
      "application/fhir+json,application/json+fhir,application/json,application/fhir+xml,application/xml+fhir,application/xml")
  @Produces(
      "application/fhir+json,application/json+fhir,application/json,application/fhir+xml,application/xml+fhir,application/xml")
  public Response updatePatient(
      @Context HttpServletRequest req,
      @Context UriInfo uriInfoContext,
      @PathParam("idString") String idString,
      String body) {

    // Check if fhir is enabled in the Configuration
    if (!Boolean.parseBoolean(Config.instance.getProperty("experimental.fhir.enable"))) {
      return Response.status(404).build();
    }

    // TODO: define api key authorization for fhir
    Servers.instance.checkPermission(req, "fhir_update");

    // TODO: audit trail relevant (?) information here
    logger.info(
        "PUT FHIR Patient Resource update: "
            + idString
            + " (API Key: "
            + req.getHeader("mainzellisteApiKey")
            + ", Server: "
            + req.getRemoteHost()
            + ")");

    // create a writer from the query parameters and the HTTP header
    FHIRWriter writer = new FHIRWriter(this.ctx);
    writer.setParser(req.getHeader("Accept"), uriInfoContext.getQueryParameters());

    // Parse the FHIR Resource to a HAPI Patient instance
    FHIRReader<org.hl7.fhir.r4.model.Patient> patientReader = new FHIRReader<>(this.ctx);
    try {
      patientReader.parseResource(
          req.getHeader("Content-Type"), body, org.hl7.fhir.r4.model.Patient.class);
    } catch (DataFormatException dfe) {
      return writer.genericErrorResponse(
          400,
          IssueSeverity.ERROR,
          IssueType.STRUCTURE,
          "MSG_CANT_PARSE_CONTENT",
          "Unable to parse feed (entry content type = " + dfe.getMessage() + ")");
    }

    // retrieve the patient by idString and update it using the reader
    Patient p = this.getPatientByIdString(idString, req);
    try {
      p = this.updatePatientByReader(p, patientReader, req);
    } catch (FHIRProcessingException fpe) {
      return fpe.getResponse(writer);
    }

    // create a parser and return the patient
    writer.putResource(p.toFHIR());
    return Response.status(200)
        .entity(writer.encodeToString())
        .location(writer.getLocationUri(uriInfoContext))
        .build();
  }

  /**
   * Performs a [delete operation](http://hl7.org/fhir/http.html#delete) for Patient FHIR Resources.
   *
   * <p>Currently not implemented.
   */
  @DELETE
  @Path("{idString}")
  public Response deletePatient(
      @Context HttpServletRequest req,
      @Context UriInfo uriInfoContext,
      @PathParam("idString") String idString) {

    // Check if fhir is enabled in the Configuration
    if (!Boolean.parseBoolean(Config.instance.getProperty("experimental.fhir.enable"))) {
      return Response.status(404).build();
    }

    // TODO: define api key authorization for fhir
    Servers.instance.checkPermission(req, "fhir_delete");

    FHIRWriter writer = new FHIRWriter(this.ctx);
    return writer.genericErrorResponse(
        405,
        IssueSeverity.FATAL,
        IssueType.BUSINESSRULE,
        "MSG_OP_NOT_ALLOWED",
        "Operation DELETE not allowed from resource Patient");
  }

  /**
   * Performs a [create operation](http://hl7.org/fhir/http.html#create) for Patient FHIR Resources.
   *
   * <p>This method is responsible to parse the incoming HTTP request body to a FHIR Patient
   * Resource, use the FHIRPath mapping from the Configuration to extract all needed information
   * from the resource and supply a map of resources to the PatientBackend, which will create, match
   * and persist the Patient data.
   */
  @POST
  @Consumes(
      "application/fhir+json,application/json+fhir,application/json,application/fhir+xml,application/xml+fhir,application/xml")
  @Produces(
      "application/fhir+json,application/json+fhir,application/json,application/fhir+xml,application/xml+fhir,application/xml")
  public Response createPatient(
      @Context HttpServletRequest req, @Context UriInfo uriInfoContext, String body) {

    // Check if fhir is enabled in the Configuration
    if (!Boolean.parseBoolean(Config.instance.getProperty("experimental.fhir.enable"))) {
      return Response.status(404).build();
    }

    // TODO: define api key authorization for fhir
    Servers.instance.checkPermission(req, "fhir_create");

    // TODO: audit trail relevant (?) information here
    logger.info(
        "POST FHIR Patient Resource create. (API Key: "
            + req.getHeader("mainzellisteApiKey")
            + ", Server: "
            + req.getRemoteHost()
            + ")");

    // Parse the FHIR Resource to a HAPI Patient instance
    FHIRWriter writer = new FHIRWriter(this.ctx);
    writer.setParser(req.getHeader("Accept"), uriInfoContext.getQueryParameters());

    // Parse the FHIR Resource to a HAPI Patient instance
    FHIRReader<org.hl7.fhir.r4.model.Patient> patientReader = new FHIRReader<>(this.ctx);
    try {
      patientReader.parseResource(
          req.getHeader("Content-Type"), body, org.hl7.fhir.r4.model.Patient.class);
    } catch (DataFormatException dfe) {
      return writer.genericErrorResponse(
          400,
          IssueSeverity.ERROR,
          IssueType.STRUCTURE,
          "MSG_CANT_PARSE_CONTENT",
          "Unable to parse feed (entry content type = " + dfe.getMessage() + ")");
    }

    // create the patient using the read patient instance (i.e. the patientReader)
    Patient createdPatient;
    try {
      createdPatient = this.createPatientByReader(patientReader, req, uriInfoContext);
    } catch (FHIRProcessingException fpe) {
      return fpe.getResponse(writer);
    }

    // put the patient into the writer
    writer.putResource(createdPatient.toFHIR());

    return Response.status(201)
        .entity(writer.encodeToString())
        .location(writer.getLocationUri(uriInfoContext))
        .build();
  }

  /**
   * Performs a [search operation](http://hl7.org/fhir/http.html#search) for Patient FHIR Resources.
   *
   * <p>This method extract a search formular from the query parameters and supplies it to
   * searchPatientForm, which shall be equivalent according to the FHIR specification.
   */
  @GET
  @Produces(
      "application/fhir+json,application/json+fhir,application/json,application/fhir+xml,application/xml+fhir,application/xml")
  public Response searchPatient(@Context HttpServletRequest req, @Context UriInfo uriInfoContext) {

    // Check if fhir is enabled in the Configuration
    if (!Boolean.parseBoolean(Config.instance.getProperty("experimental.fhir.enable"))) {
      return Response.status(404).build();
    }

    // TODO: define api key authorization for fhir
    Servers.instance.checkPermission(req, "fhir_search");

    return this.searchPatientForm(req, uriInfoContext, uriInfoContext.getQueryParameters());
  }

  /**
   * Performs a [search operation](http://hl7.org/fhir/http.html#search) for Patient FHIR Resources.
   *
   * <p>This method performs a search operation for Identifiers. Any other search parameter will be
   * ignored. The result will be replied using a searchset-Bundle.
   */
  @POST
  @Path("_search")
  @Consumes("application/x-www-form-urlencoded")
  @Produces(
      "application/fhir+json,application/json+fhir,application/json,application/fhir+xml,application/xml+fhir,application/xml")
  public Response searchPatientForm(
      @Context HttpServletRequest req,
      @Context UriInfo uriInfoContext,
      MultivaluedMap<String, String> form) {

    // Check if fhir is enabled in the Configuration
    if (!Boolean.parseBoolean(Config.instance.getProperty("experimental.fhir.enable"))) {
      return Response.status(404).build();
    }

    // TODO: define api key authorization for fhir
    Servers.instance.checkPermission(req, "fhir_search");

    List<String> identifiers = form.get("identifier");
    FHIRWriter writer = new FHIRWriter(this.ctx);
    writer.setParser(req.getHeader("Accept"), uriInfoContext.getQueryParameters());

    // TODO: audit trail relevant (?) information here
    logger.info(
        "GET/POST FHIR Patient Resource identifier search: "
            + String.join(",", identifiers)
            + " (API Key: "
            + req.getHeader("mainzellisteApiKey")
            + ", Server: "
            + req.getRemoteHost()
            + ")");

    List<Patient> patients;
    try {
      patients = this.searchPatientsByIdentifiers(identifiers, req);
    } catch (FHIRProcessingException fpe) {
      return fpe.getResponse(writer);
    }

    // Create the response bundle and add all search results to it
    Bundle searchSetBundle = new Bundle();
    searchSetBundle.setType(Bundle.BundleType.SEARCHSET);
    for (Patient patient : patients) {
      // Add the Resource to a Entry Component
      BundleEntryComponent bec = new BundleEntryComponent();
      bec.setResource(patient.toFHIR());

      BundleEntrySearchComponent besc = new BundleEntrySearchComponent();
      besc.setMode(Bundle.SearchEntryMode.MATCH);
      bec.setSearch(besc);
      searchSetBundle.addEntry(bec);
    }

    writer.putBundle(searchSetBundle);
    writer.setElementFilter(uriInfoContext.getQueryParameters());

    return Response.status(200).entity(writer.encodeToString()).build();
  }

  /**
   * Searches a list of Patients by a List of Identifiers in the FHIR notation:
   * "<idType>|<idString>"
   *
   * @param identifiers list of Strings that represent '|' delimited identifiers
   * @param req request instance, needed to create URIs for example
   * @return A List of Patient instances that were found by the List of identifiers
   * @throws FHIRProcessingException Any processing error throws a FHIRProcessingException
   */
  public List<Patient> searchPatientsByIdentifiers(List<String> identifiers, HttpServletRequest req)
      throws FHIRProcessingException {
    // region copypasteFromPatientsResource
    /*
     * TODO: The following is taken from PatientsResource.getPatients(). I guess it
     * would be preferable to refactor these parts to a callable function instead of
     * copying them.
     */
    List<Patient> patients = new ArrayList<>();
    List<ID> requestIds = new ArrayList<>();
    for (String identifier : identifiers) {
      if (!identifier.contains("|")) {
        throw new FHIRProcessingException(
            400,
            IssueSeverity.ERROR,
            IssueType.VALUE,
            "MSG_INVALID_ID",
            "Id not accepted: " + identifier);
      }
      String idType = identifier.split("\\|")[0];
      String idString = identifier.split("\\|")[1];
      requestIds.add(IDGeneratorFactory.instance.buildId(idType, idString));
    }

    try {
      for(ID requestedId : requestIds) {
        patients.add(Persistor.instance.getPatient(requestedId));
      }
    } catch (InternalErrorException e) {
      logger.error("Unable to read retrieved Patients internally: {}", e.getMessage());
      throw new FHIRProcessingException(
          500,
          IssueSeverity.FATAL,
          IssueType.EXCEPTION,
          "Occured an internal bug, could not read the internal Patient response. Please report"
              + " this to www.mainzelliste.de!");
    }
    return patients;
  }

  /** Shared utils for these API methods, which can be re-used for the Bundle API. */

  /**
   * Retrieves a Patient by idString
   *
   * @param idString String representation of the FHIR Patient Resource (aka idString of the
   *     DefaultIDGenerator)
   * @param req request instance, needed to create URIs for example
   * @return The Patient instance found using idString
   * @throws FHIRProcessingException Any FHIR processing error will be replied back via this
   *     exception,
   */
  public Patient getPatientByIdString(String idString, HttpServletRequest req)
      throws FHIRProcessingException {
    // retrieve patient with specified idString
    String mainIdType = IDGeneratorFactory.instance.getDefaultIDType();
    if (!IDGeneratorFactory.instance.getFactory(mainIdType).verify(idString)) {
      throw new FHIRProcessingException(
          400, IssueSeverity.ERROR, IssueType.STRUCTURE, "MSG_INVALID_ID", "Id not accepted");
    }
    ID patientId = IDGeneratorFactory.instance.buildId(mainIdType, idString);
    try {
      return Persistor.instance.getPatient(patientId);
    } catch (InternalErrorException e) {
      logger.error("Unable to read retrieved Patients internally: {}", e.getMessage());
      throw new FHIRProcessingException(
          500,
          IssueSeverity.FATAL,
          IssueType.EXCEPTION,
          "Occured an internal bug, could not read the internal Patient response. Please report"
              + " this to www.mainzelliste.de!");
    }
  }

  /**
   * Internal update method to update `patient` with the values from `patientReader`. The idea of
   * this method is to supply parsed HAPI Patient instances and return the updated patient.
   *
   * @param patient Patient instance of the patient that shall be updated
   * @param patientReader FHIRReader instance to make it easier to retrieve all data
   * @param req The HttpServletRequest, needed to set URIs and so on
   * @return An updated Patient instance
   * @throws FHIRProcessingException any FHIR Processing error will be returned back
   */
  public Patient updatePatientByReader(
      Patient patient,
      FHIRReader<org.hl7.fhir.r4.model.Patient> patientReader,
      HttpServletRequest req)
      throws FHIRProcessingException {

    // retrieve patient with specified idString
    String mainIdType = IDGeneratorFactory.instance.getDefaultIDType();
    ID patientId = patient.getId(mainIdType);

    // check if id from path and id from the FHIR Resource match
    if (!patientId
        .getIdString()
        .equals(patientReader.getResource().getId().substring("Patient/".length()))) {
      throw new FHIRProcessingException(
          400,
          IssueSeverity.ERROR,
          IssueType.VALUE,
          "MSG_RESOURCE_ID_MISMATCH",
          "Resource Id Mismatch");
    }

    // retrieve supplied fields from the FHIR Resource
    Map<String, String> suppliedFields = patientReader.readPatientInputFields();
    // retrieve supplied ids from the FHIR Resource and filter for external IDs only
    List<ID> ids =
        patientReader.readIds().stream()
            .filter(id -> id.getFactory().isExternal())
            .collect(Collectors.toList());

    // Create the mocked edit token
    Token editToken;
    try {
      editToken =
          createMockEditPatientToken(
              req,
              patientId,
              new ArrayList<String>(suppliedFields.keySet()),
              ids.stream().map(id -> id.getType()).collect(Collectors.toList()));
    } catch (JSONException je) {
      logger.error("Unable to build Token JSON: " + je.getMessage());
      throw new FHIRProcessingException(
          500,
          IssueSeverity.FATAL,
          IssueType.EXCEPTION,
          "Occured an internal bug, could not build internal authentication token. Please report"
              + " this to www.mainzelliste.de!");
    }

    // Add supplied ids to the form
    for (ID id : ids) {
      suppliedFields.put(id.getType(), id.getIdString());
    }

    // update patient from supplied FHIR Resource
    PatientsResource pr = new PatientsResource();

    // TODO: audit trail relevant (?) log output here
    logger.info("Updating " + patientId.getIdString() + " with: " + suppliedFields.toString());
    pr.editPatient(editToken.getId(), suppliedFields, req);

    // update patient
    return Persistor.instance.getPatient(patientId);
  }

  /**
   * Creates a new Patient via data supplied in the patientReader, a FHIRReader instance.
   *
   * @param patientReader FHIRReader of type (HAPI FHIR) Patient, making the data in the FHIR
   *     Patient Resource accessible
   * @param req HTTP request instance, used to create URIs for example
   * @param uriInfoContext the uriInfoContext is shamefully used to create a MultivaluedMap
   *     instance.
   * @return The create Patient instance
   */
  public Patient createPatientByReader(
      FHIRReader<org.hl7.fhir.r4.model.Patient> patientReader,
      HttpServletRequest req,
      UriInfo uriInfoContext) {
    // get all external IDs supplied in the FHIR Resource
    List<ID> inputIds = patientReader.readIds();
    // get all ID requests in the FHIR Resource and add the types from the inputIds
    // to them (needed for the Token)
    List<String> idRequests = patientReader.readIdRequests();
    idRequests.addAll(inputIds.stream().map(id -> id.getType()).collect(Collectors.toList()));
    // add default id to ensure it is created
    idRequests.add(IDGeneratorFactory.instance.getDefaultIDType());
    // get all input fields and add all external IDs to the map
    Map<String, String> inputFields = patientReader.readPatientInputFields();
    // create a Map pointing to lists from external Identifiers
    Map<String, List<String>> externalIds = new HashMap<>();
    inputIds.forEach(id -> externalIds.put(id.getType(), Collections.singletonList(id.getIdString())));

    logger.debug(
        "Parsed formular patient data from FHIR Resource: "
            + String.join(
                "&",
                inputFields.entrySet().stream()
                    .map(e -> e.getKey() + "=" + e.getValue())
                    .collect(Collectors.toList())));

    // Create a mocked addPatient Token
    Token addToken = null;
    try {
      addToken = this.createMockAddPatientToken(req, idRequests);
    } catch (JSONException je) {
      logger.error("Unable to build Token JSON or create new Patient: " + je.getMessage());
      throw new FHIRProcessingException(
          500,
          IssueSeverity.FATAL,
          IssueType.EXCEPTION,
          "Occured an internal bug, could not build internal authentication token. Please report"
              + " this to www.mainzelliste.de!");
    }

    // Use the Token and the formular data to create (or match) a Patient
    IDRequest idRequest;
    try {
      idRequest =
          PatientBackend.getDefaultInstance().createAndPersistPatient(
              inputFields,
              externalIds,
              new RequestedIdTypes(idRequests.stream().collect(Collectors.toSet())),
              true, false,
              addToken.getId());
    } catch (WebApplicationException wea) {
      String message = (String) wea.getResponse().getEntity();
      if (message.equals("Neither complete IDAT nor an external ID has been given as input!")) {
        throw new FHIRProcessingException(
            422,
            IssueSeverity.ERROR,
            IssueType.BUSINESSRULE,
            "Cannot create Patient without external Identifier or without any data supplied.");
      } else {
        throw wea;
      }
    }
    if (idRequest == null) {
      logger.error(
          "Was not able to create an idRequest for the patient. Token-ID: "
              + addToken.getId()
              + " | FormData: "
              + String.join(
                  ",",
                  inputFields.entrySet().stream()
                      .map(e -> e.getKey() + "=" + e.getValue())
                      .collect(Collectors.toList()))
              + " | ApiVersion: "
              + Servers.instance.getRequestApiVersion(req));
      throw new FHIRProcessingException(
          500,
          IssueSeverity.FATAL,
          IssueType.EXCEPTION,
          "Occured an internal bug, could not create / retrieve a Patient. Please report this to"
              + " www.mainzelliste.de!");
    }
    Patient createdPatient = idRequest.getAssignedPatient();

    // If the patient cannot be created for some reason, respond accordingly
    if (createdPatient == null) {
      logger.error(
          "Was not able to create a Patient using PatientBackend, no assigned patient in"
              + " idRequest. IDRequest(inputFields="
              + idRequest.getInputFields()
              + ",idTypes="
              + idRequest.getRequestedIdTypes()
              + ",matchResult="
              + idRequest.getMatchResult().getResultType()
              + ",assignedPatient="
              + idRequest.getAssignedPatient());
      throw new FHIRProcessingException(
          500,
          IssueSeverity.FATAL,
          IssueType.EXCEPTION,
          "Unable to create new Patient. This is most likely a bug, please report this to"
              + " www.mainzelliste.de!");
    }
    return createdPatient;
  }

  /*
   * Internal Utilities for these API methods
   */

  /**
   * Create a mocked ReadPatient Token, which is basically a copy & pase collection of calls from
   * the traditional Mainzelliste API
   *
   * @param req
   * @param idsToSearch
   * @param fieldsToRequest
   * @param idsToResult
   * @return
   * @throws JSONException
   */
  private Token createMockReadPatientToken(
      HttpServletRequest req,
      List<ID> idsToSearch,
      List<String> fieldsToResult,
      List<String> idsToResult)
      throws JSONException {
    JSONArray searchIds = new JSONArray();
    for (ID id : idsToSearch) {
      JSONObject jsonId = new JSONObject();
      jsonId.put("idType", id.getType());
      jsonId.put("idString", id.getIdString());
      searchIds.put(jsonId);
    }

    JSONObject readPatientData = new JSONObject();
    readPatientData.put("searchIds", searchIds);
    readPatientData.put("resultFields", new JSONArray(fieldsToResult));
    readPatientData.put("resultIds", new JSONArray(idsToResult));

    JSONObject readPatientToken = new JSONObject();
    readPatientToken.put("type", "readPatients");
    readPatientToken.put("data", readPatientData);
    logger.info("Created ReadToken: " + readPatientToken);
    return createAndRegisterToken(req, readPatientToken);
  }

  /**
   * Create a mocked EditPatient Token, which is basically a copy & paste collection of calls from
   * the traditional Mainzelliste API.
   *
   * @param req
   * @param patientId
   * @param fieldsToRequest
   * @param idsToRequest
   * @return
   * @throws JSONException
   */
  private Token createMockEditPatientToken(
      HttpServletRequest req, ID patientId, List<String> fieldsToRequest, List<String> idsToRequest)
      throws JSONException {
    // Build an edit Patient Token
    JSONObject editTokenData = new JSONObject();
    JSONObject editTokenJson = new JSONObject();
    editTokenData.put(
        "patientId",
        new JSONObject()
            .put("idType", patientId.getType())
            .put("idString", patientId.getIdString()));
    if (fieldsToRequest != null && fieldsToRequest.size() > 0) {
      editTokenData.put("fields", new JSONArray(fieldsToRequest));
    }
    if (idsToRequest != null && idsToRequest.size() > 0) {
      editTokenData.put("ids", new JSONArray(idsToRequest));
    }
    editTokenJson.put("type", "editPatient");
    editTokenJson.put("data", editTokenData);

    return createAndRegisterToken(req, editTokenJson);
  }

  /**
   * Create a mocked EditPatient Token, which is basically a copy & paste collection of calls from
   * the traditional Mainzelliste API.
   *
   * @param req
   * @param idsToRequest
   * @return
   * @throws JSONException
   */
  private Token createMockAddPatientToken(HttpServletRequest req, List<String> idsToRequest)
      throws JSONException {
    JSONObject addTokenJson = new JSONObject();
    JSONObject addTokenData = new JSONObject();
    if (idsToRequest != null && idsToRequest.size() > 0) {
      addTokenData.put("idtypes", new JSONArray(idsToRequest));
      addTokenJson.put("data", addTokenData);
    }
    addTokenJson.put("type", "addPatient");
    addTokenJson.put("allowedUses", "1");

    return createAndRegisterToken(req, addTokenJson);
  }

  /**
   * Creates a Token from a JSONObject and registers it to the server.
   *
   * @param req
   * @param tokenJson
   * @return
   */
  private Token createAndRegisterToken(HttpServletRequest req, JSONObject tokenJson) {
    Session mockSession = Servers.instance.newSession(req.getRemoteHost());
    Token token = new TokenParam(tokenJson.toString()).getValue();
    Servers.instance.registerToken(mockSession.getId(), token, req.getRemoteAddr());
    return token;
  }
}
