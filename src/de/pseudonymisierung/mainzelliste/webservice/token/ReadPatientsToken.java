/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.webservice.token;

import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.CryptoID;
import de.pseudonymisierung.mainzelliste.DerivedIDGenerator;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.IDGenerator;
import de.pseudonymisierung.mainzelliste.IDGeneratorFactory;
import de.pseudonymisierung.mainzelliste.Servers.ApiVersion;
import de.pseudonymisierung.mainzelliste.WildcardID;
import de.pseudonymisierung.mainzelliste.dto.Persistor;
import de.pseudonymisierung.mainzelliste.exceptions.GeneralCryptoException;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidTokenException;
import de.pseudonymisierung.mainzelliste.exceptions.NotImplementedException;
import de.pseudonymisierung.mainzelliste.webservice.Token;
import de.pseudonymisierung.mainzelliste.webservice.commons.MainzellisteCallbackUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

/**
 * allows to read IDAT or pseudonyms of given patients
 */
public class ReadPatientsToken extends Token {
  public static final String TYPE = "readPatients";

  private final List<PatientSearchId> patientSearchIds = new ArrayList<>();

  private final List<String> searchIdTypes = new ArrayList<>();
  private final List<String> baseSearchIdTypes = new ArrayList<>();
  private boolean readAllPatients = false;
  private final Set<String> resultFields = new LinkedHashSet<>();
  private final Set<String> resultIds = new LinkedHashSet<>();

  private boolean returnAssociatedIds = false;
  private boolean containAssociatedSearchId = false;

  private boolean readAllPatientIds;
  private boolean readAllPatientIdTypes;
  private String callback;
  private String redirect;

  public ReadPatientsToken(int allowedUses) {
    super(TYPE, allowedUses);
  }

  public void setData(Map<String, ?> data) {
    super.setData(data);
    //deserialize resultIds
    this.resultIds.addAll(this.getDataItemAsStringList("resultIds"));

    //deserialize searchIds
    List<Map<String, String>> searchIdsMap;
    try {
      searchIdsMap = (List<Map<String, String>>) this.getDataItemList("searchIds");
      // check that IDs to search for are provided
      if (searchIdsMap == null) {
        throw new InvalidTokenException(
            "Please provide an array of IDs as field 'searchIds' in token data!");
      }
    } catch (ClassCastException e) {
      throw new InvalidTokenException(
          "Field 'searchIds' has wrong format. Expected array of IDs, received: "
              + this.getData().get("searchIds"));
    }
    this.returnAssociatedIds = this.resultIds.stream()
        .anyMatch(IDGeneratorFactory.instance::isAssociatedIdTypeExist);
    int order = 0;
    for (Map<String, String> searchId : searchIdsMap) {
      // parse patientId
      String idType = StringUtils.trimToNull(searchId.get("idType"));
      String idString = StringUtils.trimToNull(searchId.get("idString"));
      if (idType == null || idString == null) {
        throw new InvalidTokenException("Every item in 'searchIds' must be an object with fields "
            + "'idType' and 'idString'. Found: " + searchId);
      }

      // parse readAllPatient flag
      if (idType.equals("*")) {
        //validate data
        if (searchIdsMap.size() > 1) {
          throw new InvalidTokenException(
              "reading all patients with wildcard and searching patients with specific ids or id type with wildcard in the same 'searchIds' array not supported");
        }
        if (!idString.equals("*")) {
          throw new InvalidTokenException("invalid id string");
        }
        if (this.returnAssociatedIds) {
          throw new NotImplementedException(
              "returning associated IDs in a request with wildcard select is not supported yet");
        }
        this.readAllPatients = true;
      } else {
        if (!containAssociatedSearchId && IDGeneratorFactory.instance.isAssociatedIdTypeExist(idType)) {
          containAssociatedSearchId = true;
        }
        checkIdType(idType);
        if (!idString.equals("*")) {
          // build search id
          ID id;
          try { // Decrypt input id, if configured
            id = IDGeneratorFactory.instance.decryptAndBuildId(idType, idString);
          } catch (GeneralCryptoException e) {
            id = IDGeneratorFactory.instance.buildId(idType, idString);
          }
          // find base id
          ID baseId = id;
          if (id instanceof CryptoID) {
            IDGenerator<? extends ID> searchIDGenerator = IDGeneratorFactory.instance
                .getGenerator(id.getType());
            baseId = ((DerivedIDGenerator) searchIDGenerator).getBaseId(id);
          }

          patientSearchIds.add(new PatientSearchId(order, baseId, id));
          order++;
        } else { // if search id with wildcard
          searchIdTypes.add(idType);

          //convert to baseIdTypes
          IDGenerator<? extends ID> idGenerator = IDGeneratorFactory.instance.getGenerator(idType);
          baseSearchIdTypes.add(!idGenerator.isPersistent() ?
              ((DerivedIDGenerator) idGenerator).getBaseIdType() : idType);

          if (this.returnAssociatedIds) {
            throw new NotImplementedException(
                "Returning associated IDs in a request with wildcard select is not supported yet");
          }
        }
        //validate
        if (!patientSearchIds.isEmpty() && !searchIdTypes.isEmpty()) {
          throw new NotImplementedException(
              "searching patients with specific patient ids and wildcard in the same 'searchIds' array is not supported yet.");
        }
      }
    }

    //deserialize resultFields
    resultFields.addAll(this.getDataItemAsStringList("resultFields"));
    this.readAllPatientIds = this.getDataItemAsBoolean("readAllPatientIds");
    this.readAllPatientIdTypes = this.getDataItemAsBoolean("readAllPatientIdTypes");
    this.callback = StringUtils.trimToEmpty(this.getDataItemString("callback"));
    this.redirect = StringUtils.trimToEmpty(this.getDataItemString("redirect"));
  }

  @Override
  public void checkValidity(ApiVersion apiVersion) {
    if (this.patientSearchIds.isEmpty() && !searchWithWildCard() && !isReadAllPatients()) {
      throw new InvalidTokenException(
          "Please provide an array of IDs as field 'searchIds' in token data!");
    }

    //check searchIds
    if (apiVersion.majorVersion < 3
        || apiVersion.majorVersion == 3 && apiVersion.minorVersion < 2) {
      for (PatientSearchId searchPatientId: this.patientSearchIds) {
        ID id = searchPatientId.getSearchId();
        if (id instanceof WildcardID) {
          continue;
        }
        // CryptoIds are transient, compute the base id to check if the patient exists
        IDGenerator<? extends ID> generator = IDGeneratorFactory.instance.getGenerator(
            id.getType());
        if (!generator.isPersistent()) {
          id = ((DerivedIDGenerator) generator).getBaseId(id);
        }
        if (!Persistor.instance.patientExists(id.getType(), id.getIdString())) {
          throw new InvalidTokenException("No patient found with provided " + id.getType() + " '"
              + id.getIdString() + "'!");
        }
      }
    }

    checkResultFields();
    checkResultIds(patientSearchIds.stream().map(e -> e.getSearchId().getType()).toList());

    // Check callback URL
    if (!this.callback.isEmpty()) {
      MainzellisteCallbackUtil.checkCallbackUrl(this.callback);
    }

    if (Config.instance.auditTrailIsOn()) {
      checkAuditTrail();
    }
  }

  /**
   * Check if "resultFields" contains only valid field names.
   */
  private void checkResultFields() {
    Set<String> fieldList = Config.instance.getFieldKeys();
    if (this.resultFields.isEmpty()) {
      return; // Allow omitting resultFields (same semantics as providing empty array).
    }

    for (String resultField : this.resultFields) {
      if (!fieldList.contains(resultField)) {
        throw new InvalidTokenException("Field '" + resultField
            + "' provided in field list is unknown!");
      }
    }
  }

  /**
   * Check if "resultIds" contains only valid ID types.
   */
  private void checkResultIds(Collection<String> searchIdTypes) {
    Set<String> definedIdTypes = new HashSet<>(
        Arrays.asList(IDGeneratorFactory.instance.getIDTypes()));
    if (resultIds.isEmpty()) {
      return; // Allow omitting resultIds (same semantics as providing empty array).
    }

    // check if result id types is configured
    Map<String, List<String>> relatedAssociatedIdTypeMap = new HashMap<>();
    for (String thisIdType : resultIds) {
      if (!definedIdTypes.contains(thisIdType)) {
        List<String> relatedAssociatedIdTypes = IDGeneratorFactory.instance.getRelatedAssociatedIdTypes(
            thisIdType);
        if (relatedAssociatedIdTypes.isEmpty()) {
          throw new InvalidTokenException("ID type '" + thisIdType + "' provided in ID type list is"
              + " unknown!");
        }
        relatedAssociatedIdTypeMap.put(thisIdType, relatedAssociatedIdTypes);
      }
    }

    // Note: if at least one of the "resultIds" is an associated ID, all "searchIds" must be related
    // associated IDs too, in order to return exactly one corresponding associated ID, otherwise
    // the request would be ambiguous and uncertain which associated ID should be returned.
    for (Entry<String, List<String>> relatedAssociatedIdTypes : relatedAssociatedIdTypeMap.entrySet()) {
      for (String currentSearchIdType : searchIdTypes) {
        if (IDGeneratorFactory.instance.isIdTypeExist(currentSearchIdType)
            || !relatedAssociatedIdTypes.getValue().contains(currentSearchIdType)) {
          throw new InvalidTokenException("In order to return the requested corresponding "
              + "associated ID '" + relatedAssociatedIdTypes.getKey() + "', all searched ID in "
              + "'searchIds' ['" + String.join(",", searchIdTypes) + "'] must be related "
              + "associated ID too");
        }
      }
    }
  }

  public List<PatientSearchId> getPatientSearchIds() {
    return patientSearchIds;
  }

  public List<String> getSearchIdTypes() {
    return this.searchIdTypes;
  }

  public List<String> getBaseSearchIdTypes() {
    return baseSearchIdTypes;
  }

  public boolean searchWithWildCard() {
    return !this.searchIdTypes.isEmpty();
  }

  public boolean isReadAllPatients() {
    return readAllPatients;
  }

  public Set<String> getResultFields() {
    return resultFields;
  }

  public Set<String> getResultIds() {
    return resultIds;
  }

  public boolean isReadAllPatientIds() {
    return readAllPatientIds;
  }

  public boolean isReadAllPatientIdTypes() {
    return readAllPatientIdTypes;
  }

  /**
   * @return true if "resultIds" contains at least one associated id type
   */
  public boolean isReturnAssociatedIds() {
    return returnAssociatedIds;
  }

  /**
   * @return true if "searchIds" contains at least one associated id type
   */
  public boolean containAssociatedSearchId() {
    return containAssociatedSearchId;
  }

  public String getCallback() {
    return callback;
  }

  public String getRedirect() {
    return redirect;
  }

  public static class PatientSearchId {
    final int order;
    final ID baseId;
    final ID searchId;

    public PatientSearchId(int order, ID baseId, ID searchId) {
      this.order = order;
      this.baseId = baseId;
      this.searchId = searchId;
    }

    public int getOrder() {
      return order;
    }

    public ID getBaseId() {
      return baseId;
    }

    public ID getSearchId() {
      return searchId;
    }
  }
}
