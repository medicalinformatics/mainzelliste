/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.webservice;

import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.DerivedIDGenerator;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.IDGenerator;
import de.pseudonymisierung.mainzelliste.IDGeneratorFactory;
import de.pseudonymisierung.mainzelliste.Servers.ApiVersion;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CreateIdsToken extends Token {

	private final List<ID> searchIds = new ArrayList<>();
	private final Set<String> idTypes = new LinkedHashSet<>();

	public CreateIdsToken(int allowedUses) {
		super("createIds", allowedUses);
	}

	@Override
	public void setData(Map<String, ?> data) {
		super.setData(data);
		List<Map<String, ?>> searchIdMap = (List<Map<String, ?>>) this.getDataItemList("searchIds");
		for (int i = 0; i < searchIdMap.size(); i++) {
			Map<String, String> searchId = (Map<String, String>) searchIdMap.get(i);
			String idType = searchId.get("idType");
			String idString = searchId.get("idString");

			searchIds.add(createIdFromJson(idType, idString));
		}

		if (this.hasDataItem("idTypes"))
			this.getDataItemList("idTypes").forEach( o -> idTypes.add(o.toString()));
	}

	private ID createIdFromJson(String idType, String idString) {
		ID id = IDGeneratorFactory.instance.decryptAndBuildId(idType, idString);

		IDGenerator<?> generator = IDGeneratorFactory.instance.getGenerator(id.getType());
		if (!generator.isPersistent()) {
			// CryptoIds are transient, compute the base id to check if the patient exists
			id = ((DerivedIDGenerator) generator).getBaseId(id);
		}
		return id;
	}

	@Override
	public void checkValidity(ApiVersion apiVersion) {
		for (ID searchId : searchIds) {
			this.checkIdType(String.valueOf(searchId.getType()));
		}
		if (this.hasDataItem("idTypes")) {
			this.checkIdTypes(this.idTypes);
		}
		if (Config.instance.auditTrailIsOn()) {
			checkAuditTrail();
		}
	}

	public List<ID> getSearchIds() {
		return searchIds;
	}

	public Set<String> getIdTypes() {
		return idTypes;
	}

	public boolean containIdTypesIgnoreCase(String idType) {
		for(String currentIdType : idTypes)
			if(currentIdType.equalsIgnoreCase(idType))
				return true;
		return false;
	}
}
