/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.webservice;

import com.google.gson.Gson;
import de.pseudonymisierung.mainzelliste.AssociatedIds;
import de.pseudonymisierung.mainzelliste.AuditTrail;
import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.Field;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.IDGeneratorFactory;
import de.pseudonymisierung.mainzelliste.IDRequest;
import de.pseudonymisierung.mainzelliste.Patient;
import de.pseudonymisierung.mainzelliste.PatientBackend;
import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.Session;
import de.pseudonymisierung.mainzelliste.api.AddPatientRequest;
import de.pseudonymisierung.mainzelliste.api.IdResponse;
import de.pseudonymisierung.mainzelliste.api.ReadPatientResponse;
import de.pseudonymisierung.mainzelliste.dto.Persistor;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidJSONException;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidTokenException;
import de.pseudonymisierung.mainzelliste.exceptions.UnauthorizedException;
import de.pseudonymisierung.mainzelliste.matcher.MatchResult;
import de.pseudonymisierung.mainzelliste.matcher.MatchResult.MatchResultType;
import de.pseudonymisierung.mainzelliste.model.persistor.search.SearchPatientResult;
import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.PatientFieldFilter;
import de.pseudonymisierung.mainzelliste.permissions.PermissionEnum;
import de.pseudonymisierung.mainzelliste.service.CallbackService;
import de.pseudonymisierung.mainzelliste.webservice.commons.CallbackFactory;
import de.pseudonymisierung.mainzelliste.webservice.commons.Redirect;
import de.pseudonymisierung.mainzelliste.webservice.commons.RedirectBuilder;
import de.pseudonymisierung.mainzelliste.webservice.commons.RedirectUtils;
import de.pseudonymisierung.mainzelliste.webservice.token.ReadPatientsToken;
import de.pseudonymisierung.mainzelliste.webservice.token.ReadPatientsToken.PatientSearchId;
import jakarta.inject.Singleton;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.glassfish.jersey.server.mvc.Viewable;
import org.glassfish.jersey.uri.UriTemplate;

/**
 * Resource-based access to patients.
 */
@Path("/patients")
@Singleton
public class PatientsResource {
  /**
   * The logging instance.
   */
  private static final Logger logger = LogManager.getLogger(PatientsResource.class);

  private final Gson gson = new Gson();

  /**
   * Session to be used when in debug mode.
   */
  private Session debugSession = null;

  private static final CallbackService callbackService = new CallbackService();

  /**
   * Get a list of patients.
   *
   * @param request The injected HttpServletRequest.
   * @param tokenId Id of a valid "readPatients" token.
   * @return A JSON result as specified in the API documentation.
   * @throws UnauthorizedException If no token is provided.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getAllPatients(@Context HttpServletRequest request,
      @QueryParam("limit") int limit,
      @QueryParam("page") int page,
      @QueryParam("tokenId") String tokenId,
      @QueryParam("ignoreOrder") boolean ignoreOrder,
      @Context UriInfo uriInfo) {
    logger.debug("Received GET /patients");
    return this.getPatients(tokenId, limit, page, ignoreOrder, request, uriInfo);
  }

	/**
	 * Create a new patient. Interface for web browser.
	 * 
	 * @param tokenId
	 *            Id of a valid "addPatient" token.
	 * @param mainzellisteApiVersion
	 *            The API version used to make the request.
	 * @param form
	 *            Input as provided by the HTML form.
	 * @param request
	 *            The injected HttpServletRequest.
	 * @return An HTTP response as specified in the API documentation.
	 */
  @POST
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces({MediaType.TEXT_HTML, MediaType.WILDCARD})
  public synchronized Response newPatientBrowser(
      @QueryParam("tokenId") String tokenId,
      @QueryParam("mainzellisteApiVersion") String mainzellisteApiVersion,
      MultivaluedMap<String, String> form,
      @Context HttpServletRequest request) {
    try {
      logger.debug("@POST newPatientBrowser");
      AddPatientToken token = getAddPatientToken(tokenId);
      logger.info("Handling ID Request with token {}", token.getId());
      IDRequest createRet = addNewPatient(form, token.getFields(), token.getIds(),
          token.getRequestedIdTypes(), token.getId());

      List<ID> ids = createRet.getRequestedIds(token.getRequestedIdTypeSet());
			MatchResult result = createRet.getMatchResult();
			Map <String, Object> map = new HashMap<String, Object>();
			if (ids.isEmpty()) { // unsure case
				// Copy form to JSP model so that input is redisplayed
				for (String key : form.keySet())
				{
					map.put(key, form.getFirst(key));
				}
				map.put("readonly", "true");
				map.put("tokenId", tokenId);
				map.put("mainzellisteApiVersion", mainzellisteApiVersion);
				return Response.status(Status.CONFLICT)
						.entity(new Viewable("/unsureMatch.jsp", map)).build();
			} else {
                    // If Idat are to be redisplayed in the result form...
                    if (Boolean.parseBoolean(Config.instance.getProperty("result.printIdat"))) {
                        // ...copy input to JSP
                        for (String key : form.keySet()) {
                            map.put(key, form.getFirst(key));
                        }
                        // and set flag for JSP to display them
                        map.put("printIdat", true);
                    }

                    map.put("ids", callbackService.filterIds(ids, token.getResultIds()));

                    map.put("tentative", false);
                    // Only put true in map if one or more PID are tentative
                    for (ID id : ids) {
                        if (id != null && id.isTentative()) {
                            map.put("tentative", true);
                            break;
                        }
                    }

                    if (Config.instance.debugIsOn() && result.getResultType() != MatchResultType.NON_MATCH) {
                        map.put("debug", "on");
                        map.put("weight", Double.toString(result.getBestMatchedWeight()));
                        Map<String, Field<?>> matchedFields = result.getBestMatchedPatient().getFields();
                        Map<String, String> bestMatch = new HashMap<String, String>();
                        for (String fieldName : matchedFields.keySet()) {
                            bestMatch.put(fieldName, matchedFields.get(fieldName).toString());
                        }
                        map.put("bestMatch", bestMatch);
                    }
                    // Callback request
                    String callbackUrl = token.getCallbackUrl();
                    if (!callbackUrl.isEmpty()) {
                        callbackService.sendCallback(request, token.getId(), callbackUrl, ids,
                            token.getCallbackResultIds(), null, null);
                    }
                    callbackService.sendCallbacks(request, token.getId(), token.getRequestedIdTypeSet(), ids,
                        createRet.getInputFields());

                    String redirectRequest = token.getDataItemString("redirect");
                    if (redirectRequest != null && redirectRequest.length() > 0) {
                        UriTemplate redirectURITempl = new UriTemplate(token.getDataItemString("redirect"));
                        List<String> templateVariables = redirectURITempl.getTemplateVariables();
                      List<String> requestedIds = createRet.getRequestedIdTypes().isEmpty() ?
                          Collections.singletonList(IDGeneratorFactory.instance.getDefaultIDType()) :
                          new ArrayList<>(createRet.getRequestedIdTypes());

                        Redirect redirect = new RedirectBuilder().setTokenId(token.getId())
                                .setMappedIdTypesdAndIds(requestedIds, createRet).setTemplateURI(redirectURITempl)
                                .build();

                        if (templateVariables.contains("tokenId")
                                && !Boolean.parseBoolean(Config.instance.getProperty("result.show"))) {
                            return redirect.execute();
                        } else {
                            // Remove query parameters and pass them to JSP. The redirect is put
                            // into the "action" tag of a form and the parameters are passed as
                            // hidden fields
                            // TODO: generate for frontend
                            map.put("redirect", redirect.getRedirectURI());
                            map.put("redirectParams", redirect.getRedirectParams());
                            return Response.ok(new Viewable("/patientCreated.jsp", map)).build();
                        }
                    }

                    return Response.ok(new Viewable("/patientCreated.jsp", map)).build();
                }

            } catch (WebApplicationException e) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("message", e.getResponse().getEntity());
                return Response.status(e.getResponse().getStatus()).entity(new Viewable("/errorPage.jsp", map)).build();
            }
    }

	/**
	 * Create a new patient. Interface for software applications.
	 * 
	 * @param tokenId
	 *            Id of a valid "addPatient" token.
	 * @param request
	 *            The injected HttpServletRequest.
	 * @param context
	 *            Injected information of application and request URI.
	 * @param form
	 *            Input as provided by the HTTP request.
	 * @return An HTTP response as specified in the API documentation.
	 * @throws JSONException
	 *             If a JSON error occurs.
	 */
  @POST
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(MediaType.APPLICATION_JSON)
  public synchronized Response newPatientJson(
      @QueryParam("tokenId") String tokenId,
      @Context HttpServletRequest request,
      @Context UriInfo context,
      MultivaluedMap<String, String> form) throws JSONException {
    logger.debug("@POST newPatientJson");

    AddPatientToken token = getAddPatientToken(tokenId);
    logger.info("Handling ID Request with token {}", token.getId());
    IDRequest response = addNewPatient(form, token.getFields(), token.getIds(),
        token.getRequestedIdTypes(), token.getId());
    return handleAddPatientResponse(request, context, response, token);
  }

  /**
   * Create a new patient. Interface for software applications.
   *
   * @param tokenId   Id of a valid "addPatient" token.
   * @param request   The injected HttpServletRequest.
   * @param context   Injected information of application and request URI.
   * @param inputData Input as json.
   * @return An HTTP response as specified in the API documentation.
   * @throws JSONException If a JSON error occurs.
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public synchronized Response addPatientFromJson(
      @QueryParam("tokenId") String tokenId,
      @Context HttpServletRequest request,
      @Context UriInfo context,
      String inputData) throws JSONException {
    logger.debug("@POST addPatientFromJson");
    AddPatientToken token = getAddPatientToken(tokenId);
    logger.info("Handling ID Request with token {}", token.getId());

    // deserialize input to json
    AddPatientRequest inputDataJson = AddPatientRequest.fromJson(inputData);

    // override input fields from Token
    Map<String, String> fields = new HashMap<>(token.getFields());
    if (inputDataJson.getFields() != null) {
      fields.putAll(inputDataJson.getFields());
    }

    // override external ids from Token
    Map<String, List<String>> externalIds = new LinkedHashMap<>();
    if (inputDataJson.getIds() != null) {
      inputDataJson.getIds().forEach((k,l) -> {
        Optional.ofNullable(token.getIds().get(k)).ifPresent(l::add);
        externalIds.put(k, l);
      });
    } else {
      token.getIds().forEach((k, v) -> externalIds.put(k, Arrays.asList(v)));
    }

    // create patient
    IDRequest response = PatientBackend.getDefaultInstance().createAndPersistPatient(fields, externalIds,
        token.getRequestedIdTypes(), inputDataJson.isSureness(), inputDataJson.ignoreInvalidIDAT(), token.getId());
    return handleAddPatientResponse(request, context, response, token);
  }

  private Response handleAddPatientResponse(HttpServletRequest request, UriInfo context,
      IDRequest response, AddPatientToken token) throws JSONException{
    //handle possible matches
		if (response.getMatchResult().getResultType() == MatchResultType.POSSIBLE_MATCH && response.getRequestedIds().isEmpty()) {
			JSONObject ret = new JSONObject();
			if (token.showPossibleMatches()) {
				JSONArray possibleMatches = new JSONArray();
				for (Entry<Double, List<Patient>> possibleMatch : response.getMatchResult().getPossibleMatches().entrySet()) {
					for (Patient p : possibleMatch.getValue())
						possibleMatches.put(p.createId(IDGeneratorFactory.instance.getDefaultIDType()).toJSON());
				}
				ret.put("possibleMatches", possibleMatches);
			}
			ret.put("message", "Unable to definitely determined whether the data refers to an existing or to a new "
					+ "patient. Please check data or resubmit with sureness=true to get a tentative result. Please check"
					+ " documentation for details.");
			return Response
					.status(Status.CONFLICT)
					.entity(ret.toString())
					.build();
		}

		List<ID> newIds = new LinkedList<>(response.getRequestedIds(token.getRequestedIdTypeSet()));
    //filter result ids that will be returned in the response payload
    List<ID> resultIds = callbackService.filterIds(newIds, token.getResultIds());

		int apiMajorVersion = Servers.instance.getRequestMajorApiVersion(request);

    // execute callback
    String callbackUrl = token.getCallbackUrl();
    if (!callbackUrl.isEmpty()) {
      callbackService.sendCallback(request, token.getId(), callbackUrl, newIds, token.getCallbackResultIds(), null, null);
    }
    callbackService.sendCallbacks(request, token.getId(), token.getRequestedIdTypeSet(), newIds,
        response.getInputFields());

		if (apiMajorVersion >= 2) {
            String redirect = token.getDataItemString("redirect");
            if (redirect != null && !redirect.isEmpty()) {
                UriTemplate redirectURITempl = new UriTemplate(token.getDataItemString("redirect"));
                List<String> templateVariables = redirectURITempl.getTemplateVariables();
                List<String> requestedIds = token.getRequestedIdTypeSet().isEmpty() ?
                    Collections.singletonList(IDGeneratorFactory.instance.getDefaultIDType()) :
                    new ArrayList<>(token.getRequestedIdTypeSet());

                if (templateVariables.contains("tokenId")) {
                    return new RedirectBuilder().setTokenId(token.getId())
                            .setMappedIdTypesdAndIds(requestedIds, response).setTemplateURI(redirectURITempl)
                            .build().execute();
                } else {
                    return new RedirectBuilder().setMappedIdTypesdAndIds(requestedIds, response).setTemplateURI(redirectURITempl)
                            .build().execute();
                }
            }

			JSONArray ret = new JSONArray();
			for (ID thisID : resultIds) {
				URI newUri = context.getBaseUriBuilder()
						.path(PatientsResource.class)
						.path("/{idtype}/{idvalue}")
						.build(thisID.getType(), thisID.getEncryptedIdStringFirst());
	
				ret.put(thisID.toJSON()
					.put("uri", newUri));
			}
					
			return Response
				.status(Status.CREATED)
				.entity(ret.toString())
				.build();
		} else {
			/*
			 *  Old api permits only one ID in response. If several
			 *  have been requested, which one to choose?
			 */
			if (resultIds.size() > 1) {
				throw new WebApplicationException(
						Response.status(Status.BAD_REQUEST)
						.entity("Selected API version 1.0 permits only one ID in response, " +
								"but several were requested. Set mainzellisteApiVersion to a " +
								"value >= 2.0 or request only one ID type in token.")
								.build());
			}
			
			ID newId = resultIds.get(0);
			
			URI newUri = context.getBaseUriBuilder()
					.path(PatientsResource.class)
					.path("/{idtype}/{idvalue}")
					.build(newId.getType(), newId.getIdString());
			
			JSONObject ret = new JSONObject()
					.put("newId", newId.getEncryptedIdStringFirst())
					.put("tentative", newId.isTentative())
					.put("uri", newUri);

            return Response
                    .status(Status.CREATED)
                    .entity(ret.toString())
                    .location(newUri)
                    .build();
        }
	}

  /**
   * Get patients via "readPatient" token.
   *
   * @param tokenId Id of a valid "readPatient" token.
   * @return A JSON result as specified in the API documentation.
   */
  @Path("/tokenId/{tokenId}")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getPatients(@PathParam("tokenId") String tokenId,
      @QueryParam("limit") int limit,
      @QueryParam("page") int page,
      @QueryParam("ignoreOrder") boolean ignoreOrder,
      @Context HttpServletRequest request,
      @Context UriInfo uriInfo) {
    logger.info("Received request to get patient with token {}", tokenId);
    logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "start getting patients");

    // Check if token exists and has the right type.
    // Validity of token is checked upon creation
    ReadPatientsToken token = Servers.instance.findToken(tokenId, ReadPatientsToken.class,
        ReadPatientsToken.TYPE);

    // calculate offset and validate pagination parameters
    int offset = calculateOffset(page, limit);

    //init patient field filter from query parameters
    PatientFieldFilter patientFieldFilter = new PatientFieldFilter(uriInfo.getQueryParameters(),
        Persistor.instance.getPatientFieldSearchConditionFieldMap());

    List<AuditTrail> auditTrailRecords = Collections.synchronizedList(new ArrayList<>());
    List<PatientSearchId> patientSearchIds = token.getPatientSearchIds();

    if (limit > 0 && !ignoreOrder) { // searchIds contains ids
      patientSearchIds = patientSearchIds.stream()
          .filter(e -> e.getOrder() >= offset && e.getOrder() < offset + limit).toList();
    }
    SearchPatientResult searchPatientResult = PatientBackend.getDefaultInstance().findPatients(
        token, request, patientFieldFilter, patientSearchIds, ignoreOrder, limit, offset);

    List<Patient> patients = searchPatientResult.patients();

    // serialize patients to json
    Collection<ReadPatientResponse> response = patientsToJson(patients, token, request, auditTrailRecords,
        patientSearchIds,
        token.searchWithWildCard() || token.isReadAllPatients() || ignoreOrder,
        limit, offset);
    logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "serializing patients done");

    //persist audit trails
    if (Config.instance.auditTrailIsOn()) {
      Persistor.instance.persistAuditTrailRecords(auditTrailRecords);
    }

    // return 404 if no patient found
    if (patients.isEmpty()) {
      return Response.status(Status.NOT_FOUND)
          .entity("No patient found")
          .build();
    }

    // Callback
    String callbackUrl = token.getCallback();
    if (!callbackUrl.isEmpty()
        && Servers.instance.hasPermissionByName(token.getParentServerName(),
        PermissionEnum.CALLBACK)) {
      callbackService.executes(Collections.singletonList(CallbackFactory.createCallback(
          Servers.instance.getRequestApiVersion(request),
          callbackUrl, token.getId(), null, null, response, null)
      ));
    }

    //TODO refactor redirect -> reduce code complexity
    // Redirect
    String redirect = token.getRedirect();
    if (!redirect.isEmpty()
        && Servers.instance.hasPermissionByName(token.getParentServerName(), "redirect")) {
      UriTemplate redirectURITempl = new UriTemplate(token.getRedirect());
      List<String> templateVariables = redirectURITempl.getTemplateVariables();

      if (templateVariables.contains("tokenId")) {
        // TODO: send mapped list of requests?
        if (!token.getPatientSearchIds().isEmpty() && !patients.isEmpty()) {
          List<String> requestedIds = RedirectUtils.getRequestedIDsTypeFromToken(token);
          // TODO check if the first patient in the list patients correspond to the patient with the first searchIds
          return new RedirectBuilder().setTokenId(token.getId())
              .setMappedIdTypesdAndIds(requestedIds, patients.get(0))
              .setTemplateURI(redirectURITempl)
              .build().execute();
        }
      } else {
        return Response.status(HttpStatus.SC_BAD_REQUEST)
            .entity("Couldn't generate redirect because request is not valid").build();
      }
    }

    return Response.ok()
        .header("X-Total-Count", new AtomicLong(searchPatientResult.totalCount()).get())
        .entity(gson.toJson(response))
        .build();
  }

  private Collection<ReadPatientResponse> patientsToJson(List<Patient> patients, ReadPatientsToken token, HttpServletRequest request,
      List<AuditTrail> auditTrailRecords, List<PatientSearchId> patientSearchIds,
      boolean ignoreOrder, int paginationLimit, int offset) {

    // serialize patients to json
    if (ignoreOrder) {
      List<ReadPatientResponse> response = new ArrayList<>(patients.size());
      for (Patient patient : patients) {

        // find associated search id if provided
        ID searchAssociatedId = null;
        if (!patientSearchIds.isEmpty()) {
          searchAssociatedId = patientSearchIds.stream()
              .map(PatientSearchId::getBaseId)
              .filter(patient::hasId)
              .findFirst()
              .filter(i -> !IDGeneratorFactory.instance.isIdTypeExist(i.getType()))
              .orElse(null);
        }

        ReadPatientResponse readPatientResponse = patientToJson(patient, searchAssociatedId, token);
        response.add(readPatientResponse);

        // create audit trail record
        if (Config.instance.auditTrailIsOn()) {
          auditTrailRecords.add(PatientBackend.getDefaultInstance().buildReadAuditTrailRecord(
              token.getId(), patient.getIds().iterator().next(),
              gson.toJson(readPatientResponse)));
        }
      }
      return response;
    } else { // return result in the same order as searchIds
      Map<Integer, ReadPatientResponse> response = Collections.synchronizedMap(
          new HashMap<>(paginationLimit > 0 ? paginationLimit : patientSearchIds.size()));

      // filter results
      ReentrantLock lock = new ReentrantLock();
      patientSearchIds.parallelStream().forEach(patientSearchId -> {
        //find patient from result list with the given searchId
        Patient correspondingPatient = patients.stream()
            .filter(p -> p.hasId(patientSearchId.getBaseId()))
            .findFirst().orElse(null);

        // serialize patient to json
        ID searchAssociatedId =
            IDGeneratorFactory.instance.isIdTypeExist(patientSearchId.getBaseId().getType()) ?
                null : patientSearchId.getBaseId();
        ReadPatientResponse readPatientResponse = patientToJson(correspondingPatient, searchAssociatedId, token);
        lock.lock();
        try {
          response.put(patientSearchId.getOrder() - offset, readPatientResponse);

          // create audit trail record
          if (correspondingPatient != null && Config.instance.auditTrailIsOn()) {
            auditTrailRecords.add(PatientBackend.getDefaultInstance().buildReadAuditTrailRecord(
                token.getId(), patientSearchId.getSearchId(), gson.toJson(readPatientResponse)));
          }
        } finally {
          lock.unlock();
        }
      });
      return response.values();
    }
  }

  private int calculateOffset(int page, int limit) {
    // validate input
    String errorMessage = "";
    if (page < 0 || limit < 0) {
      errorMessage = (page < 0 ? "page" : "limit") + " query parameter must be positive";
    } else if (page > 0 && limit == 0 || page == 0 && limit > 0) {
      errorMessage = (page == 0 ? "page" : "limit") + " query parameter must be greater than 0";
    }
    if (!errorMessage.isEmpty()) {
      logger.error(errorMessage);
      throw new WebApplicationException(
          Response.status(Status.BAD_REQUEST).entity(errorMessage).build());
    }

    // calculate offset (pre condition: page > 0 & limit > 0 |  page == 0 && limit == 0)
    return page < 1 ? 0 : (page - 1) * limit;
  }

  private ReadPatientResponse patientToJson(Patient patient, ID associatedSearchId, ReadPatientsToken token) {
    List<IdResponse> idsResponse = new ArrayList<>();
    Map<String, String> fieldsResponse = new HashMap<>();
    List<String> idTypes = new ArrayList<>();

    if(patient == null) {
      return new ReadPatientResponse(null, null, null);
    }

    // serialize patient fields
    if (!token.getResultFields().isEmpty()) {
      Set<String> requestedFieldTypes = token.getResultFields();
      fieldsResponse = patient.getInputFields().entrySet().stream()
          .filter(e -> requestedFieldTypes.contains(e.getKey()))
          .collect(Collectors.toMap(Entry::getKey, e -> e.getValue().toString()));
      if(fieldsResponse.values().stream().allMatch(StringUtils::isBlank))
        fieldsResponse = Collections.emptyMap();
    }

    // serialize patient ids
    if (token.isReadAllPatientIds()) {
      Servers.instance.checkPermissionByName(token.getParentServerName(), PermissionEnum.READ_ALL_PATIENT_IDS);
      idsResponse = patient.getIds().stream()
          .map(id -> new IdResponse(id.getType(), id.getEncryptedIdStringFirst(), id.isTentative(), null))
          .toList();

    } else if (!token.getResultIds().isEmpty()) {
      idsResponse = token.getResultIds().stream()
          .map( t -> IDGeneratorFactory.instance.isIdTypeExist(t)? patient.findId(t) :
              patient.findAssociatedId(t, associatedSearchId))
          .filter(Objects::nonNull)
          .map(id -> new IdResponse(id.getType(), id.getEncryptedIdStringFirst(), id.isTentative(), null))
          .toList();
    }

    // serialize patient id types
    if (token.isReadAllPatientIdTypes()) {
      Servers.instance.checkPermissionByName(token.getParentServerName(), PermissionEnum.READ_ALL_PATIENT_ID_TYPES);
      idTypes = patient.getIds().stream().map(ID::getType).toList();
    }

    return new ReadPatientResponse(
        idsResponse.isEmpty() ? null : idsResponse,
        fieldsResponse.isEmpty() ? null : fieldsResponse,
        idTypes.isEmpty()? null : idTypes
    );
  }

    /**
     * Edit a patient. Interface for web browsers. The patient to edit is determined
     * from the given "editPatient" token.
     *
     * @param tokenId A valid "editPatient" token.
     * @param form    Input as provided by the HTML form.
     * @param request The injected HttpServletRequest.
     * @return An HTTP response as specified in the API documentation.
     */
    @Path("/tokenId/{tokenId}")
    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response editPatientBrowser(@PathParam("tokenId") String tokenId, MultivaluedMap<String, String> form,
                                       @Context HttpServletRequest request) {
        logger.debug("@PUT editPatientBrowser");

            try {
                // Collect fields from input form
                Map<String, String> newFieldValues = new HashMap<String, String>();
                for (String fieldName : form.keySet()) {
                    newFieldValues.put(fieldName, form.getFirst(fieldName));
                }

                EditPatientToken t = this.editPatient(tokenId, newFieldValues, request);

                if (t.getRedirect() != null) {
                    return Response.status(Status.SEE_OTHER).header("Location", t.getRedirect().toString()).build();
                }
                return Response.ok(new Viewable("/patientEdited.jsp")).build();
            } catch (WebApplicationException e) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("message", e.getResponse().getEntity());
                return Response.status(e.getResponse().getStatus()).entity(new Viewable("/errorPage.jsp", map)).build();
            }
    }

    /**
     * Edit a patient. Interface for software applications. The patient to edit is
     * determined from the given "editPatient" token.
     *
     * @param tokenId A valid "editPatient" token.
     * @param data    Input data as JSON object, keys are field names and values the
     *                respective field values.
     * @param request The injected HttpServletRequest.
     * @return An HTTP response as specified in the API documentation.
     */
    @Path("/tokenId/{tokenId}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response editPatientJSON(@PathParam("tokenId") String tokenId, String data,
        @Context HttpServletRequest request) {
      logger.debug("@PUT editPatientJSON");

      // Collect fields from input form
      try {
        JSONObject newFieldValuesJSON = new JSONObject(data);
        Map<String, String> newFieldValues = new HashMap<>();
        Iterator<?> i = newFieldValuesJSON.keys();
        while (i.hasNext()) {
          String fieldName = i.next().toString();
          if (newFieldValuesJSON.isNull(fieldName)) {
            newFieldValues.put(fieldName, "");
          } else {
            newFieldValues.put(fieldName, newFieldValuesJSON.get(fieldName).toString());
          }
        }
        EditPatientToken token = this.editPatient(tokenId, newFieldValues, request);

        //callback
        String callbackUrl = token.getCallbackUrl();
        if (!callbackUrl.isEmpty()
            && Servers.instance.hasPermissionByName(token.getParentServerName(),
            PermissionEnum.CALLBACK)) {
          callbackService.sendCallback(request, token.getId(), callbackUrl,
              Collections.singletonList(token.getPatientId()), null, null, null);
        }

        //redirect
        String redirect = token.getDataItemString("redirect");
        if (redirect != null && !redirect.isEmpty()) {
          UriTemplate redirectURITempl = new UriTemplate(token.getDataItemString("redirect"));
          List<String> templateVariables = redirectURITempl.getTemplateVariables();
          if (templateVariables.contains("tokenId")) {
            return new RedirectBuilder().setTokenId(token.getId()).setTemplateURI(redirectURITempl)
                .build()
                .execute();
          }
        }
        return Response.status(Status.NO_CONTENT).build();
      } catch (JSONException e) {
        logger.error("Couldn't parse json in PatientResource.editPatientJSON", e);
        throw new InvalidJSONException("Couldn't parse editPatient JSON");
      }
    }

    /**
     * Handles requests to edit a patient (i.e. change IDAT fields). Methods for
     * specific media types should delegate all processing apart from converting the
     * input (e.g. form fields) to this function, including error handling for
     * invalid tokens etc.
     *
     * @param tokenId        Id of a valid editPatient token.
     * @param newFieldValues Field values to set. Fields that do not appear as map
     *                       keys are left as they are. In order to delete a field
     *                       value, provide an empty string.
     * @param request        The injected HttpServletRequest.
     * @return The token that is as authorization the patient. Used for retreiving
     * the redirect URL afterwards.
     */
    public synchronized EditPatientToken editPatient(String tokenId, Map<String, String> newFieldValues,
                                                      HttpServletRequest request) {

        Token t = Servers.instance.getTokenByTid(tokenId);
        EditPatientToken tt;
        if (t == null || !"editPatient".equals(t.getType())) {
            logger.info(() -> "Token with id " + tokenId + " "
                    + (t == null ? "is unknown." : ("has wrong type '" + t.getType() + "'")));
            throw new InvalidTokenException("Please supply a valid 'editPatient' token.", Status.UNAUTHORIZED);
        }
        // synchronize on token
        synchronized (t) {
            /*
             * Get token again and check if it still exist. This prevents the following race
             * condition: 1. Thread A gets token t and enters synchronized block 2. Thread B
             * also gets token t, now waits for A to exit the synchronized block 3. Thread A
             * deletes t and exits synchronized block 4. Thread B enters synchronized block
             * with invalid token
             */
            tt = (EditPatientToken) Servers.instance.getTokenByTid(tokenId);
            if (tt == null) {
                String infoLog = "Token with ID " + tokenId
                        + " is invalid. It was invalidated by a concurrent request or the session timed out during this request.";
                logger.info(infoLog);
                throw new WebApplicationException(Response.status(Status.UNAUTHORIZED)
                        .entity("Please supply a valid 'editPatient' token.").build());
            }

            // Form fields (union of fields and ids)
            Set<String> allowedFormFields = tt.getFields();
            if (tt.getIds() != null) {
                if (allowedFormFields != null) {
                    allowedFormFields.addAll(tt.getIds());
                } else {
                    allowedFormFields = tt.getIds();
                }
            }

            // Check that the caller is allowed to change the provided fields or ids
            if (allowedFormFields != null) {
                for (String fieldName : newFieldValues.keySet()) {
                    if (!fieldName.equals("sureness")){
                        if (!allowedFormFields.contains(fieldName)) {
                            if (IDGeneratorFactory.instance.getExternalIdTypes().contains(fieldName)) {
                                throw new UnauthorizedException(
                                    "No authorization to edit external id " + fieldName + " with this token.");
                            } else {
                                throw new UnauthorizedException(
                                    "No authorization to edit field " + fieldName + " with this token.");
                            }
                        }
                    }
                }
            }

            boolean sureness = newFieldValues.get("sureness") != null || Boolean.parseBoolean(newFieldValues.get("sureness"));


            PatientBackend.getDefaultInstance().editPatient(tt.getPatientId(), newFieldValues,
                sureness, tokenId);
        } // end of synchronized block

        return tt;
    }

    /**
     * Delete a patient
     *
     * @param idType              ID type of an ID of the patient to delete.
     * @param idString            ID string of an ID of the patient to delete.
     * @param withDuplicatesParam Whether to delete duplicates of the given patient.
     * @param request             The injected HttpServletRequest.
     * @return A response according to the API documentation.
     */
    @Deprecated
    @Path("{tokenId}/{idType}/{idString}")
    @DELETE
    public Response deletePatient(@PathParam("tokenId") String tokenId, @PathParam("idType") String idType,
                                  @PathParam("idString") String idString, @QueryParam("withDuplicates") String withDuplicatesParam,
                                  @Context HttpServletRequest request) {
        logger.debug("@DELETE deletePatientIDAT request");

        Token token = Servers.instance.getTokenByTid(tokenId);

        if (token == null) {
            logger.info("No token with id {} found", tokenId);
            throw new InvalidTokenException("Please supply a valid 'deletePatient' token.", Status.UNAUTHORIZED);
        }
        token.checkTokenType("deletePatient");
        boolean withDuplicates = Boolean.parseBoolean(withDuplicatesParam);

        ID id = IDGeneratorFactory.instance.buildId(idType, idString);
        if(!Persistor.instance.patientExists(id)) {
            throw new InvalidTokenException(
                "No patient found with provided " + id.getType() + " '"
                        + id.getIdString() + "'!");
        }

        return deletePatientIDAT(token, id, withDuplicates, request);
    }

    /**
     * Delete a patient
     *
     * @param tokenId             Id of a valid deletePatient token
     * @param request             The injected HttpServletRequest.
     * @return A response according to the API documentation.
     */
    @DELETE
    public Response deletePatient(@QueryParam("tokenId") String tokenId,
                                  @Context HttpServletRequest request) {
        logger.debug("@DELETE deletePatientIDAT request");

        Token token = Servers.instance.getTokenByTid(tokenId);

        if (token == null) {
            logger.info("No token with id {} found", tokenId);
            throw new InvalidTokenException("Please supply a valid 'deletePatient' token.", Status.UNAUTHORIZED);
        }
        token.checkTokenType("deletePatient");

        Map<String,String> patientId = (Map<String, String>) token.getDataItemMap("patientId");
        String idType = patientId.get("idType");
        String idString = patientId.get("idString");
        ID id = IDGeneratorFactory.instance.buildId(idType, idString);

        boolean withDuplicates = Boolean.TRUE.equals(token.getData().get("withDuplicates"));

        if (!Persistor.instance.patientExists(id)) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("No patient found")
                    .build();
        }

        return deletePatientIDAT(token, id, withDuplicates, request);
    }

    /**
     * !Experimental! find patient with similarity scores and ids
     * @param tokenId token id
     * @param form patient idat
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("checkMatch/{tokenId}")
    public Response getBestMatch(@Context HttpServletRequest request, @PathParam("tokenId")
    String tokenId, MultivaluedMap<String, String> form) throws JSONException {
      logger.debug("checkMatch tokenId: {}", tokenId);

      Token token = Servers.instance.findToken(tokenId, Token.class, Token.TYPE_CHECK_MATCH);

      // read input fields and external ids from FORM
      Map<String, String> inputFields = new HashMap<>();
      Map<String, List<String>> externalIds = new LinkedHashMap<>();
      extractFieldsAndExternalIds(form, inputFields, externalIds);

      MatchResult matchResult = PatientBackend.getDefaultInstance().findMatch(inputFields, externalIds);
      logger.info("CheckMatch/Bestmatch score: {}", matchResult.getBestMatchedWeight());

      List<Double> similarityScores = Collections.singletonList(matchResult.getBestMatchedWeight());
      JSONArray jsonArray = new JSONArray();
      JSONObject jsonPatientObject = new JSONObject().put("similarityScore", similarityScores.get(0));

      Set<String> idTypes = token.readIdTypesFromJsonArray("idTypes");
      List<ID> requestedIds = new ArrayList<>();
      if (matchResult.getBestMatchedPatient() != null) {
        // get requested id from patient
        for (String requestedIdType : idTypes) {
          if (IDGeneratorFactory.instance.getTransientIdTypes().contains(requestedIdType)) {
            Optional.ofNullable(matchResult.getBestMatchedPatient().getTransientId(requestedIdType))
                .ifPresent(requestedIds::add);
          } else if (IDGeneratorFactory.instance.isIdTypeExist(requestedIdType)) {
            Optional.ofNullable(matchResult.getBestMatchedPatient().getId(requestedIdType))
                .ifPresent(requestedIds::add);
          } else if (IDGeneratorFactory.instance.isAssociatedIdTypeExist(requestedIdType)) {
            for (AssociatedIds associatedIds : matchResult.getBestMatchedPatient()
                .getAssociatedIdsList()) {
              ID associatedId = associatedIds.getId(requestedIdType);
              if (associatedId == null) {
                associatedId = associatedIds.getTransientId(requestedIdType);
              }
              Optional.ofNullable(associatedId).ifPresent(requestedIds::add);
            }
          }
        }
        // filter result ids and serialize to json object
        List<ID> resultIds = callbackService.filterIds(requestedIds, token.readIdTypesFromJsonArray("resultIds"));
        for (ID resultId : resultIds) {
          if(IDGeneratorFactory.instance.isIdTypeExist(resultId.getType())) {
            jsonPatientObject.put(resultId.getType(), resultId.getEncryptedIdStringFirst());
          } else { //add associated ids
            if(!jsonPatientObject.has(resultId.getType()))
              jsonPatientObject.append(resultId.getType(), resultId.getEncryptedIdStringFirst());
            else
              jsonPatientObject.getJSONArray(resultId.getType()).put(resultId.getEncryptedIdStringFirst());
          }
        }
      }

      jsonArray.put(jsonPatientObject);

      // needs to send weight and token
      String callback = token.getDataItemString("callback");
      if (callback != null && !callback.isEmpty()) {
        callbackService.sendCallback(request, token.getId(), callback, requestedIds,
            token.readIdTypesFromJsonArray("callbackResultIds"), null, similarityScores);
      }
      // needs to send weight and token
      String redirect = token.getDataItemString("redirect");
      if (redirect != null && redirect.length() > 0) {
        UriTemplate redirectURITempl = new UriTemplate(token.getDataItemString("redirect"));
        List<String> templateVariables = redirectURITempl.getTemplateVariables();
        if (templateVariables.contains("tokenId")) {
          return new RedirectBuilder().setTokenId(token.getId())
              .setSimilarityScores(similarityScores).setTemplateURI(redirectURITempl).build()
              .execute();
        }
      }

      return Response
          .status(Status.OK)
          .entity(jsonArray.toString())
          .build();
    }

    /* Utils */

  public static IDRequest addNewPatient(MultivaluedMap<String, String> form,
      Map<String, String> fieldsFromToken, Map<String, String> externalIdsFromToken,
      RequestedIdTypes requestedIdTypes, String tokeId) {

    // read input fields and external ids from FORM and Token
    Map<String, String> fields = new HashMap<>(fieldsFromToken);
    Map<String, List<String>> externalIds = new LinkedHashMap<>();

    extractFieldsAndExternalIds(form, fields, externalIds);

    // add ids from Token
    externalIdsFromToken.forEach((k, v) -> {
      List<String> oldValues = externalIds.get(k);
      if (oldValues == null) {
        externalIds.put(k, Arrays.asList(v));
      } else {
        oldValues.add(v);
      }
    });

    // read sureness flag
    boolean sureness =
        form.getFirst("sureness") != null || Boolean.parseBoolean(form.getFirst("sureness"));
    boolean ignoreInvalidIDAT =
        form.getFirst("ignoreInvalidIDAT") != null || Boolean.parseBoolean(form.getFirst("ignoreInvalidIDAT"));
    return PatientBackend.getDefaultInstance().createAndPersistPatient(fields, externalIds, requestedIdTypes,
        sureness, ignoreInvalidIDAT, tokeId);
  }

  private static void extractFieldsAndExternalIds(MultivaluedMap<String, String> form,
      Map<String, String> inputFields, Map<String, List<String>> externalIds) {

    List<String> externalAssociatedIdTypes = IDGeneratorFactory.instance.getExternalAssociatedIdTypes();
    form.entrySet().stream()
        .filter(e -> e.getValue() != null && !e.getValue().isEmpty())
        .forEach(e -> {
          //extract external ids
          if (IDGeneratorFactory.instance.getExternalIdTypes().contains(e.getKey())
              || externalAssociatedIdTypes.contains(e.getKey())) {
            List<String> oldValues = externalIds.get(e.getKey());
            if (oldValues == null) {
              externalIds.put(e.getKey(), new ArrayList<>(e.getValue()));
            } else {
              oldValues.addAll(e.getValue());
            }
          }
          // extract input fields
          if (Config.instance.getFieldKeys().contains(e.getKey())) {
            inputFields.put(e.getKey(), e.getValue().get(0));
          }
        });
  }

  /**
   * find if add patient token exist otherwise create a new one if started in debug mode
   * @param tokenId token id
   * @return add patient token
   * @throws InvalidTokenException if no token with the given id found
   */
  private AddPatientToken getAddPatientToken(String tokenId) {
    Token token = Servers.instance.getTokenByTid(tokenId);
    // Try to read token from session.
    if (token == null) {
      // If no token found and debug mode is on, create token, otherwise fail
      if (!Config.instance.debugIsOn()) {
        logger.error("No token with id {} found", tokenId);
        throw new InvalidTokenException("Please supply a valid 'addPatient' token.",
            Status.UNAUTHORIZED);
      }
      AddPatientToken addPatientToken = new AddPatientToken();
      Servers.instance.registerToken(getDebugSession().getId(), addPatientToken, "127.0.0.1");
      return addPatientToken;
    } else if (!(token instanceof AddPatientToken)) { // correct token type?
      logger.error("Token {} is not of type 'addPatient' but '{}'", token.getId(), token.getType());
      throw new InvalidTokenException("Please supply a valid 'addPatient' token.",
          Status.UNAUTHORIZED);
    }
    return (AddPatientToken) token;
  }

  /**
   * Get a session for use in debug mode.
   *
   * @return The debug session.
   */
  private Session getDebugSession() {
    if (debugSession == null || Servers.instance.getSession(debugSession.getId()) == null) {
      debugSession = Servers.instance.newSession("");
      try {
        debugSession.setURI(new URI("debug"));
      } catch (URISyntaxException e) {
        throw new WebApplicationException(e);
      }
    }
    return debugSession;
  }

    private void performAuditTrail(Patient patient, String tokenId,String action)
    {
        patient.getIds().forEach(id -> Persistor.instance.persistAuditTrailRecords(
                PatientBackend.getDefaultInstance().buildAuditTrailRecord(
                        tokenId,
                        id.getIdString(),
                        id.getType(),
                        action,
                        patient.toString(),
                        null)
        ));
    }

  private Response deletePatientIDAT(Token token, ID id, boolean withDuplicates,
      HttpServletRequest request) {
    if (withDuplicates) {
      List<Patient> deletedPatients = Persistor.instance.deletePatientWithDuplicates(id);
      if (Config.instance.auditTrailIsOn()) {
        deletedPatients.forEach(p -> performAuditTrail(p, token.getId(), "delete"));
      }
    } else {
      Patient deletedPatient = Persistor.instance.deletePatient(id);
      if (Config.instance.auditTrailIsOn()) {
        performAuditTrail(deletedPatient, token.getId(), "delete");
      }
    }

    //callback
    String callback = token.getDataItemString("callback");
    if (callback != null && !callback.isEmpty()
        && Servers.instance.hasPermissionByName(token.getParentServerName(),
        PermissionEnum.CALLBACK)) {
      callbackService.sendCallback(request, token.getId(), callback,
          Collections.singletonList(id), null,null, null);
    }

    //redirect
    String redirect = token.getDataItemString("redirect");
    if (redirect != null && !redirect.isEmpty()) {
      UriTemplate redirectURITempl = new UriTemplate(token.getDataItemString("redirect"));
      List<String> templateVariables = redirectURITempl.getTemplateVariables();
      if (templateVariables.contains("tokenId")) {
        return new RedirectBuilder().setTokenId(token.getId()).setTemplateURI(redirectURITempl)
            .build()
            .execute();
      }
    }
    return Response.status(Status.NO_CONTENT).build();
  }
}
