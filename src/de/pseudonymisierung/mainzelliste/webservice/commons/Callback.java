package de.pseudonymisierung.mainzelliste.webservice.commons;

import de.pseudonymisierung.mainzelliste.util.gson.Exclude;
import de.pseudonymisierung.mainzelliste.webservice.commons.callback.CallbackId;
import de.pseudonymisierung.mainzelliste.webservice.commons.callback.CallbackPatient;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public record Callback(
    @Exclude
    String url,
    @Exclude
    Map<String, String> headers,
    String tokenId,
    String id,
    List<CallbackId> ids,
    Map<String, String> fields,
    List<CallbackPatient> patients,
    Collection<Double> similarityScores
) {

}
