package de.pseudonymisierung.mainzelliste.webservice.commons;

import de.pseudonymisierung.mainzelliste.Field;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.Servers.ApiVersion;
import de.pseudonymisierung.mainzelliste.api.ReadPatientResponse;
import de.pseudonymisierung.mainzelliste.webservice.commons.callback.CallbackId;
import de.pseudonymisierung.mainzelliste.webservice.commons.callback.CallbackPatient;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

public class CallbackFactory {

  private CallbackFactory() {
  }

  public static Callback createCallback(ApiVersion apiVersion, String url, String tokenId,
      Collection<ID> ids, Map<String, Field<?>> fields, Collection<ReadPatientResponse> patients,
      Collection<Double> similarityScores) {
    return new Callback(url, new HashMap<>(), cleanTokenId(tokenId), convertId(apiVersion, ids), convertIds(ids),
        convertFields(fields), convertPatients(patients), cleanSimilarityScores(similarityScores));
  }

  public static Callback createCallback(ApiVersion apiVersion, String url,
      Map<String, String> headers, String tokenId, Collection<ID> ids,
      Map<String, Field<?>> fields, List<ReadPatientResponse> patients, Collection<Double> similarityScores) {
    return new Callback(url, headers, cleanTokenId(tokenId), convertId(apiVersion, ids), convertIds(ids),
        convertFields(fields), convertPatients(patients), cleanSimilarityScores(similarityScores));
  }

  private static String cleanTokenId(String tokenId) {
    return StringUtils.trimToNull(tokenId);
  }

  private static String convertId(ApiVersion apiVersion, Collection<ID> ids) {
    if (apiVersion.majorVersion == 1) {
      if (!CollectionUtils.isEmpty(ids) && ids.size() > 1) {
        throw new WebApplicationException(
            Response.status(Response.Status.BAD_REQUEST)
                .entity("Selected API version 1.0 permits only one ID in callback, " +
                    "but several were requested. Set mainzellisteApiVersion to a " +
                    "value >= 2.0 or request only one ID type in token.")
                .build());
      }
      return ids.iterator().next().getEncryptedIdStringFirst();
    } else {
      return null;
    }
  }

  private static List<CallbackId> convertIds(Collection<ID> ids) {
    return CollectionUtils.isEmpty(ids) ? null : ids.stream()
        .map(id -> new CallbackId(id.getType(), id.getEncryptedIdStringFirst(), id.isTentative()))
        .toList();
  }

  private static Map<String, String> convertFields(Map<String, Field<?>> fields) {
    return MapUtils.isEmpty(fields) ? null : fields.entrySet().stream()
        .collect(Collectors.toMap(Entry::getKey, e -> e.getValue().toString()));
  }

  private static List<CallbackPatient> convertPatients(Collection<ReadPatientResponse> patients) {
    return null;
    //TODO wasn't supported before
//    return patients == null || patients.length() == 0 ? null : null;
//    if (this.patients != null && (this.ids != null || this.fields != null)) {
//      throw new IllegalArgumentException(
//          "Can't handle MainzellisteCallback that returns patients and either ids or fields. Return only patients or returnIds and returnFields");
//    }
//    if (this.patients == null) {
//      this.patients = new JSONArray();
//    }
//    for (int i = 0; i < patients.length(); i++) {
//      JSONObject patient = patients.optJSONObject(i);
//      if (patient != null) {
//        this.patients.put(patient);
//      }
//    }
  }

  private static Collection<Double> cleanSimilarityScores(Collection<Double> similarityScores) {
    return CollectionUtils.isEmpty(similarityScores) ? null : similarityScores;
  }
}
