/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.webservice.commons;

import de.pseudonymisierung.mainzelliste.configuration.permission.ExtendedPermissions;
import de.pseudonymisierung.mainzelliste.exceptions.UnauthorizedException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ExtendedPermissionUtils {

  private ExtendedPermissionUtils() {}

  public static void checkPermission(Map<String, ?> tokenData, ExtendedPermissions extendedPermissions,
      boolean isCaseSensitive) {

    tokenData.remove("type");
    // deserialize token data
    List<TokenDataElement> requestedPermissions = new ArrayList<>();
    deserializeTokenDataElements("", tokenData, isCaseSensitive, requestedPermissions);

    //check permissions
    checkPermissions(requestedPermissions, extendedPermissions);
  }

  private static void checkPermissions(List<TokenDataElement> requestedPermissions,
      ExtendedPermissions serverPermissions) {
    for (TokenDataElement requestedPermission : requestedPermissions) {
      if (!serverPermissions.checkPermission(requestedPermission.getJsonPath(),
          requestedPermission.getValue())) {
        throw new UnauthorizedException("Your permissions do not allow the requested access."
            + requestedPermission.getJsonPath() + " : " + requestedPermission.getValue() + " is "
            + "not allowed to request");
      }
    }
  }

  protected static void deserializeTokenDataElements(String jsonPath, Object value,
      boolean isCaseSensitive, List<TokenDataElement> result) {
    if (value instanceof List<?> valueList) {
      for (Object currentValue : valueList) {
        deserializeTokenDataElements(jsonPath, currentValue, isCaseSensitive, result);
      }
    } else if (value instanceof Map<?, ?> jsonObject) {
      for (Entry<?, ?> jsonProperty : jsonObject.entrySet()) {
        deserializeTokenDataElements(jsonPath + (jsonPath.isEmpty()?"":".") + jsonProperty.getKey(),
            jsonProperty.getValue(), isCaseSensitive, result);
      }
    } else if (value instanceof String || value instanceof Integer
        || value instanceof Long || value instanceof Float
        || value instanceof Double || value instanceof Boolean) {
      result.add(
          new TokenDataElement(jsonPath, String.valueOf(value), !isCaseSensitive));
    }
  }

  protected static class TokenDataElement {

    private final String jsonPath;
    private final String value;

    public TokenDataElement(String jsonPath, String value, boolean lowerCase) {
      this.jsonPath = lowerCase ? jsonPath.toLowerCase() : jsonPath;
      this.value = lowerCase ? value.toLowerCase() : value;
    }

    public String getJsonPath() {
      return jsonPath;
    }

    public String getValue() {
      return value;
    }
  }
}
