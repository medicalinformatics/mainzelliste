package de.pseudonymisierung.mainzelliste.webservice.commons.callback;

import java.util.Collection;
import java.util.Map;

public record CallbackPatient(Collection<CallbackId> ids, Map<String, String> fields) {
}
