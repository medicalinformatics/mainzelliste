package de.pseudonymisierung.mainzelliste.webservice.commons.callback;

public record CallbackId(String idType, String  idString, boolean tentative) {}
