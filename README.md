![Mainzelliste Logo](./doc/images/mainzelliste-logo-650.png)

Mainzelliste is a web-based first-level pseudonymization service. It allows for the creation of personal identifiers (PID) from identifying attributes (IDAT), and thanks to the record linkage functionality, this is even possible with poor quality identifying data. The functions are available through a RESTful web interface.

Further information and documentation on Mainzelliste can be found on the [project web page of the University Medical Center Mainz](http://www.unimedizin-mainz.de/imbei/informatik/ag-verbundforschung/mainzelliste.html?L=1).

In order to receive up-to-date information on the project, you can register to be on our [mailing list](https://lists.uni-mainz.de/sympa/subscribe/mainzelliste).

The following article describes the underlying concepts of Mainzelliste and the motivation for its development. Please cite it when referring to Mainzelliste in publications:

> Lablans M, Borg A, Ückert F. A RESTful interface to pseudonymization services in modern web applications. BMC Medical Informatics and Decision Making 2015, 15:2. <http://www.biomedcentral.com/1472-6947/15/2>.

Java developers should have a look at [Mainzelliste.Client](https://github.com/medicalinformatics/mainzelliste-client), a library that handles the HTTP calls necessary for using Mainzelliste in a client application.

Information about the newman tests can be found [here](./newman.md). 

## Getting Started
[These tutorials](./doc/getting-started.md) let you discover Mainzelliste basic functionalities. 
To get deeper insights on the REST APIs please refer to 
the [documentation](https://bitbucket.org/medicalinformatics/mainzelliste/wiki/2.%20Mainzelliste%20Documentation.md#!mainzelliste-documentation)
and the [newman tests](./newman.md).

## Installation
You can deploy Mainzelliste using Docker containers on Windows, macOS and linux distributions. 
More information about using the official docker image can be found [here](./docker.md).

## Performance
Since Mainzelliste v1.11 we achieved a significant milestone which provides an enhanced performance.
Just check out the [latest benchmark test](./doc/BENCHMARK.md)

## Client Applications
A beta Version of the Mainzelliste User Interface (UI) for the patient list’s administrator is 
available in [GitHub](https://github.com/medicalinformatics/mainzelliste-gui/)

![Mainzelliste UI Screenshot](./doc/images/mainzelliste-ui-screenshot-browser.png)

## References

Mainzelliste is used in various medical joint research projects, including:

- [German Mukoviszidose Register](https://onlinelibrary.wiley.com/doi/full/10.1002/pds.70076)
- European chILD-EU register ([Ethics/Data Safety](http://www.klinikum.uni-muenchen.de/Child-EU/en/child-eu-register/register/ethics_data_safety/index.html))
- [German Cancer Consortium](https://ccp-it.dktk.dkfz.de/)
- Cluster for Individualized Immune Intervention (Ci3) ([Meeting abstract on IT concept](http://www.egms.de/static/de/meetings/gmds2014/14gmds106.shtml))
- Studies conducted by the [LASER group](http://www.la-ser.com/)
- The [MIRACUM consortium](http://www.miracum.org/miracolix-tools)
- The [NEW ESID online database network](https://academic.oup.com/bioinformatics/advance-article/doi/10.1093/bioinformatics/btz525/5526873)
- The [VHL-Register](https://vhl-register.org/files/VHL-Vorgehensweise.pdf)
- [CURENet](https://cure-net.de/index.php/de/aktuelles) Network researching congenital urorectal malformations
- [asthesis longlife - das BQS Register](https://www.bqs.de/leistungen/wissenschaftliche-register/18-leistungen/47-register-basis-modul-asthesis)
- The [ParaReg](https://www.dmgp-kongress.de/fileadmin/congress/media/dmgp2019/druckelemente/DMGP2019_Abstractband.pdf) Registry for lifelong monitoring of paraplegic patients
- The [DISCOVER](https://www.sciencedirect.com/science/article/pii/S2214782921000750) study
- The [oregis](https://oregis.de) registry of the German scientific association of ophthalmology ([Deutsche Ophthalmologische Gesellschaft](https://www.dog.org))
- the [RESCUED](https://link.springer.com/article/10.1007/s00392-024-02460-z) registry for sudden cardiac death in the young in Germany
- National Register for Rare Diseases ([NARSE](https://www.narse.de/fileadmin/narse/2024-01-13_NARSE_Patienteninfo_Einwilligung_V1.3.pdf))
- Amputation Registry Germany ([AMP Registry](https://link.springer.com/article/10.1007/s00113-025-01539-0))

Another important use case is pseudonymization in central biobanks, for example:

- [Comprehensive Biomaterial Bank Marburg](http://www.cbbmr.de/informationen-allgemein/allgemeines.html)
- [Hannover Unified Biobank](http://www.pg-ss.imi.uni-erlangen.de/SiteCollectionDocuments/Hannover_HUB_IT_Kersting.pdf)
- The biobank network of the Ruhr University Bochum (BioNetRUB)

Apart from long-lived research infrastructures, Mainzelliste also serves in data analysis as a tool for record linkage:

- Eisenberg et al., Time-dependent prediction of mortality and cytomegalovirus reactivation after allogeneic hematopoietic cell transplantation using machine learning. ([Preprint](https://doi.org/10.1101/2021.09.14.21263446))
- The [KOBRA](https://doi.org/10.18420/inf2020_37) project on learning-based, automatic configuration of business rules for duplicate detection
- Lehmann et al. Clinical effectiveness of patient-oriented depression feedback in primary care: The empirical method of the GET.FEEDBACK.GP multicenter randomized controlled trial. Contemporary Clinical Trials 2021. ([Link](https://doi.org/10.1016/j.cct.2021.106562))

The Mainzelliste API has been implemented in the following projects and software products:

- [OSSE – Open Source Registry System for Rare Diseases in the EU](http://osse-register.de)
- [OpenClinica](https://openclinica.com/) (see [presentation on integrating Mainzelliste and other software](https://community.openclinica.com/sites/fileuploads/akaza/cms-community/Tomas%20Skripcak%20-%20Lessons%20learned.pdf))
- [secuTrial](http://secutrial.com) (see [modules description](http://www.secutrial.com/module/))
- [Semantic Clinical Registry System for Rare Diseases](http://aksw.org/Projects/SCRS.html)
- [MOSAIC](https://mosaic-greifswald.de/) (the external interface of the "Trusted Third Party Dispatcher" is oriented towards the token-based concept of the Mainzelliste API, see [Bialke et al., J Transl Med. 2015, 13:176](http://www.translational-medicine.com/content/13/1/176))
- [German Center for Cardiovascular Disease (DZHK)](https://dzhk.de) (see MOSAIC and the [data protection concept](https://dzhk.de/fileadmin/user_upload/Datenschutzkonzept_des_DZHK.pdf))
- [German National Cohort](https://nako.de) (see MOSAIC and the [data protection concept](https://nako.de/wp-content/uploads/2015/07/Treuhandstellenkonzept.pdf))
- [Electronic data capture system by Fraunhofer FOKUS](https://cdn3.scrivito.com/fokus/57a537e2ec27cb7b/0a3a0655dcc079f58890e39dbdca4781/E-HEALTH_Standards_PB_03-2015_v03.pdf)
- [CentraXX](http://www.kairos.de/produkte/centraxx/) by Kairos GmbH 
- Platform for medical research by [Climedo Health GmbH](https://www.climedo.de/)
- The [NEW ESID online database network](https://academic.oup.com/bioinformatics/advance-article/doi/10.1093/bioinformatics/btz525/5526873)
- [MaganaMed](https://maganamed.com/de/module)'s trial management software, using its "Mainzelliste Module" for separating patient data from clinical data.
- The [MainzelHandler](https://pubmed.ncbi.nlm.nih.gov/34042740/) open-source library by Daniel Preciado-Marquez et al. for integrating the Mainzelliste via its REST API in Web Applications.
- The [Karnak](https://github.com/OsiriX-Foundation/karnak) open-source DICOM gateway for data de-identification and DICOM attribute normalization by the OsiriX Foundation.
- [Elpax](https://elpax.de/) by Axaris Software and the Gesundes Kinzigtal GmbH.
- The [DISERDIS](https://www.researchgate.net/publication/344635918_Pseudonymization_of_Rare_Diseases_Patients_in_a_Diagnosis_Support_System_based_on_Cross-Institutional_Clinical_Data) diagnosis support system within the MIRACUM consortium
- The [SOLKID-GNR](https://www.gesundheitsforschung-bmbf.de/de/solkid-gnr-die-sicherheit-des-lebendnierenspenders-das-deutsche-nationale-register-9015.php), where the Mainzelliste is used as a central pseudonymisation service for [Redcap](https://www.project-redcap.org). see [Linking EMR Data to REDCap: Implementation in the SOLKID Register](https://ebooks.iospress.nl/doi/10.3233/SHTI220434)

We have compiled this list from the results of public search engines. If you use the Mainzelliste or its API, we would be happy to include your project in this list. Please don't hestitate to [contact us](mailto:info@mainzelliste.de).

## Contributing

We would love to include your useful changes to the Mainzelliste code in a future official release. See the related [Wiki page](https://bitbucket.org/medicalinformatics/mainzelliste/wiki/Contributing) for further information on contributing code.

## Release notes

All notable changes to the mainzelliste project you can see in the [changelog](./changelog.md)

## Contributions
As an open source project, Mainzelliste profits from contributions from the research community. We would like to thank the following colleagues for their code contributions (sorted by name in ascending order):

- Andreas Borg, Universitätsmedizin Mainz
- Benjamin Gathmann, Universitätsklinikum Freiburg
- Christian Koch, DKFZ-Heidelberg
- Christian Syska, Routine Health GmbH
- Daniel Menzel, Universität der Bundeswehr München
- Daniel Volk, Universitätsmedizin Mainz
- David Croft, DKFZ-Heidelberg
- Dirk Langner, Universitätsmedizin Greifswald
- Florens Rohde, Universität Leipzig
- Florian Stampe, DKFZ-Heidelberg
- Galina Tremper, DKFZ-Heidelberg
- Jens Schwanke, Universitätsmedizin Göttingen
- Cornelius Knopp, Universitätsmedizin Göttingen 
- Marcel Parciak, Universitätsmedizin Göttingen
- Marlena Meyer, DKFZ-Heidelberg / Universitätsklinikum Mannheim
- Martin Lablans, DKFZ-Heidelberg
- Matthias Lemmer, Universität Marburg
- Meet Bhatt, DKFZ-Heidelberg / Universitätsklinikum Mannheim
- Moanes Ben Amor, DKFZ-Heidelberg
- Maximilian Ataian, Universitätsmedizin Mainz
- Project FP7-305653-chILD-EU
- Stephan Rusch, Universitätsklinikum Freiburg
- Torben Brenner, DKFZ-Heidelberg
- Ziad Sehili, Universität Leipzig

## License

Copyright 2013 - 2024 

Licenced under GNU Affero General Public License Version 3
