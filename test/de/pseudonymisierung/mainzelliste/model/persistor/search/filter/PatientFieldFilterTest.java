package de.pseudonymisierung.mainzelliste.model.persistor.search.filter;

import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.mapper.PatientFieldSearchConditionField;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PatientFieldFilterTest {

  private final Map<String, PatientFieldSearchConditionField> patientFieldSearchConditionFieldMap = new HashMap<>();
  private MultivaluedMap<String, String> fieldMap;

  @BeforeClass
  public void init(){
    Set.of("name", "lastname", "city").forEach(
        k -> this.patientFieldSearchConditionFieldMap.put(k, new PatientFieldSearchConditionField(k))
    );

    fieldMap = new MultivaluedHashMap<>();
    fieldMap.putSingle("name", "max");
    fieldMap.putSingle("lastname", "muster");
    fieldMap.putSingle("city", "heidelberg");
  }

  @Test
  public void testBuildPostgresSearchConditions() {
    List<Object> queryParameter = new ArrayList<>();
    Assert.assertEquals(new PatientFieldFilter(fieldMap, patientFieldSearchConditionFieldMap)
            .buildSearchConditions(true, queryParameter),
        "(p.inputfieldsstring->'city'->>'value' ilike ?1 and "
            + "p.inputfieldsstring->'name'->>'value' ilike ?2 and "
            + "p.inputfieldsstring->'lastname'->>'value' ilike ?3)");
    Assert.assertEquals(queryParameter, fieldMap.values().stream().map( l -> "%" + l.get(0) + "%").toList());
  }

  @Test
  public void testBuildMySqlSearchConditions() {
    List<Object> queryParameter = new ArrayList<>();
    Assert.assertEquals(new PatientFieldFilter(fieldMap, patientFieldSearchConditionFieldMap)
            .buildSearchConditions(false, queryParameter),
        "(LOWER(p.inputfieldsstring->'$.city.value') like ?1 and "
            + "LOWER(p.inputfieldsstring->'$.name.value') like ?2 and "
            + "LOWER(p.inputfieldsstring->'$.lastname.value') like ?3)");
    Assert.assertEquals(queryParameter, fieldMap.values().stream().map( l -> "%" + l.get(0) + "%").toList());
  }
}
