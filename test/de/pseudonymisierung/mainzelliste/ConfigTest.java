package de.pseudonymisierung.mainzelliste;

import de.pseudonymisierung.mainzelliste.configuration.CallbackConfig;
import de.pseudonymisierung.mainzelliste.configuration.ConfigReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConfigTest {

  @Test
  public void test_readIDGeneratorsCallbacks() {
    Properties properties = new Properties();
    properties.put("idgenerator.projectId.generator", "PIDGenerator");
    properties.put("idgenerator.projectId.callback.0.url", "http://magicpl-umm/paths");
    properties.put("idgenerator.projectId.callback.0.resultIds", "intid, extid");
    properties.put("idgenerator.projectId.callback.0.resultFields", "name, lastname");
    properties.put("idgenerator.projectId.callback.0.headers.apiKey", "changeMe");
    properties.put("idgenerator.projectId.callback.1.url", "http://ml-umm/paths");
    properties.put("idgenerator.projectId.callback.1.resultIds", "intid, extid");
    properties.put("idgenerator.projectId.callback.1.resultFields", "name, lastname");
    properties.put("idgenerator.projectId.callback.1.headers.apiKey", "changeMe");
    properties.put("idgenerator.projectId.callback.1.headers.content", "json");
    properties.put("idgenerators", "intid, extid, projectId");
    ConfigReader reader = new ConfigReader(properties);
    Map<String, List<CallbackConfig>> idGeneratorsCallbacks = reader.
        readIDGeneratorsCallbacks(Arrays.asList("name", "lastname"));
    List<CallbackConfig> configList = idGeneratorsCallbacks.get("projectId");
    Assert.assertEquals(configList.get(0).url(), properties.get("idgenerator.projectId.callback.0.url"));
    Assert.assertEquals(configList.get(0).idTypes().size(), 2);
    Assert.assertEquals(configList.get(0).fields().size(), 2);
    Assert.assertEquals(configList.get(0).headers().get("apiKey"), properties.get("idgenerator.projectId.callback.0.headers.apiKey"));
    Assert.assertEquals(configList.get(1).url(), properties.get("idgenerator.projectId.callback.1.url"));
    Assert.assertEquals(configList.get(1).idTypes().size(), 2);
    Assert.assertEquals(configList.get(1).fields().size(), 2);
    Assert.assertEquals(configList.get(1).headers().get("apiKey"), properties.get("idgenerator.projectId.callback.1.headers.apiKey"));
    Assert.assertEquals(configList.get(1).headers().get("content"), properties.get("idgenerator.projectId.callback.1.headers.content"));
  }
}
