package de.pseudonymisierung.mainzelliste.mockup;

import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidIDException;

public class MockUpID extends ID {

  public MockUpID(String type, String value) {
    this.type = type;
    this.idString = value;
  }

  @Override
  public String getIdString() {
    return idString;
  }

  @Override
  protected void setIdString(String id) throws InvalidIDException {
    this.idString = id;
  }

  public String getEncryptedIdStringFirst() {
    return this.idString;
  }
}
