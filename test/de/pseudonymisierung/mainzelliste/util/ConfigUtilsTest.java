package de.pseudonymisierung.mainzelliste.util;

import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConfigUtilsTest {

  @Test
  public void testGetVariableSubProperties() {
    Properties properties = new Properties();
    properties.put("headers.apiKey", "changeMe");
    properties.put("headers.content", "json");
    Map<String, String> result = ConfigUtils.getVariableSubPropertiesAsMap(properties, "headers");
    Assert.assertEquals(result.get("apiKey"), "changeMe");
    Assert.assertEquals(result.get("content"), "json");
  }

  @Test
  public void testParseCharList()
  {
    Assert.assertEquals(ConfigUtils.parseCharList(".,\\,,\\;,\\\\,-,\\ "), Set.of('.', ',', ';', '\\', '-', ' '));
    Assert.assertEquals(ConfigUtils.parseCharList(" . , \\,  ,  \\; , \\\\, -  "), Set.of('.', ',', ';', '\\', '-'));
  }
}
