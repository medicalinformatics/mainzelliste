package de.pseudonymisierung.mainzelliste.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.pseudonymisierung.mainzelliste.Field;
import de.pseudonymisierung.mainzelliste.IntegerField;
import de.pseudonymisierung.mainzelliste.mockup.MockUpID;
import de.pseudonymisierung.mainzelliste.PlainTextField;
import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.util.gson.RecordExclusionStrategy;
import de.pseudonymisierung.mainzelliste.webservice.commons.Callback;
import de.pseudonymisierung.mainzelliste.webservice.commons.CallbackFactory;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CallbackServiceTest {
  private final Gson gson =  new GsonBuilder().setExclusionStrategies(new RecordExclusionStrategy()).create();

  @Test
  public void testCreateCallback() {
    // add patient callback
    Callback callback = CallbackFactory.createCallback(
        new Servers.ApiVersion("3.2"),
        "www.backend.de",
        "545-fs5dc-45654",
        Arrays.asList(new MockUpID("clinicId", "415454"), new MockUpID("projectId", "4444")),
        Map.of("name", Field.build(PlainTextField.class, "Liu"), "birthday", Field.build(
            IntegerField.class, 3)), null, null);
    String expectedResult = """
        {"tokenId":"545-fs5dc-45654",\
        "ids":[{"idType":"clinicId","idString":"415454","tentative":false},{"idType":"projectId","idString":"4444","tentative":false}],\
        "fields":{"birthday":"03","name":"Liu"}}""";
    Assert.assertEquals(gson.toJson(callback), expectedResult);

    // check match callback
    callback = CallbackFactory.createCallback(
        new Servers.ApiVersion("3.2"),
        "www.backend.de",
        "545-fs5dc-45654",
        Arrays.asList(new MockUpID("clinicId", "415454"), new MockUpID("projectId", "4444")),
        null, null, List.of(0.9));
    expectedResult = """
        {"tokenId":"545-fs5dc-45654",\
        "ids":[{"idType":"clinicId","idString":"415454","tentative":false},{"idType":"projectId","idString":"4444","tentative":false}],\
        "similarityScores":[0.9]}""";
    Assert.assertEquals(gson.toJson(callback), expectedResult);

    // delete and edit patient callback
    callback = CallbackFactory.createCallback(
        new Servers.ApiVersion("3.2"),
        "www.backend.de",
        "545-fs5dc-45654",
        Collections.singletonList(new MockUpID("clinicId", "415454")),
        null, null, null);
    expectedResult = """
        {"tokenId":"545-fs5dc-45654",\
        "ids":[{"idType":"clinicId","idString":"415454","tentative":false}]}""";
    Assert.assertEquals(gson.toJson(callback), expectedResult);
  }
}
