package de.pseudonymisierung.mainzelliste.matcher;

import de.pseudonymisierung.mainzelliste.PlainTextField;
import de.pseudonymisierung.mainzelliste.RecordTransformer;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GermanLastNameDecomposerTest {

  @Test
  public void testTransform() {
    GermanLastNameDecomposer decomposer = new GermanLastNameDecomposer(
        RecordTransformer.defaultDelimiterChars);
    List<PlainTextField> expected = List.of(new PlainTextField("a"), new PlainTextField("b"), new PlainTextField(" c "));
    Assert.assertEquals(decomposer.transform(new PlainTextField("a:b-c")).getValue(), expected);
    Assert.assertEquals(decomposer.transform(new PlainTextField("a,b c")).getValue(), expected);
    Assert.assertEquals(decomposer.transform(new PlainTextField("a;b'c")).getValue(), expected);
    Assert.assertEquals(decomposer.transform(new PlainTextField("a#b'c")).getValue(),
        List.of(new PlainTextField("a#b"), new PlainTextField("c"), new PlainTextField("")));


    // test name with paticles
    Assert.assertEquals(decomposer.transform(new PlainTextField("del'a#b'c")).getValue(),
        List.of(new PlainTextField("a#b"), new PlainTextField("c"), new PlainTextField("del")));
    Assert.assertEquals(decomposer.transform(new PlainTextField("del'a#bc")).getValue(),
        List.of(new PlainTextField("a#bc"), new PlainTextField(""), new PlainTextField("del")));
    Assert.assertEquals(decomposer.transform(new PlainTextField("del'a b c")).getValue(),
        List.of(new PlainTextField("a"), new PlainTextField("b"), new PlainTextField(" c del")));
  }
}
