package de.pseudonymisierung.mainzelliste.matcher;

import de.pseudonymisierung.mainzelliste.PlainTextField;
import de.pseudonymisierung.mainzelliste.RecordTransformer;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FirstNameDecomposerTest {

  @Test
  public void testTransform() {
    FirstNameDecomposer decomposer = new FirstNameDecomposer(
      RecordTransformer.defaultDelimiterChars);
    List<PlainTextField> expected = List.of(new PlainTextField("a"), new PlainTextField("b"), new PlainTextField("c"));
    Assert.assertEquals(decomposer.transform(new PlainTextField("a:b-c")).getValue(), expected);
    Assert.assertEquals(decomposer.transform(new PlainTextField("a,b c")).getValue(), expected);
    Assert.assertEquals(decomposer.transform(new PlainTextField("a;b'c")).getValue(), expected);
    Assert.assertEquals(decomposer.transform(new PlainTextField("a#b'c")).getValue(),
        List.of(new PlainTextField("a#b"), new PlainTextField("c"), new PlainTextField("")));
  }
}
