package de.pseudonymisierung.mainzelliste.matcher;

import de.pseudonymisierung.mainzelliste.PlainTextField;
import de.pseudonymisierung.mainzelliste.RecordTransformer;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class StringNormalizerTest {

  @DataProvider
  public Object[][] sendData(){
    return new Object[][]{
        {" .:,;-'äxÄxáxÁxöxÖx .:,;-xüxÜxßxéxÉxèxÈ .:,;-'", "AEXAEXAXAXOEXOEX .:,;-XUEXUEXSSXEXEXEXE"}
    };
  }

  @Test(dataProvider="sendData")
  public void testTransform(String inputString, String resultString){
    PlainTextField input = new PlainTextField(inputString);
    PlainTextField expectedResult = new PlainTextField(resultString);
    PlainTextField result = new StringNormalizer(StringNormalizer.defaultChartReplacementMap,
        RecordTransformer.defaultDelimiterChars).transform(input);
    Assert.assertEquals(result.getValue(), expectedResult.getValue());
  }
}
